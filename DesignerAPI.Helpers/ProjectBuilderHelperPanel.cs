//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Builder
{
    using DesignerAPI.Components;
	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components.Data;
	using DesignerAPI.Components.Form;
	using DesignerAPI.Components.Grid;
	using System.Collections.Generic;

	public static partial class ProjectBuilderHelper
	{    
		public static IPanel AddPanel(this ProjectBuilder projectBuilder)
		{
			if (projectBuilder == null)
				throw new ArgumentNullException("projectBuilder");

			IPanel panel = new Panel();
			projectBuilder.Components.Add(panel);
			LatestComponent.Push(projectBuilder);
			ActiveProjectBuilder = projectBuilder;
			return panel;
		}

		public static IPanel AddPanel(this ProjectBuilder projectBuilder, IComponent component)
		{
			if (projectBuilder == null)
				throw new ArgumentNullException("projectBuilder");

			if (component == null)
				throw new ArgumentNullException("component");

			IPanel panel = new Panel();
			component.Components.Add(panel);			
			LatestComponent.Push(component);
			ActiveProjectBuilder = projectBuilder;
			return panel;
		}

        public static ITabPanel AddTabPanel(this ProjectBuilder projectBuilder)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            ITabPanel tabPanel = new TabPanel();
            projectBuilder.Components.Add(tabPanel);
            LatestComponent.Push(projectBuilder);
            ActiveProjectBuilder = projectBuilder;
            return tabPanel;
        }
        
        public static ITabPanel AddTabPanel(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");
            
            ITabPanel tabPanel = new TabPanel();
            component.Components.Add(tabPanel);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return tabPanel;
        }
	}   
}