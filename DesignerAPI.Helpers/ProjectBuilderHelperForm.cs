//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Builder
{
    using System;
	using System.Collections.Generic;
    using DesignerAPI.Components;
	using DesignerAPI.Components.Data;
	using DesignerAPI.Components.Form;
	using DesignerAPI.Components.Grid;
	using DesignerAPI.Model;

	public static partial class ProjectBuilderHelper
	{
        #region Form
		public static IFormPanel AddFormPanel(this ProjectBuilder projectBuilder)
		{
			if (projectBuilder == null)
				throw new ArgumentNullException("projectBuilder");   

			IFormPanel formPanel = new FormPanel();
			projectBuilder.Components.Add(formPanel);           
			LatestComponent.Push(projectBuilder);
			ActiveProjectBuilder = projectBuilder;
			return formPanel;
		}
        
		public static IFormPanel AddFormPanel(this ProjectBuilder projectBuilder, IComponent component)
		{
			if (projectBuilder == null)
				throw new ArgumentNullException("projectBuilder");

			if (component == null)
				throw new ArgumentNullException("component");  

			IFormPanel formPanel = new FormPanel();
			component.Components.Add(formPanel);            
			LatestComponent.Push(component);
			ActiveProjectBuilder = projectBuilder;
			return formPanel;
		}
        
        #region FieldSet
		public static IFieldSet AddFieldSet(this ProjectBuilder projectBuilder)
		{
			if (projectBuilder == null)
				throw new ArgumentNullException("projectBuilder");  

			IFieldSet fieldSet = new FieldSet();
			projectBuilder.Components.Add(fieldSet);            
			LatestComponent.Push(projectBuilder);
			ActiveProjectBuilder = projectBuilder;
			return fieldSet;
		}
        
		public static IFieldSet AddFieldSet(this ProjectBuilder projectBuilder, IComponent component)
		{
			if (projectBuilder == null)
				throw new ArgumentNullException("projectBuilder");
            
			if (component == null)
				throw new ArgumentNullException("component");       

			IFieldSet fieldSet = new FieldSet();
			component.Components.Add(fieldSet);         
			LatestComponent.Push(component);
			ActiveProjectBuilder = projectBuilder;
			return fieldSet;
		}
        #endregion FieldSet

        #region Controls

        public static ICheckbox AddCheckbox(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ICheckbox checkbox = new Checkbox();
            component.Components.Add(checkbox);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return checkbox;
        }

        public static ICheckboxGroup AddCheckboxGroup(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ICheckboxGroup checkboxGroup = new CheckboxGroup();
            component.Components.Add(checkboxGroup);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return checkboxGroup;
        }

        public static IComboBox AddComboBox(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            IComboBox comboBox = new ComboBox();
            component.Components.Add(comboBox);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return comboBox;
        }

        public static ICompositeField AddCompositeField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ICompositeField compositeField = new CompositeField();
            component.Components.Add(compositeField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return compositeField;
        }

        public static DesignerAPI.Components.Form.IDateField AddDateField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            DesignerAPI.Components.Form.IDateField dateField = new DesignerAPI.Components.Form.DateField();
            component.Components.Add(dateField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return dateField;
        }

        public static IDisplayField AddDisplayField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            IDisplayField displayField = new DisplayField();
            component.Components.Add(displayField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return displayField;
        }

        public static IHidden AddHidden(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            IHidden hidden = new Hidden();
            component.Components.Add(hidden);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return hidden;
        }

        public static IHtmlEditor AddHtmlEditor(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            IHtmlEditor htmlEditor = new HtmlEditor();
            component.Components.Add(htmlEditor);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return htmlEditor;
        }

        public static INumberField AddNumberField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            INumberField numberField = new NumberField();
            component.Components.Add(numberField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return numberField;
        }

        public static IRadio AddRadio(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            IRadio radio = new Radio();
            component.Components.Add(radio);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return radio;
        }

        public static IRadioGroup AddRadioGroup(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            IRadioGroup radioGroup = new RadioGroup();
            component.Components.Add(radioGroup);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return radioGroup;
        }

        public static ISliderField AddSliderField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ISliderField sliderField = new SliderField();
            component.Components.Add(sliderField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return sliderField;
        }

        public static ITextArea AddTextArea(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ITextArea textArea = new TextArea();
            component.Components.Add(textArea);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return textArea;
        }

        public static ITextField AddTextField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ITextField textField = new TextField();
            component.Components.Add(textField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return textField;
        }

        public static ITimeField AddTimeField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ITimeField timeField = new TimeField();
            component.Components.Add(timeField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return timeField;
        }

        public static ITriggerField AddTriggerField(this ProjectBuilder projectBuilder, IComponent component)
        {
            if (projectBuilder == null)
                throw new ArgumentNullException("projectBuilder");
            
            if (component == null)
                throw new ArgumentNullException("component");       
            
            ITriggerField triggerField = new TriggerField();
            component.Components.Add(triggerField);         
            LatestComponent.Push(component);
            ActiveProjectBuilder = projectBuilder;
            return triggerField;
        }

        #endregion Controls

        #endregion Form
	}
    
}