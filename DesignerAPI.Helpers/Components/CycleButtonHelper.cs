//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
    
	using System.Collections.Generic;

	public static class CycleButtonHelper
	{
        #region ForceIcon
        
		public static ICycleButton SetForceIcon(this ICycleButton cycleButton, string forceIcon)
		{
			if (cycleButton == null)
				throw new ArgumentNullException("cycleButton");
            
			if (String.IsNullOrEmpty(forceIcon) && cycleButton.UserConfig.ContainsKey("forceIcon"))
				cycleButton.UserConfig.Remove("forceIcon");
			else
				cycleButton.UserConfig ["forceIcon"] = forceIcon;
            
			return cycleButton;
		}
        
		public static string GetForceIcon(this ICycleButton cycleButton)
		{
			if (cycleButton == null)
				throw new ArgumentNullException("cycleButton");
            
			if (cycleButton.UserConfig.ContainsKey("forceIcon"))
				return (string)cycleButton.UserConfig ["forceIcon"];
			else
				return null;
		}
        
        #endregion ForceIcon


        #region PrependText
        
		public static ICycleButton SetPrependText(this ICycleButton cycleButton, string prependText)
		{
			if (cycleButton == null)
				throw new ArgumentNullException("cycleButton");
            
			if (String.IsNullOrEmpty(prependText) && cycleButton.UserConfig.ContainsKey("prependText"))
				cycleButton.UserConfig.Remove("prependText");
			else
				cycleButton.UserConfig ["prependText"] = prependText;
            
			return cycleButton;
		}
        
		public static string GetPrependText(this ICycleButton cycleButton)
		{
			if (cycleButton == null)
				throw new ArgumentNullException("cycleButton");
            
			if (cycleButton.UserConfig.ContainsKey("prependText"))
				return (string)cycleButton.UserConfig ["prependText"];
			else
				return null;
		}
        
        #endregion PrependText

        #region ShowText
        
		public static ICycleButton SetShowText(this ICycleButton cycleButton, bool showText)
		{
			if (cycleButton == null)
				throw new ArgumentNullException("cycleButton");
            
			cycleButton.UserConfig ["showText"] = showText;
            
			return cycleButton;
		}
        
		public static bool GetShowText(this ICycleButton cycleButton)
		{
			if (cycleButton == null)
				throw new ArgumentNullException("cycleButton");
            
			if (cycleButton.UserConfig.ContainsKey("showText"))
				return (bool)cycleButton.UserConfig ["showText"];
			else
				return false;
		}
        
        #endregion ShowText
	}
       
}
