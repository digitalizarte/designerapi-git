//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
    
	using System.Collections.Generic;


	public static class CheckboxHelper
	{
		// TODO: autoCreate

        #region BoxLabel
                
		public static ICheckbox SetBoxLabel(this ICheckbox checkbox, string boxLabel)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (String.IsNullOrEmpty(boxLabel) && checkbox.UserConfig.ContainsKey("boxLabel"))
				checkbox.UserConfig.Remove("boxLabel");
			else
				checkbox.UserConfig ["boxLabel"] = boxLabel;
            
			return checkbox;
		}
        
		public static string GetBoxLabel(this ICheckbox checkbox)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (checkbox.UserConfig.ContainsKey("boxLabel"))
				return (string)checkbox.UserConfig ["boxLabel"];
			else
				return null;
		}
        
        #endregion BoxLabel

        #region Checked
        
		public static ICheckbox SetChecked(this ICheckbox checkbox, bool value)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			checkbox.UserConfig ["checked"] = value;
            
			return checkbox;
		}
        
		public static bool GetChecked(this ICheckbox checkbox)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (checkbox.UserConfig.ContainsKey("checked"))
				return (bool)checkbox.UserConfig ["checked"];
			else
				return false;
		}
        
        #endregion Checked

        #region FieldClass
        
		public static ICheckbox SetFieldClass(this ICheckbox checkbox, string fieldClass)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (String.IsNullOrEmpty(fieldClass) && checkbox.UserConfig.ContainsKey("fieldClass"))
				checkbox.UserConfig.Remove("fieldClass");
			else
				checkbox.UserConfig ["fieldClass"] = fieldClass;
            
			return checkbox;
		}
        
		public static string GetFieldClass(this ICheckbox checkbox)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (checkbox.UserConfig.ContainsKey("fieldClass"))
				return (string)checkbox.UserConfig ["fieldClass"];
			else
				return null;
		}
        
        #endregion FieldClass
        
        #region FocusClass
        
		public static ICheckbox SetFocusClass(this ICheckbox checkbox, string focusClass)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (String.IsNullOrEmpty(focusClass) && checkbox.UserConfig.ContainsKey("focusClass"))
				checkbox.UserConfig.Remove("focusClass");
			else
				checkbox.UserConfig ["focusClass"] = focusClass;
            
			return checkbox;
		}
        
		public static string GetFocusClass(this ICheckbox checkbox)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (checkbox.UserConfig.ContainsKey("focusClass"))
				return (string)checkbox.UserConfig ["focusClass"];
			else
				return null;
		}
        
        #endregion FocusClass

        #region InputValue
        
		public static ICheckbox SetInputValue(this ICheckbox checkbox, string inputValue)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (String.IsNullOrEmpty(inputValue) && checkbox.UserConfig.ContainsKey("inputValue"))
				checkbox.UserConfig.Remove("inputValue");
			else
				checkbox.UserConfig ["inputValue"] = inputValue;
            
			return checkbox;
		}
        
		public static string GetInputValue(this ICheckbox checkbox)
		{
			if (checkbox == null)
				throw new ArgumentNullException("checkbox");
            
			if (checkbox.UserConfig.ContainsKey("inputValue"))
				return (string)checkbox.UserConfig ["inputValue"];
			else
				return null;
		}
        
        #endregion InputValue

	}
            
}
