
namespace DesignerAPI.Components.Form
{
	using System;
    using DesignerAPI.Builder;
    using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	using System.Collections.Generic;

	public static class FormPanelHelper
	{ 
		public static IFormPanel SetFormId(this IFormPanel form, string formId)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (String.IsNullOrEmpty(formId) && form.UserConfig.ContainsKey("formId"))
				form.UserConfig.Remove("formId");
			else
				form.UserConfig ["formId"] = formId;
			
			return form;
		}
		
		public static string GetFormId(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (form.UserConfig.ContainsKey("formId"))
				return (string)form.UserConfig ["formId"];
			else
				return null;
		}

		public static IFormPanel SetItemCls(this IFormPanel form, string itemCls)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (String.IsNullOrEmpty(itemCls) && form.UserConfig.ContainsKey("itemCls"))
				form.UserConfig.Remove("itemCls");
			else
				form.UserConfig ["itemCls"] = itemCls;
			
			return form;
		}
		
		public static string GetItemCls(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (form.UserConfig.ContainsKey("itemCls"))
				return (string)form.UserConfig ["itemCls"];
			else
				return null;
		}

		public static IFormPanel SetLayout(this IFormPanel form, string layout)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (String.IsNullOrEmpty(layout) && form.UserConfig.ContainsKey("layout"))
				form.UserConfig.Remove("layout");
			else
				form.UserConfig ["layout"] = layout;
			
			return form;
		}
		
		public static string GetLayout(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (form.UserConfig.ContainsKey("layout"))
				return (string)form.UserConfig ["layout"];
			else
				return null;
		}

		public static IFormPanel SetMinButtonWidth(this IFormPanel form, int minButtonWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["minButtonWidth"] = minButtonWidth;
			
			
			return form;
		}
		
        [CLSCompliantAttribute(false)]
		public static IFormPanel SetMinButtonWidth(this IFormPanel form, uint minButtonWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["minButtonWidth"] = minButtonWidth;
			
			
			return form;
		}
		
		public static IFormPanel SetMinButtonWidth(this IFormPanel form, long minButtonWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["minButtonWidth"] = minButtonWidth;
			
			
			return form;
		}
		
        [CLSCompliantAttribute(false)]
		public static IFormPanel SetMinButtonWidth(this IFormPanel form, ulong minButtonWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["minButtonWidth"] = minButtonWidth;
			
			
			return form;
		}
		
		public static IFormPanel SetMinButtonWidth(this IFormPanel form, short minButtonWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["minButtonWidth"] = minButtonWidth;
			
			
			return form;
		}
		
        [CLSCompliantAttribute(false)]
		public static IFormPanel SetMinButtonWidth(this IFormPanel form, ushort minButtonWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["minButtonWidth"] = minButtonWidth;
			
			
			return form;
		}
				
		public static Variant GetMinButtonWidth(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (form.UserConfig.ContainsKey("minButtonWidth"))
				return new Variant(form.UserConfig ["minButtonWidth"]);
			else
				return null;
		}

		public static IFormPanel SetMonitorPoll(this IFormPanel form, int monitorPoll)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorPoll"] = monitorPoll;
			
			
			return form;
		}
		
        [CLSCompliantAttribute(false)]
		public static IFormPanel SetMonitorPoll(this IFormPanel form, uint monitorPoll)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorPoll"] = monitorPoll;
			
			
			return form;
		}
		
		public static IFormPanel SetMonitorPoll(this IFormPanel form, long monitorPoll)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorPoll"] = monitorPoll;
			
			
			return form;
		}
  
        [CLSCompliantAttribute(false)]
		public static IFormPanel SetMonitorPoll(this IFormPanel form, ulong monitorPoll)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorPoll"] = monitorPoll;
			
			
			return form;
		}
		
		public static IFormPanel SetMonitorPoll(this IFormPanel form, short monitorPoll)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorPoll"] = monitorPoll;
			
			
			return form;
		}
		
        [CLSCompliantAttribute(false)]
		public static IFormPanel SetMonitorPoll(this IFormPanel form, ushort monitorPoll)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorPoll"] = monitorPoll;
			
			
			return form;
		}
		
		public static Variant GetMonitorPoll(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (form.UserConfig.ContainsKey("monitorPoll"))
				return new Variant(form.UserConfig ["monitorPoll"]);
			else
				return null;
		}

		public static IFormPanel SetMonitorValid(this IFormPanel form, bool monitorValid)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.UserConfig ["monitorValid"] = monitorValid;
			
			
			return form;
		}
		
		public static bool GetMonitorValid(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			if (form.UserConfig.ContainsKey("monitorValid"))
				return (bool)form.UserConfig ["monitorValid"];
			else
				return false;
		}

        #region LabelWidth
        
		public static IFormPanel SetLabelWidth(this IFormPanel form, int labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}
        
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, int labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}

        [CLSCompliantAttribute(false)]
		public static IFormPanel SetLabelWidth(this IFormPanel form, uint labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}

        [CLSCompliantAttribute(false)]
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, uint labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}
        
		public static IFormPanel SetLabelWidth(this IFormPanel form, long labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}
        
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, long labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}

        [CLSCompliantAttribute(false)]
		public static IFormPanel SetLabelWidth(this IFormPanel form, ulong labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}

        [CLSCompliantAttribute(false)]
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, ulong labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}
        
		public static IFormPanel SetLabelWidth(this IFormPanel form, short labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}
        
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, short labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}

        [CLSCompliantAttribute(false)]
		public static IFormPanel SetLabelWidth(this IFormPanel form, ushort labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}

        [CLSCompliantAttribute(false)]
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, ushort labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}
        
		public static IFormPanel SetLabelWidth(this IFormPanel form, decimal labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}
        
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, decimal labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}
        
		public static IFormPanel SetLabelWidth(this IFormPanel form, float labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}
        
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, float labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");
			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}
        
		public static IFormPanel SetLabelWidth(this IFormPanel form, double labelLabelWidth)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			form.UserConfig ["labelLabelWidth"] = labelLabelWidth;
            
			return form;
		}
        
		public static TEntity SetLabelWidth <TEntity>(this IFormPanel form, double labelLabelWidth)
		{
            if (form == null)
                throw new ArgumentNullException ("form");

			return (TEntity)form.SetLabelWidth(labelLabelWidth);
		}
        
		public static Variant GetLabelWidth(this IFormPanel form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
            
			if (form.UserConfig.ContainsKey("labelLabelWidth"))
				return new Variant(form.UserConfig ["labelLabelWidth"]);
			else
				return null;
		}
        
        #endregion LabelWidth
	}		
}
