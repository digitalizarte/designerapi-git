//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Linq;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
    
	using System.Collections.Generic;

	public static class DisplayFieldHelper
	{
        #region FieldClass
        
		public static IDisplayField SetFieldClass(this IDisplayField displayField, string fieldClass)
		{
			if (displayField == null)
				throw new ArgumentNullException("displayField");
            
			if (String.IsNullOrEmpty(fieldClass) && displayField.UserConfig.ContainsKey("fieldClass"))
				displayField.UserConfig.Remove("fieldClass");
			else
				displayField.UserConfig ["fieldClass"] = fieldClass;
            
			return displayField;
		}
        
		public static string GetFieldClass(this IDisplayField displayField)
		{
			if (displayField == null)
				throw new ArgumentNullException("displayField");
            
			if (displayField.UserConfig.ContainsKey("fieldClass"))
				return (string)displayField.UserConfig ["fieldClass"];
			else
				return null;
		}
        
        #endregion FieldClass

        #region HtmlEncode
        
		public static IDisplayField SetHtmlEncode(this IDisplayField displayField, bool htmlEncode)
		{
			if (displayField == null)
				throw new ArgumentNullException("displayField");
            
			displayField.UserConfig ["htmlEncode"] = htmlEncode;
            
			return displayField;
		}
        
		public static bool GetHtmlEncode(this IDisplayField displayField)
		{
			if (displayField == null)
				throw new ArgumentNullException("displayField");
            
			if (displayField.UserConfig.ContainsKey("htmlEncode"))
				return (bool)displayField.UserConfig ["htmlEncode"];
			else
				return false;
		}
        
        #endregion HtmlEncode
	}           
}
