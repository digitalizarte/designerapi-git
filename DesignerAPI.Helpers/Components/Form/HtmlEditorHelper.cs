//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
    
	using System.Collections.Generic;

	public static class HtmlEditorHelper
	{
        #region CreateLinkText
        
		public static IHtmlEditor SetCreateLinkText(this IHtmlEditor htmlEditor, string createLinkText)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (String.IsNullOrEmpty(createLinkText) && htmlEditor.UserConfig.ContainsKey("createLinkText"))
				htmlEditor.UserConfig.Remove("createLinkText");
			else
				htmlEditor.UserConfig ["createLinkText"] = createLinkText;
            
			return htmlEditor;
		}
        
		public static string GetCreateLinkText(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("createLinkText"))
				return (string)htmlEditor.UserConfig ["createLinkText"];
			else
				return null;
		}
        
        #endregion CreateLinkText

        #region DefaultLinkValue
        
		public static IHtmlEditor SetDefaultLinkValue(this IHtmlEditor htmlEditor, string defaultLinkValue)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (String.IsNullOrEmpty(defaultLinkValue) && htmlEditor.UserConfig.ContainsKey("defaultLinkValue"))
				htmlEditor.UserConfig.Remove("defaultLinkValue");
			else
				htmlEditor.UserConfig ["defaultLinkValue"] = defaultLinkValue;
            
			return htmlEditor;
		}
        
		public static string GetDefaultLinkValue(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("defaultLinkValue"))
				return (string)htmlEditor.UserConfig ["defaultLinkValue"];
			else
				return null;
		}
        
        #endregion DefaultLinkValue       

        #region DefaultValue
        
		public static IHtmlEditor SetDefaultValue(this IHtmlEditor htmlEditor, string defaultValue)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (String.IsNullOrEmpty(defaultValue) && htmlEditor.UserConfig.ContainsKey("defaultValue"))
				htmlEditor.UserConfig.Remove("defaultValue");
			else
				htmlEditor.UserConfig ["defaultValue"] = defaultValue;
            
			return htmlEditor;
		}
        
		public static string GetDefaultValue(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("defaultValue"))
				return (string)htmlEditor.UserConfig ["defaultValue"];
			else
				return null;
		}
        
        #endregion DefaultValue    

        #region EnableAlignments
        
		public static IHtmlEditor SetEnableAlignments(this IHtmlEditor htmlEditor, bool enableAlignments)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableAlignments"] = enableAlignments;
            
			return htmlEditor;
		}
        
		public static bool GetEnableAlignments(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableAlignments"))
				return (bool)htmlEditor.UserConfig ["enableAlignments"];
			else
				return false;
		}
        
        #endregion EnableAlignments

        #region EnableColors
        
		public static IHtmlEditor SetEnableColors(this IHtmlEditor htmlEditor, bool enableColors)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableColors"] = enableColors;
            
			return htmlEditor;
		}
        
		public static bool GetEnableColors(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableColors"))
				return (bool)htmlEditor.UserConfig ["enableColors"];
			else
				return false;
		}
        
        #endregion EnableColors

        #region EnableFont
        
		public static IHtmlEditor SetEnableFont(this IHtmlEditor htmlEditor, bool enableFont)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableFont"] = enableFont;
            
			return htmlEditor;
		}
        
		public static bool GetEnableFont(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableFont"))
				return (bool)htmlEditor.UserConfig ["enableFont"];
			else
				return false;
		}
        
        #endregion EnableFont

        #region EnableFontSize
        
		public static IHtmlEditor SetEnableFontSize(this IHtmlEditor htmlEditor, bool enableFontSize)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableFontSize"] = enableFontSize;
            
			return htmlEditor;
		}
        
		public static bool GetEnableFontSize(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableFontSize"))
				return (bool)htmlEditor.UserConfig ["enableFontSize"];
			else
				return false;
		}
        
        #endregion EnableFontSize

        #region EnableFormat
        
		public static IHtmlEditor SetEnableFormat(this IHtmlEditor htmlEditor, bool enableFormat)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableFormat"] = enableFormat;
            
			return htmlEditor;
		}
        
		public static bool GetEnableFormat(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableFormat"))
				return (bool)htmlEditor.UserConfig ["enableFormat"];
			else
				return false;
		}
        
        #endregion EnableFormat

        #region EnableLinks
        
		public static IHtmlEditor SetEnableLinks(this IHtmlEditor htmlEditor, bool enableLinks)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableLinks"] = enableLinks;
            
			return htmlEditor;
		}
        
		public static bool GetEnableLinks(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableLinks"))
				return (bool)htmlEditor.UserConfig ["enableLinks"];
			else
				return false;
		}
        
        #endregion EnableLinks

        #region EnableLists
        
		public static IHtmlEditor SetEnableLists(this IHtmlEditor htmlEditor, bool enableLists)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableLists"] = enableLists;
            
			return htmlEditor;
		}
        
		public static bool GetEnableLists(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableLists"))
				return (bool)htmlEditor.UserConfig ["enableLists"];
			else
				return false;
		}
        
        #endregion EnableLists

        #region EnableSourceEdit
        
		public static IHtmlEditor SetEnableSourceEdit(this IHtmlEditor htmlEditor, bool enableSourceEdit)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			htmlEditor.UserConfig ["enableSourceEdit"] = enableSourceEdit;
            
			return htmlEditor;
		}
        
		public static bool GetEnableSourceEdit(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("enableSourceEdit"))
				return (bool)htmlEditor.UserConfig ["enableSourceEdit"];
			else
				return false;
		}
        
        #endregion EnableSourceEdit

        #region FontFamilies        
        
		public static IHtmlEditor SetFontFamilies(this IHtmlEditor htmlEditor, string[] fontFamilies)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (fontFamilies == null)
				throw new ArgumentNullException("fontFamilies");
            
			htmlEditor.UserConfig ["fontFamilies"] = new List<string>(fontFamilies);
            
			return htmlEditor;
		}
        
		public static IHtmlEditor SetFontFamilies(this IHtmlEditor htmlEditor, IList<string> fontFamilies)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (fontFamilies == null)
				throw new ArgumentNullException("fontFamilies");
            
			htmlEditor.UserConfig ["fontFamilies"] = fontFamilies;
            
			return htmlEditor;
		}
        
		public static IList<string> GetFontFamilies(this IHtmlEditor htmlEditor)
		{
			if (htmlEditor == null)
				throw new ArgumentNullException("htmlEditor");
            
			if (htmlEditor.UserConfig.ContainsKey("fontFamilies"))
				return (IList<string>)htmlEditor.UserConfig ["fontFamilies"];
			else
				return null;
		}
        
        #endregion FontFamilies
	}  
}
