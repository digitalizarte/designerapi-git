//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
    using DesignerAPI.Model;
    using DesignerAPI.Components;
    using DesignerAPI.Components.Form;

    using System.Collections.Generic;

    public static class FieldHelper
    {
        #region Cls
        
        public static IField SetCls (this IField field, string cls)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (cls) && field.UserConfig.ContainsKey ("cls"))
                field.UserConfig.Remove ("cls");
            else
                field.UserConfig ["cls"] = cls;
            
            return field;
        }
        
        public static string GetCls (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("cls"))
                return (string)field.UserConfig ["cls"];
            else
                return null;
        }
        
        #endregion Cls

        #region Disabled
        
        public static IField SetDisabled (this IField field, bool disabled)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["disabled"] = disabled;
            
            return field;
        }
        
        public static bool GetDisabled (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("disabled"))
                return (bool)field.UserConfig ["disabled"];
            else
                return false;
        }
        
        #endregion Disabled

        #region InputType
        
        public static IField SetInputType (this IField field, string inputType)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (inputType) && field.UserConfig.ContainsKey ("inputType"))
                field.UserConfig.Remove ("inputType");
            else
                field.UserConfig ["inputType"] = inputType;
            
            return field;
        }
        
        public static string GetInputType (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("inputType"))
                return (string)field.UserConfig ["inputType"];
            else
                return null;
        }
        
        #endregion InputType

        #region InvalidClass
        
        public static IField SetInvalidClass (this IField field, string invalidClass)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (invalidClass) && field.UserConfig.ContainsKey ("invalidClass"))
                field.UserConfig.Remove ("invalidClass");
            else
                field.UserConfig ["invalidClass"] = invalidClass;
            
            return field;
        }
        
        public static string GetInvalidClass (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("invalidClass"))
                return (string)field.UserConfig ["invalidClass"];
            else
                return null;
        }
        
        #endregion InvalidClass

        #region InvalidText
        
        public static IField SetInvalidText (this IField field, string invalidText)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (invalidText) && field.UserConfig.ContainsKey ("invalidText"))
                field.UserConfig.Remove ("invalidText");
            else
                field.UserConfig ["invalidText"] = invalidText;
            
            return field;
        }
        
        public static string GetInvalidText (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("invalidText"))
                return (string)field.UserConfig ["invalidText"];
            else
                return null;
        }
        
        #endregion InvalidText

        #region MsgFx
        
        public static IField SetMsgFx (this IField field, MsgFx msgFx)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            string value = Enum.GetName (typeof(MsgFx), msgFx).ToUpperInvariant ();
            field.UserConfig ["msgFx"] = value;
            
            return field;
        }
        
        public static MsgFx GetMsgFx (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("msgFx"))
                return (MsgFx)Enum.Parse (typeof(MsgFx), (string)field.UserConfig ["msgFx"], true);
            else
                return MsgFx.Normal;
        }
        
        #endregion MsgFx

        #region MsgTarget

        public static IField SetMsgTarget (this IField field, MsgTarget msgTarget)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            string value = Enum.GetName (typeof(MsgTarget), msgTarget).ToUpperInvariant ();
            field.UserConfig ["msgTarget"] = value;
            
            return field;
        }

        public static IField SetMsgTarget (this IField field, string msgTarget)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (msgTarget) && field.UserConfig.ContainsKey ("msgTarget"))
                field.UserConfig.Remove ("msgTarget");
            else
                field.UserConfig ["msgTarget"] = msgTarget;
            
            return field;
        }

        public static Variant GetMsgTarget (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("msgTarget"))
                return new Variant (field.UserConfig ["msgTarget"]);
            else
                return null;
        }
        
        #endregion MsgTarget

        #region Name
        
        public static IField SetName (this IField field, string name)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (name) && field.UserConfig.ContainsKey ("name"))
                field.UserConfig.Remove ("name");
            else
                field.UserConfig ["name"] = name;
            
            return field;
        }

//        public static IField SetName(this IField field, string name)
//        {
//            if (field == null)
//                throw new ArgumentNullException("field");
//            
//            if (String.IsNullOrEmpty(name) && field.UserConfig.ContainsKey("name"))
//                field.UserConfig.Remove("name");
//            else
//                field.UserConfig ["name"] = name;
//            
//            return field;
//        }

        public static TEntity SetName<TEntity> (this IField field, string name)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            return (TEntity)field.SetName (name);
        }
        
        public static string GetName (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("name"))
                return (string)field.UserConfig ["name"];
            else
                return null;
        }
        
        #endregion Name

        #region PreventMark
        
        public static IField SetPreventMark (this IField field, bool preventMark)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["preventMark"] = preventMark;
            
            return field;
        }
        
        public static bool GetPreventMark (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("preventMark"))
                return (bool)field.UserConfig ["preventMark"];
            else
                return false;
        }
        
        #endregion PreventMark

        #region ReadOnly
        
        public static IField SetReadOnly (this IField field, bool readOnly)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["readOnly"] = readOnly;
            
            return field;
        }

        public static TEntity SetReadOnly<TEntity> (this IField field, bool readOnly)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            return (TEntity)field.SetReadOnly (readOnly);
        }
        
        public static bool GetReadOnly (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("readOnly"))
                return (bool)field.UserConfig ["readOnly"];
            else
                return false;
        }

        
        #endregion ReadOnly

        #region SubmitValue
        
        public static IField SetSubmitValue (this IField field, bool submitValue)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["submitValue"] = submitValue;
            
            return field;
        }
        
        public static bool GetSubmitValue (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("submitValue"))
                return (bool)field.UserConfig ["submitValue"];
            else
                return false;
        }
        
        #endregion SubmitValue

        #region TabIndex
        
        public static IField SetTabIndex (this IField field, int tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }

        [CLSCompliantAttribute(false)]
        public static IField SetTabIndex (this IField field, uint tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }
        
        public static IField SetTabIndex (this IField field, long tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }

        [CLSCompliantAttribute(false)]
        public static IField SetTabIndex (this IField field, ulong tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }
        
        public static IField SetTabIndex (this IField field, short tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }
        
        [CLSCompliantAttribute(false)]
        public static IField SetTabIndex (this IField field, ushort tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }
        
        public static IField SetTabIndex (this IField field, decimal tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }
        
        public static IField SetTabIndex (this IField field, float tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }
        
        public static IField SetTabIndex (this IField field, double tabIndex)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["tabIndex"] = tabIndex;
            
            return field;
        }

        public static Variant GetTabIndex (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("tabIndex"))
                return new Variant (field.UserConfig ["tabIndex"]);
            else
                return null;
        }
        
        #endregion TabIndex

        #region ValidateOnBlur
        
        public static IField SetValidateOnBlur (this IField field, bool validateOnBlur)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validateOnBlur"] = validateOnBlur;
            
            return field;
        }
        
        public static bool GetValidateOnBlur (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("validateOnBlur"))
                return (bool)field.UserConfig ["validateOnBlur"];
            else
                return false;
        }
        
        #endregion ValidateOnBlur

        #region ValidationDelay
        
        public static IField SetValidationDelay (this IField field, int validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }

        [CLSCompliantAttribute(false)]
        public static IField SetValidationDelay (this IField field, uint validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }
        
        public static IField SetValidationDelay (this IField field, long validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }

        [CLSCompliantAttribute(false)]
        public static IField SetValidationDelay (this IField field, ulong validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }
        
        public static IField SetValidationDelay (this IField field, short validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }

        [CLSCompliantAttribute(false)]
        public static IField SetValidationDelay (this IField field, ushort validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }
        
        public static IField SetValidationDelay (this IField field, decimal validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }
        
        public static IField SetValidationDelay (this IField field, float validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }
        
        public static IField SetValidationDelay (this IField field, double validationDelay)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            field.UserConfig ["validationDelay"] = validationDelay;
            
            return field;
        }

        public static Variant GetValidationDelay (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("validationDelay"))
                return new Variant (field.UserConfig ["validationDelay"]);
            else
                return null;
        }

        #endregion ValidationDelay

        #region ValidationEvent
        
        public static IField SetValidationEvent (this IField field, string validationEvent)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (String.IsNullOrEmpty (validationEvent) && field.UserConfig.ContainsKey ("validationEvent"))
                field.UserConfig.Remove ("validationEvent");
            else
                field.UserConfig ["validationEvent"] = validationEvent;
            
            return field;
        }
        
        public static string GetValidationEvent (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("validationEvent"))
                return (string)field.UserConfig ["validationEvent"];
            else
                return null;
        }
        
        #endregion ValidationEvent

        #region Value
        
        public static IField SetValue (this IField field, object value)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (value == null && field.UserConfig.ContainsKey ("value"))
                field.UserConfig.Remove ("value");
            else
                field.UserConfig ["value"] = value;
            
            return field;
        }
        
        public static object GetValue (this IField field)
        {
            if (field == null)
                throw new ArgumentNullException ("field");
            
            if (field.UserConfig.ContainsKey ("value"))
                return field.UserConfig ["value"];
            else
                return null;
        }
        
        #endregion Value

        #region Tooltip
        
        public static IField SetTooltip(this IField field, string tooltip)
        {
            if (field == null)
                throw new ArgumentNullException("field");
            
            if (String.IsNullOrEmpty(tooltip) && field.UserConfig.ContainsKey("tooltip"))
                field.UserConfig.Remove("tooltip");
            else
                field.UserConfig ["tooltip"] = tooltip;
            
            return field;
        }
        
        public static TEntity SetTooltip<TEntity>(this IField field, string tooltip)
        {
            if (field == null)
                throw new ArgumentNullException ("field");   
            return (TEntity)field.SetTooltip(tooltip);
        }
        
        public static string GetTooltip(this IField field)
        {
            if (field == null)
                throw new ArgumentNullException("field");
            
            if (field.UserConfig.ContainsKey("tooltip"))
                return (string)field.UserConfig ["tooltip"];
            else
                return null;
        }
        
        #endregion Tooltip
    }           
}