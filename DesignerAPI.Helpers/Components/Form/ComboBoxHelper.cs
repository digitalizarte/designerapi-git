//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Linq;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
    
	using System.Collections.Generic;

	public static class ComboBoxHelper
	{

        #region AllQuery
        
		public static IComboBox SetAllQuery(this IComboBox combo, string allQuery)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(allQuery) && combo.UserConfig.ContainsKey("allQuery"))
				combo.UserConfig.Remove("allQuery");
			else
				combo.UserConfig ["allQuery"] = allQuery;
            
			return combo;
		}
        
		public static string GetAllQuery(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("allQuery"))
				return (string)combo.UserConfig ["allQuery"];
			else
				return null;
		}
        
        #endregion AllQuery
        
        #region AutoCreate
        
		// TODO: autoCreate
        
        #endregion AutoCreate

        #region AutoSelect
        
		public static IComboBox SetAutoSelect(this IComboBox combo, bool autoSelect)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["autoSelect"] = autoSelect;
            
			return combo;
		}
        
		public static bool GetAutoSelect(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("autoSelect"))
				return (bool)combo.UserConfig ["autoSelect"];
			else
				return false;
		}
        
        #endregion AutoSelect

        #region ClearFilterOnReset
        
		public static IComboBox SetClearFilterOnReset(this IComboBox combo, bool clearFilterOnReset)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["clearFilterOnReset"] = clearFilterOnReset;
            
			return combo;
		}
        
		public static bool GetClearFilterOnReset(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("clearFilterOnReset"))
				return (bool)combo.UserConfig ["clearFilterOnReset"];
			else
				return false;
		}
        
        #endregion ClearFilterOnReset

        #region DisplayField
                
		public static IComboBox SetDisplayField(this IComboBox combo, string displayField)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(displayField) && combo.UserConfig.ContainsKey("displayField"))
				combo.UserConfig.Remove("displayField");
			else
				combo.UserConfig ["displayField"] = displayField;
            
			return combo;
		}
        
		public static string GetDisplayField(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("displayField"))
				return (string)combo.UserConfig ["displayField"];
			else
				return null;
		}

        #endregion DisplayField

        #region ForceSelection
        
		public static IComboBox SetForceSelection(this IComboBox combo, bool forceSelection)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["forceSelection"] = forceSelection;
            
			return combo;
		}
        
		public static bool GetForceSelection(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("forceSelection"))
				return (bool)combo.UserConfig ["forceSelection"];
			else
				return false;
		}
        
        #endregion ForceSelection

        #region HandleHeight
        
		public static IComboBox SetHandleHeight(this IComboBox combo, int handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetHandleHeight(this IComboBox combo, uint handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}
        
		public static IComboBox SetHandleHeight(this IComboBox combo, long handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetHandleHeight(this IComboBox combo, ulong handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}
        
		public static IComboBox SetHandleHeight(this IComboBox combo, short handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetHandleHeight(this IComboBox combo, ushort handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}
        
		public static IComboBox SetHandleHeight(this IComboBox combo, decimal handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}
        
		public static IComboBox SetHandleHeight(this IComboBox combo, float handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}
        
		public static IComboBox SetHandleHeight(this IComboBox combo, double handleHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["handleHeight"] = handleHeight;
            
			return combo;
		}

		public static Variant GetHandleHeight(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("handleHeight"))
				return new Variant(combo.UserConfig ["handleHeight"]);
			else
				return null;
		}
        
        #endregion HandleHeight

        #region HiddenId
        
		public static IComboBox SetHiddenId(this IComboBox combo, string hiddenId)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(hiddenId) && combo.UserConfig.ContainsKey("hiddenId"))
				combo.UserConfig.Remove("hiddenId");
			else
				combo.UserConfig ["hiddenId"] = hiddenId;
            
			return combo;
		}
        
		public static string GetHiddenId(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("hiddenId"))
				return (string)combo.UserConfig ["hiddenId"];
			else
				return null;
		}
        
        #endregion HiddenId       

        #region HiddenName
        
		public static IComboBox SetHiddenName(this IComboBox combo, string hiddenName)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(hiddenName) && combo.UserConfig.ContainsKey("hiddenName"))
				combo.UserConfig.Remove("hiddenName");
			else
				combo.UserConfig ["hiddenName"] = hiddenName;
            
			return combo;
		}
        
		public static string GetHiddenName(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("hiddenName"))
				return (string)combo.UserConfig ["hiddenName"];
			else
				return null;
		}
        
        #endregion HiddenName

        #region HiddenValue
        
		public static IComboBox SetHiddenValue(this IComboBox combo, string hiddenValue)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(hiddenValue) && combo.UserConfig.ContainsKey("hiddenValue"))
				combo.UserConfig.Remove("hiddenValue");
			else
				combo.UserConfig ["hiddenValue"] = hiddenValue;
            
			return combo;
		}
        
		public static string GetHiddenValue(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("hiddenValue"))
				return (string)combo.UserConfig ["hiddenValue"];
			else
				return null;
		}
        
        #endregion HiddenValue

        #region ItemSelector
        
		public static IComboBox SetItemSelector(this IComboBox combo, string itemSelector)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(itemSelector) && combo.UserConfig.ContainsKey("itemSelector"))
				combo.UserConfig.Remove("itemSelector");
			else
				combo.UserConfig ["itemSelector"] = itemSelector;
            
			return combo;
		}
        
		public static string GetItemSelector(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("itemSelector"))
				return (string)combo.UserConfig ["itemSelector"];
			else
				return null;
		}
        
        #endregion ItemSelector

        #region LazyInit
        
		public static IComboBox SetLazyInit(this IComboBox combo, bool lazyInit)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["lazyInit"] = lazyInit;
            
			return combo;
		}
        
		public static bool GetLazyInit(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("lazyInit"))
				return (bool)combo.UserConfig ["lazyInit"];
			else
				return false;
		}
        
        #endregion LazyInit

        #region LazyRender
        
		public static IComboBox SetLazyRender(this IComboBox combo, bool lazyRender)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["lazyRender"] = lazyRender;
            
			return combo;
		}
        
		public static bool GetLazyRender(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("lazyRender"))
				return (bool)combo.UserConfig ["lazyRender"];
			else
				return false;
		}
        
        #endregion LazyRender

        #region ListAlign
        
		public static IComboBox SetListAlign(this IComboBox combo, string listAlign)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(listAlign) && combo.UserConfig.ContainsKey("listAlign"))
				combo.UserConfig.Remove("listAlign");
			else
			{
				List<string> list = new List<string>();
				list.Add(listAlign);
				combo.UserConfig ["listAlign"] = list;
			}
            
			return combo;
		}

		public static IComboBox SetListAlign(this IComboBox combo, string[] listAlign)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (listAlign == null)
				throw new ArgumentNullException("listAlign");
            
			combo.UserConfig ["listAlign"] = new List<string>(listAlign);
            
			return combo;
		}
        
		public static IComboBox SetListAlign(this IComboBox combo, IList<string> listAlign)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (listAlign == null)
				throw new ArgumentNullException("listAlign");
            
			combo.UserConfig ["listAlign"] = listAlign;
            
			return combo;
		}

		public static IComboBox SetListAlign(this IComboBox combo, AlignTo listAlign)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
            
			List<string> list = new List<string>();
			list.Add(listAlign.ToString().ToLowerInvariant());
			combo.UserConfig ["listAlign"] = list;
            
            
			return combo;
		}

		public static IComboBox SetListAlign(this IComboBox combo, IList<AlignTo> listAlign)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (listAlign == null)
				throw new ArgumentNullException("listAlign");
            
			var listAux = listAlign.Select(x => x.ToString().ToLowerInvariant());
			combo.UserConfig ["listAlign"] = new List<string>(listAux);
            
			return combo;
		}

		public static IComboBox SetListAlign(this IComboBox combo, AlignTo[] listAlign)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (listAlign == null)
				throw new ArgumentNullException("listAlign");

			var listAux = listAlign.Select(x => x.ToString().ToLowerInvariant());
			combo.UserConfig ["listAlign"] = new List<string>(listAux);
            
			return combo;
		}
        
		public static IList<string> GetListAlign(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("listAlign"))
				return (IList<string>)combo.UserConfig ["listAlign"];
			else
				return null;
		}
        
        #endregion ListAlign

        #region ListClass
        
		public static IComboBox SetListClass(this IComboBox combo, string listClass)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(listClass) && combo.UserConfig.ContainsKey("listClass"))
				combo.UserConfig.Remove("listClass");
			else
				combo.UserConfig ["listClass"] = listClass;
            
			return combo;
		}
        
		public static string GetListClass(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("listClass"))
				return (string)combo.UserConfig ["listClass"];
			else
				return null;
		}
        
        #endregion ListClass

        #region ListEmptyText
        
		public static IComboBox SetListEmptyText(this IComboBox combo, string listEmptyText)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(listEmptyText) && combo.UserConfig.ContainsKey("listEmptyText"))
				combo.UserConfig.Remove("listEmptyText");
			else
				combo.UserConfig ["listEmptyText"] = listEmptyText;
            
			return combo;
		}
        
		public static string GetListEmptyText(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("listEmptyText"))
				return (string)combo.UserConfig ["listEmptyText"];
			else
				return null;
		}
        
        #endregion ListEmptyText

        #region ListWidth
        
		public static IComboBox SetListWidth(this IComboBox combo, int listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetListWidth(this IComboBox combo, uint listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}
        
		public static IComboBox SetListWidth(this IComboBox combo, long listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetListWidth(this IComboBox combo, ulong listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}
        
		public static IComboBox SetListWidth(this IComboBox combo, short listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetListWidth(this IComboBox combo, ushort listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}
        
		public static IComboBox SetListWidth(this IComboBox combo, decimal listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}
        
		public static IComboBox SetListWidth(this IComboBox combo, float listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}
        
		public static IComboBox SetListWidth(this IComboBox combo, double listWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["listWidth"] = listWidth;
            
			return combo;
		}

		public static Variant GetListWidth(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("listWidth"))
				return new Variant(combo.UserConfig ["listWidth"]);
			else
				return null;
		}
        
        #endregion ListWidth

        #region LoadingText
        
		public static IComboBox SetLoadingText(this IComboBox combo, string loadingText)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(loadingText) && combo.UserConfig.ContainsKey("loadingText"))
				combo.UserConfig.Remove("loadingText");
			else
				combo.UserConfig ["loadingText"] = loadingText;
            
			return combo;
		}
        
		public static string GetLoadingText(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("loadingText"))
				return (string)combo.UserConfig ["loadingText"];
			else
				return null;
		}
        
        #endregion LoadingText
        
        #region MaxHeight
        
		public static IComboBox SetMaxHeight(this IComboBox combo, int maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMaxHeight(this IComboBox combo, uint maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}
        
		public static IComboBox SetMaxHeight(this IComboBox combo, long maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMaxHeight(this IComboBox combo, ulong maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}
        
		public static IComboBox SetMaxHeight(this IComboBox combo, short maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMaxHeight(this IComboBox combo, ushort maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}
        
		public static IComboBox SetMaxHeight(this IComboBox combo, decimal maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}
        
		public static IComboBox SetMaxHeight(this IComboBox combo, float maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}
        
		public static IComboBox SetMaxHeight(this IComboBox combo, double maxHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["maxHeight"] = maxHeight;
            
			return combo;
		}

		public static Variant GetMaxHeight(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("maxHeight"))
				return new Variant(combo.UserConfig ["maxHeight"]);
			else
				return null;
		}

        #endregion MaxHeight

        #region MinChars
        
		public static IComboBox SetMinChars(this IComboBox combo, int minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinChars(this IComboBox combo, uint minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}
        
		public static IComboBox SetMinChars(this IComboBox combo, long minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinChars(this IComboBox combo, ulong minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}
        
		public static IComboBox SetMinChars(this IComboBox combo, short minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinChars(this IComboBox combo, ushort minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}
        
		public static IComboBox SetMinChars(this IComboBox combo, decimal minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}
        
		public static IComboBox SetMinChars(this IComboBox combo, float minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}
        
		public static IComboBox SetMinChars(this IComboBox combo, double minChars)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minChars"] = minChars;
            
			return combo;
		}
        
		public static Variant GetMinChars(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("minChars"))
				return new Variant(combo.UserConfig ["minChars"]);
			else
				return null;
		}
        
        #endregion MinChars

        #region MinHeight
        
		public static IComboBox SetMinHeight(this IComboBox combo, int minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinHeight(this IComboBox combo, uint minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}
        
		public static IComboBox SetMinHeight(this IComboBox combo, long minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinHeight(this IComboBox combo, ulong minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}
        
		public static IComboBox SetMinHeight(this IComboBox combo, short minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinHeight(this IComboBox combo, ushort minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}
        
		public static IComboBox SetMinHeight(this IComboBox combo, decimal minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}
        
		public static IComboBox SetMinHeight(this IComboBox combo, float minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}
        
		public static IComboBox SetMinHeight(this IComboBox combo, double minHeight)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minHeight"] = minHeight;
            
			return combo;
		}
        
		public static Variant GetMinHeight(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("minHeight"))
				return new Variant(combo.UserConfig ["minHeight"]);
			else
				return null;
		}
        
        #endregion MinHeight

        #region MinListWidth
        
		public static IComboBox SetMinListWidth(this IComboBox combo, int minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinListWidth(this IComboBox combo, uint minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}
        
		public static IComboBox SetMinListWidth(this IComboBox combo, long minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinListWidth(this IComboBox combo, ulong minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}
        
		public static IComboBox SetMinListWidth(this IComboBox combo, short minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetMinListWidth(this IComboBox combo, ushort minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}
        
		public static IComboBox SetMinListWidth(this IComboBox combo, decimal minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}
        
		public static IComboBox SetMinListWidth(this IComboBox combo, float minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}
        
		public static IComboBox SetMinListWidth(this IComboBox combo, double minListWidth)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["minListWidth"] = minListWidth;
            
			return combo;
		}
        
		public static Variant GetMinListWidth(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("minListWidth"))
				return new Variant(combo.UserConfig ["minListWidth"]);
			else
				return null;
		}
        
        #endregion MinListWidth

        #region Mode
        
		public static IComboBox SetMode(this IComboBox combo, Mode  mode)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			string smode = Enum.GetName(typeof(Mode), mode).ToLowerInvariant();
			combo.UserConfig ["mode"] = smode;
            
			return combo;
		}
        
		public static Mode  GetMode(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("mode"))
				return (Mode)Enum.Parse(typeof(Mode), (string)combo.UserConfig ["mode"], true);
			else
				return Mode.Remote;
		}
        
        #endregion Mode

        #region PageSize
        
		public static IComboBox SetPageSize(this IComboBox combo, int pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetPageSize(this IComboBox combo, uint pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
        
		public static IComboBox SetPageSize(this IComboBox combo, long pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
    
        [CLSCompliantAttribute(false)]
		public static IComboBox SetPageSize(this IComboBox combo, ulong pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
        
		public static IComboBox SetPageSize(this IComboBox combo, short pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetPageSize(this IComboBox combo, ushort pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
        
		public static IComboBox SetPageSize(this IComboBox combo, decimal pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
        
		public static IComboBox SetPageSize(this IComboBox combo, float pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
        
		public static IComboBox SetPageSize(this IComboBox combo, double pageSize)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["pageSize"] = pageSize;
            
			return combo;
		}
        
		public static Variant GetPageSize(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("pageSize"))
				return new Variant(combo.UserConfig ["pageSize"]);
			else
				return null;
		}
        
        #endregion PageSize

        #region QueryDelay
        
		public static IComboBox SetQueryDelay(this IComboBox combo, int queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetQueryDelay(this IComboBox combo, uint queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}
        
		public static IComboBox SetQueryDelay(this IComboBox combo, long queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetQueryDelay(this IComboBox combo, ulong queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}
        
		public static IComboBox SetQueryDelay(this IComboBox combo, short queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetQueryDelay(this IComboBox combo, ushort queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}
        
		public static IComboBox SetQueryDelay(this IComboBox combo, decimal queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}
        
		public static IComboBox SetQueryDelay(this IComboBox combo, float queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}
        
		public static IComboBox SetQueryDelay(this IComboBox combo, double queryDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["queryDelay"] = queryDelay;
            
			return combo;
		}
        
		public static Variant GetQueryDelay(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("queryDelay"))
				return new Variant(combo.UserConfig ["queryDelay"]);
			else
				return null;
		}
        
        #endregion QueryDelay

        #region QueryParam
        
		public static IComboBox SetQueryParam(this IComboBox combo, string queryParam)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(queryParam) && combo.UserConfig.ContainsKey("queryParam"))
				combo.UserConfig.Remove("queryParam");
			else
				combo.UserConfig ["queryParam"] = queryParam;
            
			return combo;
		}
        
		public static string GetQueryParam(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("queryParam"))
				return (string)combo.UserConfig ["queryParam"];
			else
				return null;
		}

        #endregion QueryParam

        #region Resizable
        
		public static IComboBox SetResizable(this IComboBox combo, bool resizable)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["resizable"] = resizable;
            
			return combo;
		}
        
		public static bool GetResizable(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("resizable"))
				return (bool)combo.UserConfig ["resizable"];
			else
				return false;
		}
        
        #endregion Resizable

        #region SelectOnFocus
        
		public static IComboBox SetSelectOnFocus(this IComboBox combo, bool selectOnFocus)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["selectOnFocus"] = selectOnFocus;
            
			return combo;
		}
        
		public static bool GetSelectOnFocus(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("selectOnFocus"))
				return (bool)combo.UserConfig ["selectOnFocus"];
			else
				return false;
		}
        
        #endregion SelectOnFocus

        #region SelectedClass
        
		public static IComboBox SetSelectedClass(this IComboBox combo, string selectedClass)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(selectedClass) && combo.UserConfig.ContainsKey("selectedClass"))
				combo.UserConfig.Remove("selectedClass");
			else
				combo.UserConfig ["selectedClass"] = selectedClass;
            
			return combo;
		}
        
		public static string GetSelectedClass(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("selectedClass"))
				return (string)combo.UserConfig ["selectedClass"];
			else
				return null;
		}

        #endregion SelectedClass        

        #region Shadow
        
		public static IComboBox SetShadow(this IComboBox combo, bool shadow)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
			if (shadow)
			{
				combo.UserConfig ["shadow"] = "sides";
			} else
			{
				if (combo.UserConfig.ContainsKey("shadow"))
				{
					combo.UserConfig.Remove("shadow");
				}
			}
            
			return combo;
		}

		public static IComboBox SetShadow(this IComboBox combo, Shadows  shadow)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			string shdw = Enum.GetName(typeof(Shadows), shadow).ToLowerInvariant();
			combo.UserConfig ["shadow"] = shdw;
            
			return combo;
		}
        
		public static Shadow  GetShadow(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("shadow"))
				return (Shadow)Enum.Parse(typeof(Shadows), (string)combo.UserConfig ["shadow"], true);
			else
				return Shadow.Sides;
		}

        #endregion Shadow

        #region Store
        
		public static IComboBox SetStore(this IComboBox combo, string store)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(store) && combo.UserConfig.ContainsKey("store"))
				combo.UserConfig.Remove("store");
			else
				combo.UserConfig ["store"] = store;
            
			return combo;
		}
        
		public static string GetStore(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("store"))
				return (string)combo.UserConfig ["store"];
			else
				return null;
		}
        
        #endregion Store

        #region SubmitValue
        
		public static IComboBox SetSubmitValue(this IComboBox combo, bool submitValue)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["submitValue"] = submitValue;
            
			return combo;
		}
        
		public static bool GetSubmitValue(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("submitValue"))
				return (bool)combo.UserConfig ["submitValue"];
			else
				return false;
		}
        
        #endregion SubmitValue

        #region Title
        
		public static IComboBox SetTitle(this IComboBox combo, string title)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(title) && combo.UserConfig.ContainsKey("title"))
				combo.UserConfig.Remove("title");
			else
				combo.UserConfig ["title"] = title;
            
			return combo;
		}
        
		public static string GetTitle(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("title"))
				return (string)combo.UserConfig ["title"];
			else
				return null;
		}
        
        #endregion Title

        #region Tpl
        
		public static IComboBox SetTpl(this IComboBox combo, string tpl)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(tpl) && combo.UserConfig.ContainsKey("tpl"))
				combo.UserConfig.Remove("tpl");
			else
				combo.UserConfig ["tpl"] = tpl;
            
			return combo;
		}
        
		public static string GetTpl(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("tpl"))
				return (string)combo.UserConfig ["tpl"];
			else
				return null;
		}
        
        #endregion Tpl

        #region TriggerAction
        
		public static IComboBox SetTriggerAction(this IComboBox combo, string triggerAction)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(triggerAction) && combo.UserConfig.ContainsKey("triggerAction"))
				combo.UserConfig.Remove("triggerAction");
			else
				combo.UserConfig ["triggerAction"] = triggerAction;
            
			return combo;
		}
        
		public static string GetTriggerAction(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("triggerAction"))
				return (string)combo.UserConfig ["triggerAction"];
			else
				return null;
		}

        #endregion TriggerAction

        #region TypeAhead
        
		public static IComboBox SetTypeAhead(this IComboBox combo, bool typeAhead)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAhead"] = typeAhead;
            
			return combo;
		}
        
		public static bool GetTypeAhead(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("typeAhead"))
				return (bool)combo.UserConfig ["typeAhead"];
			else
				return false;
		}
        
        #endregion TypeAhead

        #region TypeAheadDelay
        
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, int typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, uint typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}
        
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, long typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, ulong typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}
        
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, short typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}

        [CLSCompliantAttribute(false)]
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, ushort typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}
        
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, decimal typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}
        
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, float typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}
        
		public static IComboBox SetTypeAheadDelay(this IComboBox combo, double typeAheadDelay)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			combo.UserConfig ["typeAheadDelay"] = typeAheadDelay;
            
			return combo;
		}
        
		public static Variant GetTypeAheadDelay(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("typeAheadDelay"))
				return new Variant(combo.UserConfig ["typeAheadDelay"]);
			else
				return null;
		}
        
        #endregion TypeAheadDelay

        #region ValueField
        
		public static IComboBox SetValueField(this IComboBox combo, string valueField)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(valueField) && combo.UserConfig.ContainsKey("valueField"))
				combo.UserConfig.Remove("valueField");
			else
				combo.UserConfig ["valueField"] = valueField;
            
			return combo;
		}
        
		public static string GetValueField(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("valueField"))
				return (string)combo.UserConfig ["valueField"];
			else
				return null;
		}

        #endregion ValueField

        #region ValueNotFoundText
        
		public static IComboBox SetValueNotFoundText(this IComboBox combo, string valueNotFoundText)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (String.IsNullOrEmpty(valueNotFoundText) && combo.UserConfig.ContainsKey("valueNotFoundText"))
				combo.UserConfig.Remove("valueNotFoundText");
			else
				combo.UserConfig ["valueNotFoundText"] = valueNotFoundText;
            
			return combo;
		}
        
		public static string GetValueNotFoundText(this IComboBox combo)
		{
			if (combo == null)
				throw new ArgumentNullException("combo");
            
			if (combo.UserConfig.ContainsKey("valueNotFoundText"))
				return (string)combo.UserConfig ["valueNotFoundText"];
			else
				return null;
		}
        
        #endregion ValueNotFoundText       

	}       
}
