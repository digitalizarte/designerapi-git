//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
    using System.Text.RegularExpressions;
    using DesignerAPI.Model;
    using DesignerAPI.Components;
    using DesignerAPI.Components.Form;
    using System.Collections.Generic;

    public static class TextFieldHelper
    {
        #region AllowBlank
        
        public static ITextField SetAllowBlank (this ITextField textField, bool allowBlank)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["allowBlank"] = allowBlank;
            
            return textField;
        }

        public static TEntity SetAllowBlank<TEntity> (this ITextField textField, bool allowBlank)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            return (TEntity)textField.SetAllowBlank (allowBlank);
        }
        
        public static bool GetAllowBlank (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("allowBlank"))
                return (bool)textField.UserConfig ["allowBlank"];
            else
                return false;
        }
        
        #endregion AllowBlank

        #region BlankText
        
        public static ITextField SetBlankText (this ITextField textField, string blankText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (blankText) && textField.UserConfig.ContainsKey ("blankText"))
                textField.UserConfig.Remove ("blankText");
            else
                textField.UserConfig ["blankText"] = blankText;
            
            return textField;
        }
        
        public static string GetBlankText (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("blankText"))
                return (string)textField.UserConfig ["blankText"];
            else
                return null;
        }
        
        #endregion BlankText
        
        #region DisableKeyFilter
        
        public static ITextField SetDisableKeyFilter (this ITextField textField, bool disableKeyFilter)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["disableKeyFilter"] = disableKeyFilter;
            
            return textField;
        }
        
        public static bool GetDisableKeyFilter (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("disableKeyFilter"))
                return (bool)textField.UserConfig ["disableKeyFilter"];
            else
                return false;
        }
        
        #endregion DisableKeyFilter

        #region EmptyClass
        
        public static ITextField SetEmptyClass (this ITextField textField, string emptyClass)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (emptyClass) && textField.UserConfig.ContainsKey ("emptyClass"))
                textField.UserConfig.Remove ("emptyClass");
            else
                textField.UserConfig ["emptyClass"] = emptyClass;
            
            return textField;
        }
        
        public static string GetEmptyClass (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("emptyClass"))
                return (string)textField.UserConfig ["emptyClass"];
            else
                return null;
        }
        
        #endregion EmptyClass

        #region EmptyText
        
        public static ITextField SetEmptyText (this ITextField textField, string emptyText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (emptyText) && textField.UserConfig.ContainsKey ("emptyText"))
                textField.UserConfig.Remove ("emptyText");
            else
                textField.UserConfig ["emptyText"] = emptyText;
            
            return textField;
        }

        public static TEntity SetEmptyText<TEntity> (this ITextField textField, string emptyText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            return (TEntity)textField.SetEmptyText (emptyText);
        }
        
        public static string GetEmptyText (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("emptyText"))
                return (string)textField.UserConfig ["emptyText"];
            else
                return null;
        }
        
        #endregion EmptyText        

        #region EnableKeyEvents
        
        public static ITextField SetEnableKeyEvents (this ITextField textField, bool enableKeyEvents)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["enableKeyEvents"] = enableKeyEvents;
            
            return textField;
        }
        
        public static bool GetEnableKeyEvents (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("enableKeyEvents"))
                return (bool)textField.UserConfig ["enableKeyEvents"];
            else
                return false;
        }
        
        #endregion EnableKeyEvents

        #region Grow
        
        public static ITextField SetGrow (this ITextField textField, bool grow)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["grow"] = grow;
            
            return textField;
        }
        
        public static bool GetGrow (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("grow"))
                return (bool)textField.UserConfig ["grow"];
            else
                return false;
        }

        
        #endregion Grow

        #region GrowMax
        
        public static ITextField SetGrowMax (this ITextField textField, int growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetGrowMax (this ITextField textField, uint growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }
        
        public static ITextField SetGrowMax (this ITextField textField, long growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetGrowMax (this ITextField textField, ulong growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }
        
        public static ITextField SetGrowMax (this ITextField textField, short growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetGrowMax (this ITextField textField, ushort growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }
        
        public static ITextField SetGrowMax (this ITextField textField, decimal growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }
        
        public static ITextField SetGrowMax (this ITextField textField, float growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }
        
        public static ITextField SetGrowMax (this ITextField textField, double growMax)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMax"] = growMax;
            
            return textField;
        }

        public static Variant GetGrowMax (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("growMax"))
                return new Variant (textField.UserConfig ["growMax"]);
            else
                return null;
        }
        
        #endregion GrowMax

        #region GrowMin
        
        public static ITextField SetGrowMin (this ITextField textField, int growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetGrowMin (this ITextField textField, uint growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }
        
        public static ITextField SetGrowMin (this ITextField textField, long growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetGrowMin (this ITextField textField, ulong growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }
        
        public static ITextField SetGrowMin (this ITextField textField, short growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetGrowMin (this ITextField textField, ushort growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }
        
        public static ITextField SetGrowMin (this ITextField textField, decimal growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }
        
        public static ITextField SetGrowMin (this ITextField textField, float growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }
        
        public static ITextField SetGrowMin (this ITextField textField, double growMin)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["growMin"] = growMin;
            
            return textField;
        }
        
        public static Variant GetGrowMin (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("growMin"))
                return new Variant (textField.UserConfig ["growMin"]);
            else
                return null;
        }
        
        #endregion GrowMin

        #region MaskRe

        public static ITextField SetMaskRe (this ITextField textField, string maskRe)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (maskRe) && textField.UserConfig.ContainsKey ("maskRe"))
                textField.UserConfig.Remove ("maskRe");
            else {
                if (maskRe.IsValidPattern ())
                    textField.UserConfig ["maskRe"] = maskRe;
                else
                    throw new ArgumentNullException ("maskRe");            
            }
            
            return textField;
        }

        public static ITextField SetMaskRe (this ITextField textField, Regex maskRe)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");

            if (maskRe == null)
                throw new ArgumentNullException ("maskRe");            

            textField.UserConfig ["maskRe"] = maskRe.ToString ();
            
            return textField;
        }
        
        public static Regex GetMaskRe (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("maskRe"))
                return new Regex ((string)textField.UserConfig ["maskRe"], RegexOptions.Compiled);
            else
                return null;
        }
        
        #endregion MaskRe

        #region MaxLength

        public static ITextField SetMaxLength (this ITextField textField, int maxLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["maxLength"] = maxLength;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetMaxLength (this ITextField textField, uint maxLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["maxLength"] = maxLength;
            
            return textField;
        }
        
        public static ITextField SetMaxLength (this ITextField textField, long maxLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["maxLength"] = maxLength;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetMaxLength (this ITextField textField, ulong maxLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["maxLength"] = maxLength;
            
            return textField;
        }
        
        public static ITextField SetMaxLength (this ITextField textField, short maxLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["maxLength"] = maxLength;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetMaxLength (this ITextField textField, ushort maxLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["maxLength"] = maxLength;
            
            return textField;
        }

        public static Variant GetMaxLength (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("maxLength"))
                return new Variant (textField.UserConfig ["maxLength"]);
            else
                return null;
        }

        #endregion MaxLength

        #region MaxLengthText
        
        public static ITextField SetMaxLengthText (this ITextField textField, string maxLengthText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (maxLengthText) && textField.UserConfig.ContainsKey ("maxLengthText"))
                textField.UserConfig.Remove ("maxLengthText");
            else
                textField.UserConfig ["maxLengthText"] = maxLengthText;
            
            return textField;
        }
        
        public static string GetMaxLengthText (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("maxLengthText"))
                return (string)textField.UserConfig ["maxLengthText"];
            else
                return null;
        }
        
        #endregion MaxLengthText
        
        #region MinLength
        
        public static ITextField SetMinLength (this ITextField textField, int minLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["minLength"] = minLength;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetMinLength (this ITextField textField, uint minLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["minLength"] = minLength;
            
            return textField;
        }
        
        public static ITextField SetMinLength (this ITextField textField, long minLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["minLength"] = minLength;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetMinLength (this ITextField textField, ulong minLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["minLength"] = minLength;
            
            return textField;
        }
        
        public static ITextField SetMinLength (this ITextField textField, short minLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["minLength"] = minLength;
            
            return textField;
        }

        [CLSCompliantAttribute(false)]
        public static ITextField SetMinLength (this ITextField textField, ushort minLength)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["minLength"] = minLength;
            
            return textField;
        }

        public static Variant GetMinLength (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("minLength"))
                return new Variant (textField.UserConfig ["minLength"]);
            else
                return null;
        }

        #endregion MinLength

        #region MinLengthText
        
        public static ITextField SetMinLengthText (this ITextField textField, string minLengthText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (minLengthText) && textField.UserConfig.ContainsKey ("minLengthText"))
                textField.UserConfig.Remove ("minLengthText");
            else
                textField.UserConfig ["minLengthText"] = minLengthText;
            
            return textField;
        }
        
        public static string GetMinLengthText (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("minLengthText"))
                return (string)textField.UserConfig ["minLengthText"];
            else
                return null;
        }
        
        #endregion MinLengthText

        #region Regex
        
        public static ITextField SetRegex (this ITextField textField, string regex)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (regex) && textField.UserConfig.ContainsKey ("regex"))
                textField.UserConfig.Remove ("regex");
            else {
                if (regex.IsValidPattern ())
                    textField.UserConfig ["regex"] = regex;
                else
                    throw new ArgumentNullException ("regex");            
            }
            
            return textField;
        }
        
        public static ITextField SetRegex (this ITextField textField, Regex regex)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (regex == null)
                throw new ArgumentNullException ("regex");            
            
            textField.UserConfig ["regex"] = regex.ToString ();
            
            return textField;
        }
        
        public static Regex GetRegex (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("regex"))
                return new Regex ((string)textField.UserConfig ["regex"], RegexOptions.Compiled);
            else
                return null;
        }
        
        #endregion Regex

        #region RegexText
        
        public static ITextField SetRegexText (this ITextField textField, string regexText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (regexText) && textField.UserConfig.ContainsKey ("regexText"))
                textField.UserConfig.Remove ("regexText");
            else
                textField.UserConfig ["regexText"] = regexText;
            
            return textField;
        }
        
        public static string GetRegexText (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("regexText"))
                return (string)textField.UserConfig ["regexText"];
            else
                return null;
        }
        
        #endregion RegexText

        #region SelectOnFocus
        
        public static ITextField SetSelectOnFocus (this ITextField textField, bool selectOnFocus)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            textField.UserConfig ["selectOnFocus"] = selectOnFocus;
            
            return textField;
        }
        
        public static bool GetSelectOnFocus (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("selectOnFocus"))
                return (bool)textField.UserConfig ["selectOnFocus"];
            else
                return false;
        }
        
        #endregion SelectOnFocus

        #region StripCharsRe
        
        public static ITextField SetStripCharsRe (this ITextField textField, string stripCharsRe)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (stripCharsRe) && textField.UserConfig.ContainsKey ("stripCharsRe"))
                textField.UserConfig.Remove ("stripCharsRe");
            else {
                if (stripCharsRe.IsValidPattern ())
                    textField.UserConfig ["stripCharsRe"] = stripCharsRe;
                else
                    throw new ArgumentNullException ("stripCharsRe");            
            }
            
            return textField;
        }
        
        public static ITextField SetStripCharsRe (this ITextField textField, Regex stripCharsRe)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (stripCharsRe == null)
                throw new ArgumentNullException ("stripCharsRe");            
            
            textField.UserConfig ["stripCharsRe"] = stripCharsRe.ToString ();
            
            return textField;
        }
        
        public static Regex GetStripCharsRe (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("stripCharsRe"))
                return new Regex ((string)textField.UserConfig ["stripCharsRe"], RegexOptions.Compiled);
            else
                return null;
        }
        
        #endregion StripCharsRe

        #region VType
        
        public static ITextField SetVType (this ITextField textField, string vtype)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (vtype) && textField.UserConfig.ContainsKey ("vtype"))
                textField.UserConfig.Remove ("vtype");
            else
                textField.UserConfig ["vtype"] = vtype;
            
            return textField;
        }

        public static TEntity SetVType<TEntity> (this ITextField textField, string vtype)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            return (TEntity)textField.SetVType (vtype);
        }

        public static ITextField SetVType (this ITextField textField, VTypes vtype)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");

            string value = Enum.GetName (typeof(VTypes), vtype).ToLowerInvariant ();
            return textField.SetVType (value);
        }

        public static TEntity SetVType<TEntity> (this ITextField textField, VTypes vtype)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            return (TEntity)textField.SetVType (vtype);
        }       
        
        public static string GetVType (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("vtype"))
                return (string)textField.UserConfig ["vtype"];
            else
                return null;
        }
        
        #endregion VType
        
        #region VTypeText
        
        public static ITextField SetVTypeText (this ITextField textField, string vtypeText)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (String.IsNullOrEmpty (vtypeText) && textField.UserConfig.ContainsKey ("vtypeText"))
                textField.UserConfig.Remove ("vtypeText");
            else
                textField.UserConfig ["vtypeText"] = vtypeText;
            
            return textField;
        }
        
        public static string GetVTypeText (this ITextField textField)
        {
            if (textField == null)
                throw new ArgumentNullException ("textField");
            
            if (textField.UserConfig.ContainsKey ("vtypeText"))
                return (string)textField.UserConfig ["vtypeText"];
            else
                return null;
        }
        
        #endregion VTypeText
               
    }
}
