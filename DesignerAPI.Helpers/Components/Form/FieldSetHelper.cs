//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	using System.Collections.Generic;

	public static class FieldSetHelper
	{
		public static IFieldSet SetAnimCollapse(this IFieldSet fieldSet, bool animCollapse)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			fieldSet.UserConfig ["animCollapse"] = animCollapse;
			
			
			return fieldSet;
		}
		
		public static bool GetAnimCollapse(this IFieldSet fieldSet)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (fieldSet.UserConfig.ContainsKey("animCollapse"))
				return (bool)fieldSet.UserConfig ["animCollapse"];
			else
				return false;
		}

		public static IFieldSet SetCheckboxName(this IFieldSet fieldSet, string checkboxName)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (String.IsNullOrEmpty(checkboxName) && fieldSet.UserConfig.ContainsKey("checkboxName"))
				fieldSet.UserConfig.Remove("checkboxName");
			else
				fieldSet.UserConfig ["checkboxName"] = checkboxName;
			
			return fieldSet;
		}
		
		public static string GetCheckboxName(this IFieldSet fieldSet)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (fieldSet.UserConfig.ContainsKey("checkboxName"))
				return (string)fieldSet.UserConfig ["checkboxName"];
			else
				return null;
		}

		public static IFieldSet SetCheckboxToggle(this IFieldSet fieldSet, string checkboxToggle)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (String.IsNullOrEmpty(checkboxToggle) && fieldSet.UserConfig.ContainsKey("checkboxToggle"))
				fieldSet.UserConfig.Remove("checkboxToggle");
			else
				fieldSet.UserConfig ["checkboxToggle"] = checkboxToggle;
			
			return fieldSet;
		}

		public static IFieldSet SetCheckboxToggle(this IFieldSet fieldSet, object checkboxToggle)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (checkboxToggle == null && fieldSet.UserConfig.ContainsKey("checkboxToggle"))
				fieldSet.UserConfig.Remove("checkboxToggle");
			else
				fieldSet.UserConfig ["checkboxToggle"] = checkboxToggle;
			
			return fieldSet;
		}

		public static Variant GetCheckboxToggle(this IFieldSet fieldSet)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (fieldSet.UserConfig.ContainsKey("checkboxToggle"))
				return new Variant(fieldSet.UserConfig ["checkboxToggle"]);
			else
				return null;
		}

		public static IFieldSet SetItemCls(this IFieldSet fieldSet, string itemCls)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (String.IsNullOrEmpty(itemCls) && fieldSet.UserConfig.ContainsKey("itemCls"))
				fieldSet.UserConfig.Remove("itemCls");
			else
				fieldSet.UserConfig ["itemCls"] = itemCls;
			
			return fieldSet;
		}
		
		public static string GetItemCls(this IFieldSet fieldSet)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (fieldSet.UserConfig.ContainsKey("itemCls"))
				return (string)fieldSet.UserConfig ["itemCls"];
			else
				return null;
		}

		public static IFieldSet SetLayout(this IFieldSet fieldSet, string layout)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (String.IsNullOrEmpty(layout) && fieldSet.UserConfig.ContainsKey("layout"))
				fieldSet.UserConfig.Remove("layout");
			else
				fieldSet.UserConfig ["layout"] = layout;
			
			return fieldSet;
		}
		
		public static string GetLayout(this IFieldSet fieldSet)
		{
			if (fieldSet == null)
				throw new ArgumentNullException("fieldSet");
			
			if (fieldSet.UserConfig.ContainsKey("layout"))
				return (string)fieldSet.UserConfig ["layout"];
			else
				return null;
		}
	}			
}
