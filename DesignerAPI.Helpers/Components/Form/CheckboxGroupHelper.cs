//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
    
	using System.Collections.Generic;

	public static class CheckboxGroupHelper
	{

        #region AllowBlank
        
		public static ICheckboxGroup SetAllowBlank(this ICheckboxGroup checkboxGroup, bool allowBlank)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["allowBlank"] = allowBlank;
            
			return checkboxGroup;
		}
        
		public static bool GetAllowBlank(this ICheckboxGroup checkboxGroup)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (checkboxGroup.UserConfig.ContainsKey("allowBlank"))
				return (bool)checkboxGroup.UserConfig ["allowBlank"];
			else
				return false;
		}
        
        #endregion AllowBlank

        #region BlankText
        
		public static ICheckboxGroup SetBlankText(this ICheckboxGroup checkboxGroup, string blankText)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (String.IsNullOrEmpty(blankText) && checkboxGroup.UserConfig.ContainsKey("blankText"))
				checkboxGroup.UserConfig.Remove("blankText");
			else
				checkboxGroup.UserConfig ["blankText"] = blankText;
            
			return checkboxGroup;
		}
        
		public static string GetBlankText(this ICheckboxGroup checkboxGroup)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (checkboxGroup.UserConfig.ContainsKey("blankText"))
				return (string)checkboxGroup.UserConfig ["blankText"];
			else
				return null;
		}
        
        #endregion BlankText

        #region Columns
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, string columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (String.IsNullOrEmpty(columns) && checkboxGroup.UserConfig.ContainsKey("columns"))
				checkboxGroup.UserConfig.Remove("columns");
			else
				checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, int columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

        [CLSCompliantAttribute(false)]
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, uint columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, long columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

        [CLSCompliantAttribute(false)]
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, ulong columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, short columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

        [CLSCompliantAttribute(false)]
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, ushort columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, decimal columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, float columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, double columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

        [CLSCompliantAttribute(false)]
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, uint[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
                
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<uint> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, int[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<int> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

        [CLSCompliantAttribute(false)]
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, ulong[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<ulong> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, long[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<long> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, short[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<short> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

        [CLSCompliantAttribute(false)]
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, ushort[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<ushort> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, byte[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<byte> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, float[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<float> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, double[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<double> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, decimal[] columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}
        
		public static ICheckboxGroup SetColumns(this ICheckboxGroup checkboxGroup, IList<decimal> columns)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (columns == null)
				throw new ArgumentNullException("columns");
            
			checkboxGroup.UserConfig ["columns"] = columns;
            
			return checkboxGroup;
		}

		public static Variant GetColumns(this ICheckboxGroup checkboxGroup)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (checkboxGroup.UserConfig.ContainsKey("columns"))
				return new Variant(checkboxGroup.UserConfig ["columns"]);
			else
				return null;
		}
        
        #endregion Columns        
       
        #region Vertical
        
		public static ICheckboxGroup SetVertical(this ICheckboxGroup checkboxGroup, bool vertical)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			checkboxGroup.UserConfig ["vertical"] = vertical;
            
			return checkboxGroup;
		}
        
		public static bool GetVertical(this ICheckboxGroup checkboxGroup)
		{
			if (checkboxGroup == null)
				throw new ArgumentNullException("checkboxGroup");
            
			if (checkboxGroup.UserConfig.ContainsKey("vertical"))
				return (bool)checkboxGroup.UserConfig ["vertical"];
			else
				return false;
		}
        
        #endregion Vertical
	}
            
}
