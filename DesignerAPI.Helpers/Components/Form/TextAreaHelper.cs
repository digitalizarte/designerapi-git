//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	using System.Collections.Generic;

	public static class TextAreaHelper
	{

        #region AutoCreate
        
		// TODO: autoCreate
        
        #endregion AutoCreate

        #region GrowMax
        
		public static ITextArea SetGrowMax(this ITextArea textArea, int growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}

        [CLSCompliantAttribute(false)]
		public static ITextArea SetGrowMax(this ITextArea textArea, uint growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMax(this ITextArea textArea, long growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}

        [CLSCompliantAttribute(false)]
		public static ITextArea SetGrowMax(this ITextArea textArea, ulong growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMax(this ITextArea textArea, short growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}

        [CLSCompliantAttribute(false)]
		public static ITextArea SetGrowMax(this ITextArea textArea, ushort growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMax(this ITextArea textArea, decimal growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMax(this ITextArea textArea, float growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMax(this ITextArea textArea, double growMax)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMax "] = growMax;
            
			return textArea;
		}
        
		public static Variant GetGrowMax(this ITextArea textArea)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			if (textArea.UserConfig.ContainsKey("growMax "))
				return new Variant(textArea.UserConfig ["growMax "]);
			else
				return null;
		}
        
        #endregion GrowMax

        #region GrowMin
        
		public static ITextArea SetGrowMin(this ITextArea textArea, int growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}

        [CLSCompliantAttribute(false)]
		public static ITextArea SetGrowMin(this ITextArea textArea, uint growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMin(this ITextArea textArea, long growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
        [CLSCompliantAttribute(false)]
        public static ITextArea SetGrowMin(this ITextArea textArea, ulong growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMin(this ITextArea textArea, short growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
        [CLSCompliantAttribute(false)]
        public static ITextArea SetGrowMin(this ITextArea textArea, ushort growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMin(this ITextArea textArea, decimal growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMin(this ITextArea textArea, float growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
		public static ITextArea SetGrowMin(this ITextArea textArea, double growMin)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["growMin "] = growMin;
            
			return textArea;
		}
        
		public static Variant GetGrowMin(this ITextArea textArea)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			if (textArea.UserConfig.ContainsKey("growMin "))
				return new Variant(textArea.UserConfig ["growMin "]);
			else
				return null;
		}
        
        #endregion GrowMin

        #region PreventScrollbars
        
		public static ITextArea SetPreventScrollbars(this ITextArea textArea, bool preventScrollbars)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			textArea.UserConfig ["preventScrollbars"] = preventScrollbars;
            
			return textArea;
		}
        
		public static bool GetPreventScrollbars(this ITextArea textArea)
		{
			if (textArea == null)
				throw new ArgumentNullException("textArea");
            
			if (textArea.UserConfig.ContainsKey("preventScrollbars"))
				return (bool)textArea.UserConfig ["preventScrollbars"];
			else
				return false;
		}
        
        #endregion PreventScrollbars
	}        
}
