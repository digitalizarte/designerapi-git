//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	
	using System.Collections.Generic;

	public static class BasicFormHelper
	{
		public static IBasicForm SetApi(this IBasicForm basicForm, object api)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (api == null && basicForm.UserConfig.ContainsKey("api"))
				basicForm.UserConfig.Remove("api");
			else
				basicForm.UserConfig ["api"] = api;
			
			return basicForm;
		}
		
		public static object GetApi(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("api"))
				return basicForm.UserConfig ["api"];
			else
				return null;
		}

		public static IBasicForm SetBaseParams(this IBasicForm basicForm, object baseParams)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (baseParams == null && basicForm.UserConfig.ContainsKey("baseParams"))
				basicForm.UserConfig.Remove("baseParams");
			else
				basicForm.UserConfig ["baseParams"] = baseParams;
			
			return basicForm;
		}
		
		public static object GetBaseParams(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("baseParams"))
				return basicForm.UserConfig ["baseParams"];
			else
				return null;
		}

		public static IBasicForm SetFileUpload(this IBasicForm basicForm, bool fileUpload)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["fileUpload"] = fileUpload;
			
			
			return basicForm;
		}
		
		public static bool GetFileUpload(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("fileUpload"))
				return (bool)basicForm.UserConfig ["fileUpload"];
			else
				return false;
		}

		public static IBasicForm SetMethod(this IBasicForm basicForm, string method)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (String.IsNullOrEmpty(method) && basicForm.UserConfig.ContainsKey("method"))
				basicForm.UserConfig.Remove("method");
			else
				basicForm.UserConfig ["method"] = method;
			
			return basicForm;
		}
		
		public static string GetMethod(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("method"))
				return (string)basicForm.UserConfig ["method"];
			else
				return null;
		}

		public static IBasicForm SetParamOrder(this IBasicForm basicForm, string paramOrder)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (String.IsNullOrEmpty(paramOrder) && basicForm.UserConfig.ContainsKey("paramOrder"))
				basicForm.UserConfig.Remove("paramOrder");
			else
				basicForm.UserConfig ["paramOrder"] = paramOrder;
			
			return basicForm;
		}

		public static IBasicForm SetParamOrder(this IBasicForm basicForm, string[] paramOrder)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (paramOrder == null && basicForm.UserConfig.ContainsKey("paramOrder"))
				basicForm.UserConfig.Remove("paramOrder");
			else
				basicForm.UserConfig ["paramOrder"] = paramOrder;
			
			return basicForm;
		}

		public static IBasicForm SetParamOrder(this IBasicForm basicForm, IList<string> paramOrder)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (paramOrder == null && basicForm.UserConfig.ContainsKey("paramOrder"))
				basicForm.UserConfig.Remove("paramOrder");
			else
				basicForm.UserConfig ["paramOrder"] = paramOrder;
			
			return basicForm;
		}

		public static Variant GetParamOrder(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("paramOrder"))
				return new Variant(basicForm.UserConfig ["paramOrder"]);
			else
				return null;
		}

		public static IBasicForm SetParamsAsHash(this IBasicForm basicForm, bool paramsAsHash)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["paramsAsHash"] = paramsAsHash;
			
			
			return basicForm;
		}
		
		public static bool GetParamsAsHash(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("paramsAsHash"))
				return (bool)basicForm.UserConfig ["paramsAsHash"];
			else
				return false;
		}

		public static IBasicForm SetStandardSubmit(this IBasicForm basicForm, bool standardSubmit)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["standardSubmit"] = standardSubmit;
			
			
			return basicForm;
		}
		
		public static bool GetStandardSubmit(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("standardSubmit"))
				return (bool)basicForm.UserConfig ["standardSubmit"];
			else
				return false;
		}

		public static IBasicForm SetTimeout(this IBasicForm basicForm, int timeout)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["timeout"] = timeout;
			
			
			return basicForm;
		}
		
        [CLSCompliantAttribute(false)]
		public static IBasicForm SetTimeout(this IBasicForm basicForm, uint timeout)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["timeout"] = timeout;
			
			
			return basicForm;
		}
		
		public static IBasicForm SetTimeout(this IBasicForm basicForm, long timeout)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["timeout"] = timeout;
			
			
			return basicForm;
		}
		
        [CLSCompliantAttribute(false)]
		public static IBasicForm SetTimeout(this IBasicForm basicForm, ulong timeout)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["timeout"] = timeout;
			
			
			return basicForm;
		}
		
		public static IBasicForm SetTimeout(this IBasicForm basicForm, short timeout)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["timeout"] = timeout;
			
			
			return basicForm;
		}
  
        [CLSCompliantAttribute(false)]
		public static IBasicForm SetTimeout(this IBasicForm basicForm, ushort timeout)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["timeout"] = timeout;
			
			
			return basicForm;
		}
				
		public static Variant GetTimeout(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("timeout"))
				return new Variant(basicForm.UserConfig ["timeout"]);
			else
				return null;
		}

		public static IBasicForm SetTrackResetOnLoad(this IBasicForm basicForm, bool trackResetOnLoad)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			basicForm.UserConfig ["trackResetOnLoad"] = trackResetOnLoad;
			
			
			return basicForm;
		}
		
		public static bool GetTrackResetOnLoad(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("trackResetOnLoad"))
				return (bool)basicForm.UserConfig ["trackResetOnLoad"];
			else
				return false;
		}

		public static IBasicForm SetUrl(this IBasicForm basicForm, string url)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (String.IsNullOrEmpty(url) && basicForm.UserConfig.ContainsKey("url"))
				basicForm.UserConfig.Remove("url");
			else
				basicForm.UserConfig ["url"] = url;
			
			return basicForm;
		}
		
		public static string GetUrl(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("url"))
				return (string)basicForm.UserConfig ["url"];
			else
				return null;
		}

		public static IBasicForm SetWaitTitle(this IBasicForm basicForm, string waitTitle)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (String.IsNullOrEmpty(waitTitle) && basicForm.UserConfig.ContainsKey("waitTitle"))
				basicForm.UserConfig.Remove("waitTitle");
			else
				basicForm.UserConfig ["waitTitle"] = waitTitle;
			
			return basicForm;
		}
		
		public static string GetWaitTitle(this IBasicForm basicForm)
		{
			if (basicForm == null)
				throw new ArgumentNullException("basicForm");
			
			if (basicForm.UserConfig.ContainsKey("waitTitle"))
				return (string)basicForm.UserConfig ["waitTitle"];
			else
				return null;
		}
	}	
}
