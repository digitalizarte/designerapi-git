//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Threading;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
    
	using System.Collections.Generic;

	public static class DateFieldHelper
	{
        #region AltFormats
        
		public static IDateField SetAltFormats(this IDateField dateField, string altFormats)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(altFormats) && dateField.UserConfig.ContainsKey("altFormats"))
				dateField.UserConfig.Remove("altFormats");
			else
				dateField.UserConfig ["altFormats"] = altFormats;
            
			return dateField;
		}
        
		public static string GetAltFormats(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("altFormats"))
				return (string)dateField.UserConfig ["altFormats"];
			else
				return null;
		}
        
        #endregion AltFormats

        #region AutoCreate
        
		// TODO: autoCreate
        
        #endregion AutoCreate

        #region DisabledDates
                
		public static IDateField SetDisabledDates(this IDateField dateField, string[] disabledDates)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (disabledDates == null)
				throw new ArgumentNullException("disabledDates");
            
			dateField.UserConfig ["disabledDates"] = new List<string>(disabledDates);
            
			return dateField;
		}
        
		public static IDateField SetDisabledDates(this IDateField dateField, IList<string> disabledDates)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (disabledDates == null)
				throw new ArgumentNullException("disabledDates");
            
			dateField.UserConfig ["disabledDates"] = disabledDates;
            
			return dateField;
		}
        
		public static IList<string> GetDisabledDates(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("disabledDates"))
				return (IList<string>)dateField.UserConfig ["disabledDates"];
			else
				return null;
		}
        
        #endregion DisabledDates

        #region DisabledDatesText
        
		public static IDateField SetDisabledDatesText(this IDateField dateField, string disabledDatesText)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(disabledDatesText) && dateField.UserConfig.ContainsKey("disabledDatesText"))
				dateField.UserConfig.Remove("disabledDatesText");
			else
				dateField.UserConfig ["disabledDatesText"] = disabledDatesText;
            
			return dateField;
		}
        
		public static string GetDisabledDatesText(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("disabledDatesText"))
				return (string)dateField.UserConfig ["disabledDatesText"];
			else
				return null;
		}
        
        #endregion DisabledDatesText

        #region DisabledDays
        
		public static IDateField SetDisabledDays(this IDateField dateField, int[] disabledDays)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (disabledDays == null)
				throw new ArgumentNullException("disabledDays");
            
			dateField.UserConfig ["disabledDays"] = new List<int>(disabledDays);
            
			return dateField;
		}
        
		public static IDateField SetDisabledDays(this IDateField dateField, IList<int> disabledDays)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (disabledDays == null)
				throw new ArgumentNullException("disabledDays");
            
			dateField.UserConfig ["disabledDays"] = disabledDays;
            
			return dateField;
		}
        
		public static IList<int> GetDisabledDays(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("disabledDays"))
				return (IList<int>)dateField.UserConfig ["disabledDays"];
			else
				return null;
		}
        
        #endregion DisabledDays
        
        #region DisabledDaysText
        
		public static IDateField SetDisabledDaysText(this IDateField dateField, string disabledDaysText)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(disabledDaysText) && dateField.UserConfig.ContainsKey("disabledDaysText"))
				dateField.UserConfig.Remove("disabledDaysText");
			else
				dateField.UserConfig ["disabledDaysText"] = disabledDaysText;
            
			return dateField;
		}
        
		public static string GetDisabledDaysText(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("disabledDaysText"))
				return (string)dateField.UserConfig ["disabledDaysText"];
			else
				return null;
		}
        
        #endregion DisabledDaysText

        #region Format
        
		public static IDateField SetFormat(this IDateField dateField, string format)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(format) && dateField.UserConfig.ContainsKey("format"))
				dateField.UserConfig.Remove("format");
			else
				dateField.UserConfig ["format"] = format;
            
			return dateField;
		}
        
		public static string GetFormat(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("format"))
				return (string)dateField.UserConfig ["format"];
			else
				return null;
		}
        
        #endregion Format   

        #region InvalidText
        
		public static IDateField SetInvalidText(this IDateField dateField, string invalidText)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(invalidText) && dateField.UserConfig.ContainsKey("invalidText"))
				dateField.UserConfig.Remove("invalidText");
			else
				dateField.UserConfig ["invalidText"] = invalidText;
            
			return dateField;
		}
        
		public static string GetInvalidText(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("invalidText"))
				return (string)dateField.UserConfig ["invalidText"];
			else
				return null;
		}
                
        #endregion InvalidText

        #region MaxText
        
		public static IDateField SetMaxText(this IDateField dateField, string maxText)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(maxText) && dateField.UserConfig.ContainsKey("maxText"))
				dateField.UserConfig.Remove("maxText");
			else
				dateField.UserConfig ["maxText"] = maxText;
            
			return dateField;
		}
        
		public static string GetMaxText(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("maxText"))
				return (string)dateField.UserConfig ["maxText"];
			else
				return null;
		}

        #endregion MaxText

        #region MaxValue
        
		public static IDateField SetMaxValue(this IDateField dateField, string maxValue, IFormatProvider formatProvider = null)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(maxValue) && dateField.UserConfig.ContainsKey("maxValue"))
				dateField.UserConfig.Remove("maxValue");
			else
				dateField.UserConfig ["maxValue"] = DateTime.Parse(maxValue, formatProvider ?? Thread.CurrentThread.CurrentCulture.DateTimeFormat);
            
			return dateField;
		}

		public static IDateField SetMaxValue(this IDateField dateField, DateTime maxValue)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			dateField.UserConfig ["maxValue"] = maxValue;
            
			return dateField;
		}
        
		public static DateTime? GetMaxValue(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("maxValue"))
				return (DateTime)dateField.UserConfig ["maxValue"];
			else
				return null;
		}
        
        #endregion MaxValue


        #region MinText
        
		public static IDateField SetMinText(this IDateField dateField, string minText)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(minText) && dateField.UserConfig.ContainsKey("minText"))
				dateField.UserConfig.Remove("minText");
			else
				dateField.UserConfig ["minText"] = minText;
            
			return dateField;
		}
        
		public static string GetMinText(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("minText"))
				return (string)dateField.UserConfig ["minText"];
			else
				return null;
		}
        
        #endregion MinText
        
        #region MinValue
        
		public static IDateField SetMinValue(this IDateField dateField, string minValue, IFormatProvider formatProvider = null)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (String.IsNullOrEmpty(minValue) && dateField.UserConfig.ContainsKey("minValue"))
				dateField.UserConfig.Remove("minValue");
			else
				dateField.UserConfig ["minValue"] = DateTime.Parse(minValue, formatProvider ?? Thread.CurrentThread.CurrentCulture.DateTimeFormat);
            
			return dateField;
		}
        
		public static IDateField SetMinValue(this IDateField dateField, DateTime minValue)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            			
			dateField.UserConfig ["minValue"] = minValue;
            
			return dateField;
		}
        
		public static DateTime? GetMinValue(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("minValue"))
				return (DateTime)dateField.UserConfig ["minValue"];
			else
				return null;
		}
        
        #endregion MinValue

        #region ShowToday

		public static IDateField SetShowToday(this IDateField dateField, bool showToday)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			dateField.UserConfig ["showToday"] = showToday;
            
			return dateField;
		}
        
		public static bool GetShowToday(this IDateField dateField)
		{
			if (dateField == null)
				throw new ArgumentNullException("dateField");
            
			if (dateField.UserConfig.ContainsKey("showToday"))
				return (bool)dateField.UserConfig ["showToday"];
			else
				return false;
		}        
        
        #endregion ShowToday

	}        
}
