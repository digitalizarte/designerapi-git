//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	using System.Collections.Generic;

	public static class TriggerFieldHelper
	{
        #region AutoCreate
        
		// TODO: autoCreate
        
        #endregion AutoCreate

        #region Editable
        
		public static ITriggerField SetEditable(this ITriggerField triggerField, bool editable)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			triggerField.UserConfig ["editable"] = editable;
            
			return triggerField;
		}
        
		public static bool GetEditable(this ITriggerField triggerField)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (triggerField.UserConfig.ContainsKey("editable"))
				return (bool)triggerField.UserConfig ["editable"];
			else
				return false;
		}
        
        #endregion Editable

        #region HideTrigger
        
		public static ITriggerField SetHideTrigger(this ITriggerField triggerField, bool hideTrigger)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			triggerField.UserConfig ["hideTrigger"] = hideTrigger;
            
			return triggerField;
		}
        
		public static bool GetHideTrigger(this ITriggerField triggerField)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (triggerField.UserConfig.ContainsKey("hideTrigger"))
				return (bool)triggerField.UserConfig ["hideTrigger"];
			else
				return false;
		}
        
        #endregion HideTrigger

        #region ReadOnly
        
		public static ITriggerField SetReadOnly(this ITriggerField triggerField, bool readOnly)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			triggerField.UserConfig ["readOnly"] = readOnly;
            
			return triggerField;
		}
        
		public static bool GetReadOnly(this ITriggerField triggerField)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (triggerField.UserConfig.ContainsKey("readOnly"))
				return (bool)triggerField.UserConfig ["readOnly"];
			else
				return false;
		}
        
        #endregion ReadOnly

        #region TriggerClass
        
		public static ITriggerField SetTriggerClass(this ITriggerField triggerField, string triggerClass)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (String.IsNullOrEmpty(triggerClass) && triggerField.UserConfig.ContainsKey("triggerClass"))
				triggerField.UserConfig.Remove("triggerClass");
			else
				triggerField.UserConfig ["triggerClass"] = triggerClass;
            
			return triggerField;
		}
        
		public static string GetTriggerClass(this ITriggerField triggerField)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (triggerField.UserConfig.ContainsKey("triggerClass"))
				return (string)triggerField.UserConfig ["triggerClass"];
			else
				return null;
		}
        
        #endregion TriggerClass

        #region TriggerConfig
        
		// TODO: TriggerConfig
        
        #endregion TriggerConfig

        #region WrapFocusClass
        
		public static ITriggerField SetWrapFocusClass(this ITriggerField triggerField, string wrapFocusClass)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (String.IsNullOrEmpty(wrapFocusClass) && triggerField.UserConfig.ContainsKey("wrapFocusClass"))
				triggerField.UserConfig.Remove("wrapFocusClass");
			else
				triggerField.UserConfig ["wrapFocusClass"] = wrapFocusClass;
            
			return triggerField;
		}
        
		public static string GetWrapFocusClass(this ITriggerField triggerField)
		{
			if (triggerField == null)
				throw new ArgumentNullException("triggerField");
            
			if (triggerField.UserConfig.ContainsKey("wrapFocusClass"))
				return (string)triggerField.UserConfig ["wrapFocusClass"];
			else
				return null;
		}
        
        #endregion WrapFocusClass        

	}	
}
