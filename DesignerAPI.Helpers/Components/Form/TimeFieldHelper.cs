//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Threading;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	using System.Collections.Generic;

	public static class TimeFieldHelper
	{
    
        #region AltFormats
        
		public static ITimeField SetAltFormats(this ITimeField timeField, string altFormats)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (String.IsNullOrEmpty(altFormats) && timeField.UserConfig.ContainsKey("altFormats"))
				timeField.UserConfig.Remove("altFormats");
			else
				timeField.UserConfig ["altFormats"] = altFormats;
            
			return timeField;
		}
        
		public static string GetAltFormats(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("altFormats"))
				return (string)timeField.UserConfig ["altFormats"];
			else
				return null;
		}
        
        #endregion AltFormats
        
        #region Format
        
		public static ITimeField SetFormat(this ITimeField timeField, string format)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (String.IsNullOrEmpty(format) && timeField.UserConfig.ContainsKey("format"))
				timeField.UserConfig.Remove("format");
			else
				timeField.UserConfig ["format"] = format;
            
			return timeField;
		}
        
		public static string GetFormat(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("format"))
				return (string)timeField.UserConfig ["format"];
			else
				return null;
		}
        
        #endregion Format

        #region Increment
        
		public static ITimeField SetIncrement(this ITimeField timeField, int increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}

        [CLSCompliantAttribute(false)]
		public static ITimeField SetIncrement(this ITimeField timeField, uint increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
		public static ITimeField SetIncrement(this ITimeField timeField, long increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
        [CLSCompliantAttribute(false)]
        public static ITimeField SetIncrement(this ITimeField timeField, ulong increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
		public static ITimeField SetIncrement(this ITimeField timeField, short increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}

        [CLSCompliantAttribute(false)]
		public static ITimeField SetIncrement(this ITimeField timeField, ushort increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
		public static ITimeField SetIncrement(this ITimeField timeField, decimal increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
		public static ITimeField SetIncrement(this ITimeField timeField, float increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
		public static ITimeField SetIncrement(this ITimeField timeField, double increment)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["increment "] = increment;
            
			return timeField;
		}
        
		public static Variant GetIncrement(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("increment "))
				return new Variant(timeField.UserConfig ["increment "]);
			else
				return null;
		}
        
        #endregion Increment

        #region MaxText
        
		public static ITimeField SetMaxText(this ITimeField timeField, string maxText)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (String.IsNullOrEmpty(maxText) && timeField.UserConfig.ContainsKey("maxText"))
				timeField.UserConfig.Remove("maxText");
			else
				timeField.UserConfig ["maxText"] = maxText;
            
			return timeField;
		}
        
		public static string GetMaxText(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("maxText"))
				return (string)timeField.UserConfig ["maxText"];
			else
				return null;
		}
        
        #endregion MaxText
        
        #region MaxValue
        
		public static ITimeField SetMaxValue(this ITimeField timeField, string maxValue, IFormatProvider formatProvider = null)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (String.IsNullOrEmpty(maxValue) && timeField.UserConfig.ContainsKey("maxValue"))
				timeField.UserConfig.Remove("maxValue");
			else
				timeField.UserConfig ["maxValue"] = DateTime.Parse(maxValue, formatProvider ?? Thread.CurrentThread.CurrentCulture.DateTimeFormat);
            
			return timeField;
		}
        
		public static ITimeField SetMaxValue(this ITimeField timeField, DateTime maxValue)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["maxValue"] = maxValue;
            
			return timeField;
		}
        
		public static DateTime? GetMaxValue(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("maxValue"))
				return (DateTime)timeField.UserConfig ["maxValue"];
			else
				return null;
		}
        
        #endregion MaxValue
        
        
        #region MinText
        
		public static ITimeField SetMinText(this ITimeField timeField, string minText)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (String.IsNullOrEmpty(minText) && timeField.UserConfig.ContainsKey("minText"))
				timeField.UserConfig.Remove("minText");
			else
				timeField.UserConfig ["minText"] = minText;
            
			return timeField;
		}
        
		public static string GetMinText(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("minText"))
				return (string)timeField.UserConfig ["minText"];
			else
				return null;
		}
        
        #endregion MinText
        
        #region MinValue
        
		public static ITimeField SetMinValue(this ITimeField timeField, string minValue, IFormatProvider formatProvider = null)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (String.IsNullOrEmpty(minValue) && timeField.UserConfig.ContainsKey("minValue"))
				timeField.UserConfig.Remove("minValue");
			else
				timeField.UserConfig ["minValue"] = DateTime.Parse(minValue, formatProvider ?? Thread.CurrentThread.CurrentCulture.DateTimeFormat);
            
			return timeField;
		}
        
		public static ITimeField SetMinValue(this ITimeField timeField, DateTime minValue)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			timeField.UserConfig ["minValue"] = minValue;
            
			return timeField;
		}
        
		public static DateTime? GetMinValue(this ITimeField timeField)
		{
			if (timeField == null)
				throw new ArgumentNullException("timeField");
            
			if (timeField.UserConfig.ContainsKey("minValue"))
				return (DateTime)timeField.UserConfig ["minValue"];
			else
				return null;
		}

        #endregion MinValue
	}        
}
