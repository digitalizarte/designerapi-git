//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Form
{
    using System;
	using System.Text.RegularExpressions;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Form;
	using System.Collections.Generic;

	public static class NumberFieldHelper
	{

        #region AllowDecimals
        
		public static INumberField SetAllowDecimals(this INumberField numberField, bool allowDecimals)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["allowDecimals"] = allowDecimals;
            
			return numberField;
		}
        
		public static bool GetAllowDecimals(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("allowDecimals"))
				return (bool)numberField.UserConfig ["allowDecimals"];
			else
				return false;
		}
        
        #endregion AllowDecimals

        #region AllowNegative
        
		public static INumberField SetAllowNegative(this INumberField numberField, bool allowNegative)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["allowNegative"] = allowNegative;
            
			return numberField;
		}
        
		public static bool GetAllowNegative(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("allowNegative"))
				return (bool)numberField.UserConfig ["allowNegative"];
			else
				return false;
		}
        
        #endregion AllowNegative

        #region BaseChars
        
		public static INumberField SetBaseChars(this INumberField numberField, string baseChars)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (String.IsNullOrEmpty(baseChars) && numberField.UserConfig.ContainsKey("baseChars"))
				numberField.UserConfig.Remove("baseChars");
			else
				numberField.UserConfig ["baseChars"] = baseChars;
            
			return numberField;
		}
        
		public static string GetBaseChars(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("baseChars"))
				return (string)numberField.UserConfig ["baseChars"];
			else
				return null;
		}
        
        #endregion BaseChars

        #region DecimalPrecision
        
		public static INumberField SetDecimalPrecision(this INumberField numberField, int decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}

        [CLSCompliantAttribute(false)]
		public static INumberField SetDecimalPrecision(this INumberField numberField, uint decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
		public static INumberField SetDecimalPrecision(this INumberField numberField, long decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
        [CLSCompliantAttribute(false)]
        public static INumberField SetDecimalPrecision(this INumberField numberField, ulong decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
		public static INumberField SetDecimalPrecision(this INumberField numberField, short decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
        [CLSCompliantAttribute(false)]
        public static INumberField SetDecimalPrecision(this INumberField numberField, ushort decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
		public static INumberField SetDecimalPrecision(this INumberField numberField, decimal decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
		public static INumberField SetDecimalPrecision(this INumberField numberField, float decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
		public static INumberField SetDecimalPrecision(this INumberField numberField, double decimalPrecision)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return numberField;
		}
        
		public static Variant GetDecimalPrecision(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("decimalPrecision "))
				return new Variant(numberField.UserConfig ["decimalPrecision "]);
			else
				return null;
		}
        
        #endregion DecimalPrecision


        #region DecimalSeparator
        
		public static INumberField SetDecimalSeparator(this INumberField numberField, string decimalSeparator)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (String.IsNullOrEmpty(decimalSeparator) && numberField.UserConfig.ContainsKey("decimalSeparator"))
				numberField.UserConfig.Remove("decimalSeparator");
			else
				numberField.UserConfig ["decimalSeparator"] = decimalSeparator;
            
			return numberField;
		}
        
		public static string GetDecimalSeparator(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("decimalSeparator"))
				return (string)numberField.UserConfig ["decimalSeparator"];
			else
				return null;
		}
        
        #endregion DecimalSeparator       

        #region FieldClass
        
		public static INumberField SetFieldClass(this INumberField numberField, string fieldClass)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (String.IsNullOrEmpty(fieldClass) && numberField.UserConfig.ContainsKey("fieldClass"))
				numberField.UserConfig.Remove("fieldClass");
			else
				numberField.UserConfig ["fieldClass"] = fieldClass;
            
			return numberField;
		}
        
		public static string GetFieldClass(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("fieldClass"))
				return (string)numberField.UserConfig ["fieldClass"];
			else
				return null;
		}
        
        #endregion FieldClass

        #region MaxText
        
		public static INumberField SetMaxText(this INumberField numberField, string maxText)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (String.IsNullOrEmpty(maxText) && numberField.UserConfig.ContainsKey("maxText"))
				numberField.UserConfig.Remove("maxText");
			else
				numberField.UserConfig ["maxText"] = maxText;
            
			return numberField;
		}
        
		public static string GetMaxText(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("maxText"))
				return (string)numberField.UserConfig ["maxText"];
			else
				return null;
		}
        
        #endregion MaxText

        #region MaxValue
        
		public static INumberField SetMaxValue(this INumberField numberField, int maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
        [CLSCompliantAttribute(false)]
        public static INumberField SetMaxValue(this INumberField numberField, uint maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
		public static INumberField SetMaxValue(this INumberField numberField, long maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}

        [CLSCompliantAttribute(false)]
		public static INumberField SetMaxValue(this INumberField numberField, ulong maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
		public static INumberField SetMaxValue(this INumberField numberField, short maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
        [CLSCompliantAttribute(false)]
        public static INumberField SetMaxValue(this INumberField numberField, ushort maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
		public static INumberField SetMaxValue(this INumberField numberField, decimal maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
		public static INumberField SetMaxValue(this INumberField numberField, float maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
		public static INumberField SetMaxValue(this INumberField numberField, double maxValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["maxValue "] = maxValue;
            
			return numberField;
		}
        
		public static Variant GetMaxValue(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("maxValue "))
				return new Variant(numberField.UserConfig ["maxValue "]);
			else
				return null;
		}
        
        #endregion MaxValue

        #region MinText
        
		public static INumberField SetMinText(this INumberField numberField, string minText)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (String.IsNullOrEmpty(minText) && numberField.UserConfig.ContainsKey("minText"))
				numberField.UserConfig.Remove("minText");
			else
				numberField.UserConfig ["minText"] = minText;
            
			return numberField;
		}
        
		public static string GetMinText(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("minText"))
				return (string)numberField.UserConfig ["minText"];
			else
				return null;
		}
        
        #endregion MinText
        
        #region MinValue
        
		public static INumberField SetMinValue(this INumberField numberField, int minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}

        [CLSCompliantAttribute(false)]
		public static INumberField SetMinValue(this INumberField numberField, uint minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}
        
		public static INumberField SetMinValue(this INumberField numberField, long minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}

        [CLSCompliantAttribute(false)]
		public static INumberField SetMinValue(this INumberField numberField, ulong minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}
        
		public static INumberField SetMinValue(this INumberField numberField, short minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}

        [CLSCompliantAttribute(false)]
		public static INumberField SetMinValue(this INumberField numberField, ushort minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}
        
		public static INumberField SetMinValue(this INumberField numberField, decimal minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}
        
		public static INumberField SetMinValue(this INumberField numberField, float minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}
        
		public static INumberField SetMinValue(this INumberField numberField, double minValue)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			numberField.UserConfig ["minValue "] = minValue;
            
			return numberField;
		}
        
		public static Variant GetMinValue(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("minValue "))
				return new Variant(numberField.UserConfig ["minValue "]);
			else
				return null;
		}
        
        #endregion MinValue

        #region NanText
        
		public static INumberField SetNanText(this INumberField numberField, string nanText)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (String.IsNullOrEmpty(nanText) && numberField.UserConfig.ContainsKey("nanText"))
				numberField.UserConfig.Remove("nanText");
			else
				numberField.UserConfig ["nanText"] = nanText;
            
			return numberField;
		}
        
		public static string GetNanText(this INumberField numberField)
		{
			if (numberField == null)
				throw new ArgumentNullException("numberField");
            
			if (numberField.UserConfig.ContainsKey("nanText"))
				return (string)numberField.UserConfig ["nanText"];
			else
				return null;
		}
        
        #endregion NanText
	}    
}
