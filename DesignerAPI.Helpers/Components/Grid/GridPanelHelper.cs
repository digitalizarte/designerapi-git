//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Grid
{
    using System;
    using System.Linq;
    using DesignerAPI.Model;
    using DesignerAPI.Components;
    using DesignerAPI.Components.Grid;
    using DesignerAPI.Components.Toolbar;
    using DesignerAPI.Builder;
    using System.Collections.Generic;

    public static class GridPanelHelper
    {  
        public static IPagingToolbar AddPagingToolbar  (this IGridPanel grid)
        {       
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            IPagingToolbar paging = new PagingToolbar();
            grid.Components.Add (paging);
            ProjectBuilderHelper.LatestComponent.Push (grid);
            
            return  paging;
        }

        public static ICheckboxSelectionModel AddCheckboxSelectionModel (this IGridPanel grid)
        {       
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            ICheckboxSelectionModel selectionModel = new CheckboxSelectionModel();
            grid.Components.Add (selectionModel);
            ProjectBuilderHelper.LatestComponent.Push (grid);
            
            return  selectionModel;
        }

        public static ICellSelectionModel AddCellSelectionModel (this IGridPanel grid)
        {       
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            ICellSelectionModel selectionModel = new CellSelectionModel();
            grid.Components.Add (selectionModel);
            ProjectBuilderHelper.LatestComponent.Push (grid);
            
            return  selectionModel;
        }

        public static IRowSelectionModel AddRowSelectionModel (this IGridPanel grid)
        {       
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            IRowSelectionModel selectionModel = new RowSelectionModel ();
            grid.Components.Add (selectionModel);
            ProjectBuilderHelper.LatestComponent.Push (grid);
            
            return  selectionModel;
        }

        public static IGridView AddGridView (this IGridPanel grid)
        {       
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            IGridView gridView = new GridView ();
            grid.Components.Add (gridView);
            ProjectBuilderHelper.LatestComponent.Push (grid);
            
            return  gridView;
        }

           #region Column
        
        public static IBooleanColumn AddBooleanColumn (this IGridPanel grid)
        {       
            if (grid == null)
                throw new ArgumentNullException ("grid");

            IBooleanColumn column = new BooleanColumn ();
            grid.Components.Add (column);
            ProjectBuilderHelper.LatestComponent.Push (grid);

            return  column;
        }

        public static IColumn AddColumn (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            IColumn column = new Column ();
            grid.Components.Add (column);
            ProjectBuilderHelper.LatestComponent.Push (grid);

            return  column;
        }

        public static IDateColumn AddDateColumn (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            IDateColumn column = new DateColumn ();
            grid.Components.Add (column);
            ProjectBuilderHelper.LatestComponent.Push (grid);

            return  column;
        }

        public static INumberColumn AddNumberColumn (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            INumberColumn column = new NumberColumn ();
            grid.Components.Add (column);
            ProjectBuilderHelper.LatestComponent.Push (grid);

            return  column;
        }

        #endregion Column


        #region AutoExpandColumn
        public static IGridPanel SetAutoExpandColumn (this IGridPanel grid, string autoExpandColumn)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            if (String.IsNullOrEmpty (autoExpandColumn) && grid.UserConfig.ContainsKey ("autoExpandColumn"))
                grid.UserConfig.Remove ("autoExpandColumn");
            else
                grid.UserConfig ["autoExpandColumn"] = autoExpandColumn;
            
            return grid;
        }
        
        public static string GetAutoExpandColumn (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            if (grid.UserConfig.ContainsKey ("autoExpandColumn"))
                return (string)grid.UserConfig ["autoExpandColumn"];
            else
                return null;
        }
        #endregion AutoExpandColumn

        #region AutoExpandMax
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, int autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        [CLSCompliant(false)]
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, uint autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, long autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        [CLSCompliant(false)]
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, ulong autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, short autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        [CLSCompliant(false)]
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, ushort autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, decimal autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, float autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMax (this IGridPanel grid, double autoExpandMax)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMax"] = autoExpandMax;
            
            return grid;
        }
                
        public static Variant GetAutoExpandMax (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            if (grid.UserConfig.ContainsKey ("autoExpandMax"))
                return new Variant (grid.UserConfig ["autoExpandMax"]);
            else
                return null;
        }

        #endregion AutoExpandMax

        #region AutoExpandMin
        
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, int autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        [CLSCompliant(false)]
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, uint autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, long autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        [CLSCompliant(false)]
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, ulong autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, short autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        [CLSCompliant(false)]
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, ushort autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, decimal autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, float autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }
        
        public static IGridPanel SetAutoExpandMin (this IGridPanel grid, double autoExpandMin)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            grid.UserConfig ["autoExpandMin"] = autoExpandMin;
            
            return grid;
        }

        public static Variant GetAutoExpandMin (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            if (grid.UserConfig.ContainsKey ("autoExpandMin"))
                return new Variant (grid.UserConfig ["autoExpandMin"]);
            else
                return null;
        }
        
        #endregion AutoExpandMin

        #region BubbleEvents
        
        public static IGridPanel SetBubbleEvents (this IGridPanel grid, string[] bubbleEvents)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
                                    
            if (bubbleEvents == null) {
                if (grid.UserConfig.ContainsKey ("bubbleEvents")) {
                    grid.UserConfig.Remove ("bubbleEvents");
                } else {
                    throw new ArgumentNullException ("bubbleEvents");
                }
            } else {
                grid.UserConfig ["bubbleEvents"] = bubbleEvents;
            }
            
            return grid;
        }
        
        public static IGridPanel SetBubbleEvents (this IGridPanel grid, IList<string> bubbleEvents)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");

            if (bubbleEvents == null) {
                if (grid.UserConfig.ContainsKey ("bubbleEvents")) {
                    grid.UserConfig.Remove ("bubbleEvents");
                } else {
                    throw new ArgumentNullException ("bubbleEvents");
                }
            } else {
                grid.UserConfig ["bubbleEvents"] = new List<string> (bubbleEvents);
            }
            
            return grid;
        }
        
        public static IList<string> GetBubbleEvents (this IGridPanel grid)
        {
            if (grid == null)
                throw new ArgumentNullException ("grid");
            
            if (grid.UserConfig.ContainsKey ("bubbleEvents"))
                return (IList<string>)grid.UserConfig ["bubbleEvents"];
            else
                return null;

        }
        
        #endregion BubbleEvents

    }
}
