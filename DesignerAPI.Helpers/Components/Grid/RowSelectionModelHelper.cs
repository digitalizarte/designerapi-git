//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Grid
{
	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Grid;
	using System.Collections.Generic;

	public static class RowSelectionModelHelper
	{
		#region MoveEditorOnEnter

		
		public static IRowSelectionModel SetMoveEditorOnEnter(this IRowSelectionModel rowSelectionModel, bool moveEditorOnEnter)
		{
			if (rowSelectionModel == null)
				throw new ArgumentNullException("rowSelectionModel");
			
			rowSelectionModel.UserConfig ["moveEditorOnEnter"] = moveEditorOnEnter;
			
			return rowSelectionModel;
		}
		
		public static bool GetMoveEditorOnEnter(this IRowSelectionModel rowSelectionModel)
		{
			if (rowSelectionModel == null)
				throw new ArgumentNullException("rowSelectionModel");
			
			if (rowSelectionModel.UserConfig.ContainsKey("moveEditorOnEnter"))
				return (bool)rowSelectionModel.UserConfig ["moveEditorOnEnter"];
			else
				return true;
		}
		
		#endregion MoveEditorOnEnter
	
		#region SingleSelect
		
		public static IRowSelectionModel SetSingleSelect(this IRowSelectionModel rowSelectionModel, bool singleSelect)
		{
			if (rowSelectionModel == null)
				throw new ArgumentNullException("rowSelectionModel");
			
			rowSelectionModel.UserConfig ["singleSelect"] = singleSelect;
			
			return rowSelectionModel;
		}
		
		public static bool GetSingleSelect(this IRowSelectionModel rowSelectionModel)
		{
			if (rowSelectionModel == null)
				throw new ArgumentNullException("rowSelectionModel");
			
			if (rowSelectionModel.UserConfig.ContainsKey("singleSelect"))
				return (bool)rowSelectionModel.UserConfig ["singleSelect"];
			else
				return false;
		}
		
		#endregion SingleSelect
	
	}	

}
