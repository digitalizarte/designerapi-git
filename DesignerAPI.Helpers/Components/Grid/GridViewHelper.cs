//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Grid
{
	using System;
	using System.Linq;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Grid;
	using System.Collections.Generic;

	public static class GridViewHelper
	{
		#region AutoFill

		public static IGridView SetAutoFill(this IGridView gridView, bool autoFill)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["autoFill"] = autoFill;
			
			return gridView;
		}
		
		public static bool GetAutoFill(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("autoFill"))
				return (bool)gridView.UserConfig ["autoFill"];
			else
				return false;
		}

		#endregion AutoFill

		#region CellSelector
		
		
		public static IGridView SetCellSelector(this IGridView gridView, string cellSelector)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(cellSelector) && gridView.UserConfig.ContainsKey("cellSelector"))
				gridView.UserConfig.Remove("cellSelector");
			else
				gridView.UserConfig ["cellSelector"] = cellSelector;
			
			return gridView;
		}
		
		public static string GetCellSelector(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("cellSelector"))
				return (string)gridView.UserConfig ["cellSelector"];
			else
				return null;
		}
		
		#endregion CellSelector

		#region CellSelectorDepth
		
		public static IGridView SetCellSelectorDepth(this IGridView gridView, int cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetCellSelectorDepth(this IGridView gridView, uint cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetCellSelectorDepth(this IGridView gridView, long cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetCellSelectorDepth(this IGridView gridView, ulong cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetCellSelectorDepth(this IGridView gridView, short cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetCellSelectorDepth(this IGridView gridView, ushort cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetCellSelectorDepth(this IGridView gridView, decimal cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetCellSelectorDepth(this IGridView gridView, float cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetCellSelectorDepth(this IGridView gridView, double cellSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["cellSelectorDepth"] = cellSelectorDepth;
			
			return gridView;
		}

		public static Variant GetCellSelectorDepth(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("cellSelectorDepth"))
				return new Variant(gridView.UserConfig ["cellSelectorDepth"]);
			else
				return null;
		}
		
		#endregion CellSelectorDepth

		#region ColumnsText
		
		public static IGridView SetColumnsText(this IGridView gridView, string columnsText)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(columnsText) && gridView.UserConfig.ContainsKey("columnsText"))
				gridView.UserConfig.Remove("columnsText");
			else
				gridView.UserConfig ["columnsText"] = columnsText;
			
			return gridView;
		}
		
		public static string GetColumnsText(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("columnsText"))
				return (string)gridView.UserConfig ["columnsText"];
			else
				return null;
		}
		
		#endregion ColumnsText

		#region DeferEmptyText
		
		public static IGridView SetDeferEmptyText(this IGridView gridView, bool deferEmptyText)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["deferEmptyText"] = deferEmptyText;
			
			return gridView;
		}
		
		public static bool GetDeferEmptyText(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("deferEmptyText"))
				return (bool)gridView.UserConfig ["deferEmptyText"];
			else
				return false;
		}
		
		#endregion DeferEmptyText

		#region EmptyText
		
		public static IGridView SetEmptyText(this IGridView gridView, string emptyText)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(emptyText) && gridView.UserConfig.ContainsKey("emptyText"))
				gridView.UserConfig.Remove("emptyText");
			else
				gridView.UserConfig ["emptyText"] = emptyText;
			
			return gridView;
		}
		
		public static string GetEmptyText(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("emptyText"))
				return (string)gridView.UserConfig ["emptyText"];
			else
				return null;
		}
		
		#endregion EmptyText

		#region EnableRowBody
		
		public static IGridView SetEnableRowBody(this IGridView gridView, bool enableRowBody)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["enableRowBody"] = enableRowBody;
			
			return gridView;
		}
		
		public static bool GetEnableRowBody(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("enableRowBody"))
				return (bool)gridView.UserConfig ["enableRowBody"];
			else
				return false;
		}
		
		#endregion EnableRowBody

		#region ForceFit
		
		public static IGridView SetForceFit(this IGridView gridView, bool forceFit)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["forceFit"] = forceFit;
			
			return gridView;
		}
		
		public static bool GetForceFit(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("forceFit"))
				return (bool)gridView.UserConfig ["forceFit"];
			else
				return false;
		}
		
		#endregion ForceFit

		#region HeadersDisabled
		
		public static IGridView SetHeadersDisabled(this IGridView gridView, bool headersDisabled)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["headersDisabled"] = headersDisabled;
			
			return gridView;
		}
		
		public static bool GetHeadersDisabled(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("headersDisabled"))
				return (bool)gridView.UserConfig ["headersDisabled"];
			else
				return false;
		}
		
		#endregion HeadersDisabled

		#region RowBodySelector
		
		public static IGridView SetRowBodySelector(this IGridView gridView, string rowBodySelector)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(rowBodySelector) && gridView.UserConfig.ContainsKey("rowBodySelector"))
				gridView.UserConfig.Remove("rowBodySelector");
			else
				gridView.UserConfig ["rowBodySelector"] = rowBodySelector;
			
			return gridView;
		}
		
		public static string GetRowBodySelector(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("rowBodySelector"))
				return (string)gridView.UserConfig ["rowBodySelector"];
			else
				return null;
		}
		
		#endregion RowBodySelector

		#region RowBodySelectorDepth
		
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, int rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, uint rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, long rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, ulong rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, short rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, ushort rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, decimal rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, float rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowBodySelectorDepth(this IGridView gridView, double rowBodySelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowBodySelectorDepth"] = rowBodySelectorDepth;
			
			return gridView;
		}

		public static Variant GetRowBodySelectorDepth(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("rowBodySelectorDepth"))
				return new Variant(gridView.UserConfig ["rowBodySelectorDepth"]);
			else
				return null;
		}

		
		#endregion RowBodySelectorDepth

		#region RowSelector
		
		public static IGridView SetRowSelector(this IGridView gridView, string rowSelector)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(rowSelector) && gridView.UserConfig.ContainsKey("rowSelector"))
				gridView.UserConfig.Remove("rowSelector");
			else
				gridView.UserConfig ["rowSelector"] = rowSelector;
			
			return gridView;
		}
		
		public static string GetRowSelector(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("rowSelector"))
				return (string)gridView.UserConfig ["rowSelector"];
			else
				return null;
		}
		
		#endregion RowSelector

		#region RowSelectorDepth
		
		public static IGridView SetRowSelectorDepth(this IGridView gridView, int rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetRowSelectorDepth(this IGridView gridView, uint rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowSelectorDepth(this IGridView gridView, long rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetRowSelectorDepth(this IGridView gridView, ulong rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowSelectorDepth(this IGridView gridView, short rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetRowSelectorDepth(this IGridView gridView, ushort rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowSelectorDepth(this IGridView gridView, decimal rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowSelectorDepth(this IGridView gridView, float rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}
		
		public static IGridView SetRowSelectorDepth(this IGridView gridView, double rowSelectorDepth)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["rowSelectorDepth"] = rowSelectorDepth;
			
			return gridView;
		}

		public static Variant GetRowSelectorDepth(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("rowSelectorDepth"))
				return new Variant(gridView.UserConfig ["rowSelectorDepth"]);
			else
				return null;
		}
		
		#endregion RowSelectorDepth

		#region ScrollOffset
		
		public static IGridView SetScrollOffset(this IGridView gridView, int scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetScrollOffset(this IGridView gridView, uint scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
		public static IGridView SetScrollOffset(this IGridView gridView, long scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetScrollOffset(this IGridView gridView, ulong scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
		public static IGridView SetScrollOffset(this IGridView gridView, short scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
        [CLSCompliantAttribute(false)]
		public static IGridView SetScrollOffset(this IGridView gridView, ushort scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
		public static IGridView SetScrollOffset(this IGridView gridView, decimal scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
		public static IGridView SetScrollOffset(this IGridView gridView, float scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}
		
		public static IGridView SetScrollOffset(this IGridView gridView, double scrollOffset)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			gridView.UserConfig ["scrollOffset"] = scrollOffset;
			
			return gridView;
		}

		public static Variant GetScrollOffset(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("scrollOffset"))
				return new Variant(gridView.UserConfig ["scrollOffset"]);
			else
				return null;
		}
		
		#endregion ScrollOffset

		#region SelectedRowClass
		
		public static IGridView SetSelectedRowClass(this IGridView gridView, string selectedRowClass)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(selectedRowClass) && gridView.UserConfig.ContainsKey("selectedRowClass"))
				gridView.UserConfig.Remove("selectedRowClass");
			else
				gridView.UserConfig ["selectedRowClass"] = selectedRowClass;
			
			return gridView;
		}
		
		public static string GetSelectedRowClass(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("selectedRowClass"))
				return (string)gridView.UserConfig ["selectedRowClass"];
			else
				return null;
		}
		
		#endregion SelectedRowClass

		#region SortAscText
		
		public static IGridView SetSortAscText(this IGridView gridView, string sortAscText)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(sortAscText) && gridView.UserConfig.ContainsKey("sortAscText"))
				gridView.UserConfig.Remove("sortAscText");
			else
				gridView.UserConfig ["sortAscText"] = sortAscText;
			
			return gridView;
		}
		
		public static string GetSortAscText(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("sortAscText"))
				return (string)gridView.UserConfig ["sortAscText"];
			else
				return null;
		}
		
		#endregion SortAscText

		#region SortClasses
		
		public static IGridView SetSortClasses(this IGridView gridView, string[] sortClasses)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (sortClasses == null)
				throw new ArgumentNullException("sortClasses ");
			
			gridView.UserConfig ["sortClasses "] = new List<string>(sortClasses);
			
			return gridView;
		}

		public static IGridView SetSortClasses(this IGridView gridView, IList<string> sortClasses)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (sortClasses == null)
				throw new ArgumentNullException("sortClasses ");
			
			gridView.UserConfig ["sortClasses "] = sortClasses;
			
			return gridView;
		}
		
		public static IList<string> GetSortClasses(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("sortClasses "))
				return (IList<string>)gridView.UserConfig ["sortClasses "];
			else
				return null;
		}
		
		#endregion SortClasses

		#region SortDescText
		
		public static IGridView SetSortDescText(this IGridView gridView, string sortDescText)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (String.IsNullOrEmpty(sortDescText) && gridView.UserConfig.ContainsKey("sortDescText "))
				gridView.UserConfig.Remove("sortDescText ");
			else
				gridView.UserConfig ["sortDescText "] = sortDescText;
			
			return gridView;
		}
		
		public static string GetSortDescText(this IGridView gridView)
		{
			if (gridView == null)
				throw new ArgumentNullException("gridView");
			
			if (gridView.UserConfig.ContainsKey("sortDescText "))
				return (string)gridView.UserConfig ["sortDescText "];
			else
				return null;
		}
		
		#endregion SortDescText
	}	
}
