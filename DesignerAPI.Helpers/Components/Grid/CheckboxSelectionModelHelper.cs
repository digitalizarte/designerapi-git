//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Grid
{
	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Grid;
	using System.Collections.Generic;

	public static class CheckboxSelectionModelHelper
	{
		#region CheckOnly
		
		public static ICheckboxSelectionModel SetCheckOnly(this ICheckboxSelectionModel checkboxSelectionModel, bool checkOnly)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["checkOnly"] = checkOnly;
			
			return checkboxSelectionModel;
		}
		
		public static bool GetCheckOnly(this ICheckboxSelectionModel checkboxSelectionModel)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			if (checkboxSelectionModel.UserConfig.ContainsKey("checkOnly"))
				return (bool)checkboxSelectionModel.UserConfig ["checkOnly"];
			else
				return false;
		}
		
		#endregion CheckOnly

		#region Header
		
		public static ICheckboxSelectionModel SetHeader(this ICheckboxSelectionModel checkboxSelectionModel, string header)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			if (String.IsNullOrEmpty(header) && checkboxSelectionModel.UserConfig.ContainsKey("header"))
				checkboxSelectionModel.UserConfig.Remove("header");
			else
				checkboxSelectionModel.UserConfig ["header"] = header;
			
			return checkboxSelectionModel;
		}
		
		public static string GetHeader(this ICheckboxSelectionModel checkboxSelectionModel)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			if (checkboxSelectionModel.UserConfig.ContainsKey("header"))
				return (string)checkboxSelectionModel.UserConfig ["header"];
			else
				return null;
		}
		
		#endregion Header

		#region Sortable
		
		public static ICheckboxSelectionModel SetSortable(this ICheckboxSelectionModel checkboxSelectionModel, bool sortable)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["sortable"] = sortable;
			
			return checkboxSelectionModel;
		}
		
		public static bool GetSortable(this ICheckboxSelectionModel checkboxSelectionModel)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			if (checkboxSelectionModel.UserConfig.ContainsKey("sortable"))
				return (bool)checkboxSelectionModel.UserConfig ["sortable"];
			else
				return false;
		}
		
		#endregion Sortable

		#region Width
		
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, int width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
  
        [CLSCompliantAttribute(false)]
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, uint width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, long width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
        [CLSCompliantAttribute(false)]
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, ulong width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, short width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
        [CLSCompliantAttribute(false)]
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, ushort width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, decimal width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, float width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}
		
		public static ICheckboxSelectionModel SetWidth(this ICheckboxSelectionModel checkboxSelectionModel, double width)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			checkboxSelectionModel.UserConfig ["width"] = width;
			
			return checkboxSelectionModel;
		}

		public static Variant GetWidth(this ICheckboxSelectionModel checkboxSelectionModel)
		{
			if (checkboxSelectionModel == null)
				throw new ArgumentNullException("checkboxSelectionModel");
			
			if (checkboxSelectionModel.UserConfig.ContainsKey("width"))
				return new Variant(checkboxSelectionModel.UserConfig ["width"]);
			else
				return null;
		}
		
		#endregion Width
	}
}
