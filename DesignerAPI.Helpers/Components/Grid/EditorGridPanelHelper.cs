//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Grid
{
	using System;
	using System.Linq;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using DesignerAPI.Components.Grid;
	using System.Collections.Generic;

	public static class EditorGridPanelHelper
	{
		#region AutoEncode
		
		public static IEditorGridPanel SetAutoEncode(this IEditorGridPanel editorGridPanel, bool autoEncode)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["autoEncode "] = autoEncode;
			
			return editorGridPanel;
		}
		
		public static bool GetAutoEncode(this IEditorGridPanel editorGridPanel)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			if (editorGridPanel.UserConfig.ContainsKey("autoEncode "))
				return (bool)editorGridPanel.UserConfig ["autoEncode "];
			else
				return false;
		}
		
		#endregion AutoEncode
		
		#region ClicksToEdit
		
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, int clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
        [CLSCompliantAttribute(false)]
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, uint clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, long clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
        [CLSCompliantAttribute(false)]
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, ulong clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, short clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
        [CLSCompliantAttribute(false)]
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, ushort clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, decimal clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, float clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
		public static IEditorGridPanel SetClicksToEdit(this IEditorGridPanel editorGridPanel, double clicksToEdit)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["clicksToEdit "] = clicksToEdit;
			
			return editorGridPanel;
		}
		
		public static Variant GetClicksToEdit(this IEditorGridPanel editorGridPanel)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			if (editorGridPanel.UserConfig.ContainsKey("clicksToEdit "))
				return new Variant(editorGridPanel.UserConfig ["clicksToEdit "]);
			else
				return null;
		}
		
		#endregion ClicksToEdit

		#region ForceValidation
		
		public static IEditorGridPanel SetForceValidation(this IEditorGridPanel editorGridPanel, bool forceValidation)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["forceValidation "] = forceValidation;
			
			return editorGridPanel;
		}
		
		public static bool GetForceValidation(this IEditorGridPanel editorGridPanel)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			if (editorGridPanel.UserConfig.ContainsKey("forceValidation "))
				return (bool)editorGridPanel.UserConfig ["forceValidation "];
			else
				return false;
		}
		
		#endregion ForceValidation

		#region TrackMouseOver
		
		public static IEditorGridPanel SetTrackMouseOver(this IEditorGridPanel editorGridPanel, bool trackMouseOver)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			editorGridPanel.UserConfig ["trackMouseOver "] = trackMouseOver;
			
			return editorGridPanel;
		}
		
		public static bool GetTrackMouseOver(this IEditorGridPanel editorGridPanel)
		{
			if (editorGridPanel == null)
				throw new ArgumentNullException("editorGridPanel");
			
			if (editorGridPanel.UserConfig.ContainsKey("trackMouseOver "))
				return (bool)editorGridPanel.UserConfig ["trackMouseOver "];
			else
				return false;
		}
		
		#endregion TrackMouseOver
	}	
}
