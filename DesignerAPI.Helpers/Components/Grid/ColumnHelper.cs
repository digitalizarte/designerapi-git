//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Grid
{
    using System;
    using DesignerAPI.Model;
    using DesignerAPI.Components;
    using DesignerAPI.Components.Grid;
    using System.Collections.Generic;

    public static class ColumnHelper
    {

        #region Align

        public static IColumn SetAlign (this IColumn column, string align)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (align) && column.UserConfig.ContainsKey ("align"))
                column.UserConfig.Remove ("align");
            else
                column.UserConfig ["align"] = align;
            
            return column;
        }

        public static TEntity SetAlign<TEntity> (this IColumn column, string align) 
            where TEntity : IColumn
        {
            if (column == null)
                throw new ArgumentNullException ("column");

            return (TEntity)column.SetAlign(align);
        }

        public static string GetAlign (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("align"))
                return (string)column.UserConfig ["align"];
            else
                return null;
        }       
        
        #endregion Align

        #region Css

        public static IColumn SetCss (this IColumn column, string css)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (css) && column.UserConfig.ContainsKey ("css"))
                column.UserConfig.Remove ("css");
            else
                column.UserConfig ["css"] = css;
            
            return column;
        }
        
        public static string GetCss (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("css"))
                return (string)column.UserConfig ["css"];
            else
                return null;
        }       
        
        #endregion Css

        #region DataIndex

        public static IColumn SetDataIndex (this IColumn column, string dataIndex)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (dataIndex) && column.UserConfig.ContainsKey ("dataIndex"))
                column.UserConfig.Remove ("dataIndex");
            else
                column.UserConfig ["dataIndex"] = dataIndex;
            
            return column;
        }

        public static TEntity SetDataIndex<TEntity> (this IColumn column, string dataIndex)
            where TEntity : IColumn
        {
            if (column == null)
                throw new ArgumentNullException ("column");

            return (TEntity)column.SetDataIndex(dataIndex);
        }
        
        public static string GetDataIndex (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("dataIndex"))
                return (string)column.UserConfig ["dataIndex"];
            else
                return null;
        }       
        
        #endregion DataIndex

        #region Editable
        
        public static IColumn SetEditable (this IColumn column, bool editable)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["editable"] = editable;
            
            return column;
        }
        
        public static bool GetEditable (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("editable"))
                return (bool)column.UserConfig ["editable"];
            else
                return false;
        }
        
        #endregion Editable

        #region EmptyGroupText

        public static IColumn SetEmptyGroupText (this IColumn column, string emptyGroupText)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (emptyGroupText) && column.UserConfig.ContainsKey ("emptyGroupText"))
                column.UserConfig.Remove ("emptyGroupText");
            else
                column.UserConfig ["emptyGroupText"] = emptyGroupText;
            
            return column;
        }
        
        public static string GetEmptyGroupText (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("emptyGroupText"))
                return (string)column.UserConfig ["emptyGroupText"];
            else
                return null;
        }
        
        #endregion EmptyGroupText

        #region Fixed
        
        public static IColumn SetFixed (this IColumn column, bool @fixed)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["fixed"] = @fixed;
            
            return column;
        }
        
        public static bool GetFixed (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("fixed"))
                return (bool)column.UserConfig ["fixed"];
            else
                return false;
        }
        
        #endregion Fixed

        #region Groupable

        public static IColumn SetGroupable (this IColumn column, bool groupable)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["groupable"] = groupable;
            
            return column;
        }
        
        public static bool GetGroupable (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("groupable"))
                return (bool)column.UserConfig ["groupable"];
            else
                return false;
        }       
        
        #endregion Groupable

        #region GroupName
        
        public static IColumn SetGroupName (this IColumn column, bool groupName)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["groupName"] = groupName;
            
            return column;
        }
        
        public static bool GetGroupName (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("groupName"))
                return (bool)column.UserConfig ["groupName"];
            else
                return false;
        }
        
        #endregion GroupName

        #region Header
        
        public static IColumn SetHeader (this IColumn column, string header)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (header) && column.UserConfig.ContainsKey ("header"))
                column.UserConfig.Remove ("header");
            else
                column.UserConfig ["header"] = header;
            
            return column;
        }

        public static TEntity SetHeader<TEntity> (this IColumn column, string header) 
            where TEntity : IColumn
        {
            if (column == null)
                throw new ArgumentNullException ("column");

            return (TEntity)column.SetHeader (header);
        }
        
        public static string GetHeader (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("header"))
                return (string)column.UserConfig ["header"];
            else
                return null;
        }
        
        #endregion Header

        #region Hideable
        
        public static IColumn SetHideable (this IColumn column, bool hideable)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["hideable"] = hideable;
            
            return column;
        }
        
        public static bool GetHideable (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("hideable"))
                return (bool)column.UserConfig ["hideable"];
            else
                return false;
        }
        
        #endregion Hideable

        #region Id
        
        public static IColumn SetId (this IColumn column, string id)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (id) && column.UserConfig.ContainsKey ("id"))
                column.UserConfig.Remove ("id");
            else
                column.UserConfig ["id"] = id;
            
            return column;
        }
        
        public static string GetId (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("id"))
                return (string)column.UserConfig ["id"];
            else
                return null;
        }
        
        #endregion Id

        #region MenuDisabled
        
        public static IColumn SetMenuDisabled (this IColumn column, bool menuDisabled)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["menuDisabled"] = menuDisabled;
            
            return column;
        }
        
        public static bool GetMenuDisabled (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("menuDisabled"))
                return (bool)column.UserConfig ["menuDisabled"];
            else
                return false;
        }
        
        #endregion MenuDisabled

        #region Resizable
        
        public static IColumn SetResizable (this IColumn column, bool resizable)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["resizable"] = resizable;
            
            return column;
        }
        
        public static bool GetResizable (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("resizable"))
                return (bool)column.UserConfig ["resizable"];
            else
                return false;
        }
        
        #endregion Resizable

        #region Sortable
        
        public static IColumn SetSortable (this IColumn column, bool sortable)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["sortable"] = sortable;
            
            return column;
        }
        
        public static bool GetSortable (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("sortable"))
                return (bool)column.UserConfig ["sortable"];
            else
                return false;
        }
        
        #endregion Sortable

        #region Tooltip
        
        public static IColumn SetTooltip (this IColumn column, string tooltip)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (String.IsNullOrEmpty (tooltip) && column.UserConfig.ContainsKey ("tooltip"))
                column.UserConfig.Remove ("tooltip");
            else
                column.UserConfig ["tooltip"] = tooltip;
            
            return column;
        }
        
        public static string GetTooltip (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("tooltip"))
                return (string)column.UserConfig ["tooltip"];
            else
                return null;
        }
        
        #endregion Tooltip

        #region Width

        public static IColumn SetWidth (this IColumn column, int width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        [CLSCompliantAttribute(false)]
        public static IColumn SetWidth (this IColumn column, uint width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        public static IColumn SetWidth (this IColumn column, long width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        [CLSCompliantAttribute(false)]
        public static IColumn SetWidth (this IColumn column, ulong width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        public static IColumn SetWidth (this IColumn column, short width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        [CLSCompliantAttribute(false)]
        public static IColumn SetWidth (this IColumn column, ushort width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        public static IColumn SetWidth (this IColumn column, decimal width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        public static IColumn SetWidth (this IColumn column, float width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }
        
        public static IColumn SetWidth (this IColumn column, double width)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            column.UserConfig ["width"] = width;
            
            return column;
        }

        public static Variant GetWidth (this IColumn column)
        {
            if (column == null)
                throw new ArgumentNullException ("column");
            
            if (column.UserConfig.ContainsKey ("width"))
                return new Variant (column.UserConfig ["width"]);
            else
                return null;
        }
        
        #endregion Width
    }   
}
