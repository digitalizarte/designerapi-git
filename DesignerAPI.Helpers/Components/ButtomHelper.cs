//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
    
	using System.Collections.Generic;

	public static class ButtomHelper
	{
        #region AllowDepress
        
		public static IButton SetAllowDepress(this IButton button, bool allowDepress)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["allowDepress"] = allowDepress;
            
			return button;
		}
        
		public static bool GetAllowDepress(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("allowDepress"))
				return (bool)button.UserConfig ["allowDepress"];
			else
				return false;
		}
        
        #endregion AllowDepress

        #region ArrowAlign

		public static IButton SetArrowAlign(this IButton button, string arrowAlign)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(arrowAlign) && button.UserConfig.ContainsKey("arrowAlign"))
				button.UserConfig.Remove("arrowAlign");
			else
				button.UserConfig ["arrowAlign"] = arrowAlign;
            
			return button;
		}
        
		public static IButton SetArrowAlign(this IButton button, ArrowAlign  arrowAlign)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			string value = Enum.GetName(typeof(ArrowAlign), arrowAlign).ToLowerInvariant();
			button.UserConfig ["arrowAlign"] = value;
            
			return button;
		}
        
		public static ArrowAlign  GetArrowAlign(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("arrowAlign"))
				return (ArrowAlign)Enum.Parse(typeof(ArrowAlign), (string)button.UserConfig ["arrowAlign"], true);
			else
				return ArrowAlign.Right;
		}
        
        #endregion ArrowAlign

        #region AutoWidth
        
		public static IButton SetAutoWidth(this IButton button, bool autoWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["autoWidth"] = autoWidth;
            
			return button;
		}
        
		public static bool GetAutoWidth(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("autoWidth"))
				return (bool)button.UserConfig ["autoWidth"];
			else
				return false;
		}
        
        #endregion AutoWidth

        #region ButtonSelector
        
		public static IButton SetButtonSelector(this IButton button, string buttonSelector)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(buttonSelector) && button.UserConfig.ContainsKey("buttonSelector"))
				button.UserConfig.Remove("buttonSelector");
			else
				button.UserConfig ["buttonSelector"] = buttonSelector;
            
			return button;
		}
        
		public static string GetButtonSelector(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("buttonSelector"))
				return (string)button.UserConfig ["buttonSelector"];
			else
				return null;
		}

        #endregion ButtonSelector
        
        #region ClickEvent
        
		public static IButton SetClickEvent(this IButton button, string clickEvent)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(clickEvent) && button.UserConfig.ContainsKey("clickEvent"))
				button.UserConfig.Remove("clickEvent");
			else
				button.UserConfig ["clickEvent"] = clickEvent;
            
			return button;
		}
        
		public static string GetClickEvent(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("clickEvent"))
				return (string)button.UserConfig ["clickEvent"];
			else
				return null;
		}
        
        #endregion ClickEvent

        #region Cls
        
		public static IButton SetCls(this IButton button, string cls)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(cls) && button.UserConfig.ContainsKey("cls"))
				button.UserConfig.Remove("cls");
			else
				button.UserConfig ["cls"] = cls;
            
			return button;
		}
        
		public static string GetCls(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("cls"))
				return (string)button.UserConfig ["cls"];
			else
				return null;
		}
        
        #endregion Cls

        #region Disabled
        
		public static IButton SetDisabled(this IButton button, bool disabled)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["disabled"] = disabled;
            
			return button;
		}
        
		public static bool GetDisabled(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("disabled"))
				return (bool)button.UserConfig ["disabled"];
			else
				return false;
		}
        
        #endregion Disabled

        #region EnableToggle
        
		public static IButton SetEnableToggle(this IButton button, bool enableToggle)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["enableToggle"] = enableToggle;
            
			return button;
		}
        
		public static bool GetEnableToggle(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("enableToggle"))
				return (bool)button.UserConfig ["enableToggle"];
			else
				return false;
		}
        
        #endregion EnableToggle

        #region HandleMouseEvents
        
		public static IButton SetHandleMouseEvents(this IButton button, bool handleMouseEvents)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["handleMouseEvents"] = handleMouseEvents;
            
			return button;
		}
        
		public static bool GetHandleMouseEvents(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("handleMouseEvents"))
				return (bool)button.UserConfig ["handleMouseEvents"];
			else
				return false;
		}
        
        #endregion HandleMouseEventss

        #region Hidden
        
		public static IButton SetHidden(this IButton button, bool hidden)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["hidden"] = hidden;
            
			return button;
		}
        
		public static bool GetHidden(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("hidden"))
				return (bool)button.UserConfig ["hidden"];
			else
				return false;
		}
        
        #endregion Hidden

        #region Icon
        
		public static IButton SetIcon(this IButton button, string icon)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(icon) && button.UserConfig.ContainsKey("icon"))
				button.UserConfig.Remove("icon");
			else
				button.UserConfig ["icon"] = icon;
            
			return button;
		}
        
		public static string GetIcon(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("icon"))
				return (string)button.UserConfig ["icon"];
			else
				return null;
		}
        
        #endregion Icon

        #region IconAlign

		public static IButton SetIconAlign(this IButton button, string iconAlign)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(iconAlign) && button.UserConfig.ContainsKey("iconAlign"))
				button.UserConfig.Remove("iconAlign");
			else
				button.UserConfig ["iconAlign"] = iconAlign;
            
			return button;
		}
        
		public static IButton SetIconAlign(this IButton button, IconAlign  iconAlign)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			string value = Enum.GetName(typeof(IconAlign), iconAlign).ToLowerInvariant();
			button.UserConfig ["iconAlign"] = value;
            
			return button;
		}
        
		public static IconAlign  GetIconAlign(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("iconAlign"))
				return (IconAlign)Enum.Parse(typeof(IconAlign), (string)button.UserConfig ["iconAlign"], true);
			else
				return IconAlign.Left;
		}
        
        #endregion IconAlign

        #region IconCls
        
		public static IButton SetIconCls(this IButton button, string iconCls)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(iconCls) && button.UserConfig.ContainsKey("iconCls"))
				button.UserConfig.Remove("iconCls");
			else
				button.UserConfig ["iconCls"] = iconCls;
            
			return button;
		}
        
		public static string GetIconCls(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("iconCls"))
				return (string)button.UserConfig ["iconCls"];
			else
				return null;
		}
        
        #endregion IconCls

        #region MenuAlign
        
		public static IButton SetMenuAlign(this IButton button, string menuAlign)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(menuAlign) && button.UserConfig.ContainsKey("menuAlign"))
				button.UserConfig.Remove("menuAlign");
			else
				button.UserConfig ["menuAlign"] = menuAlign;
            
			return button;
		}
        
		public static IButton SetMenuAlign(this IButton button, AlignTo  menuAlign)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			string value = Enum.GetName(typeof(AlignTo), menuAlign).ToLowerInvariant();
			button.UserConfig ["menuAlign"] = value;
            
			return button;
		}
        
		public static string GetMenuAlign(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("menuAlign"))
				return (string)button.UserConfig ["menuAlign"];
			else
				return null;
		}
        
        #endregion MenuAlign        

        #region MinWidth
        
		public static IButton SetMinWidth(this IButton button, int minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}

        [CLSCompliantAttribute(false)]
		public static IButton SetMinWidth(this IButton button, uint minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
		public static IButton SetMinWidth(this IButton button, long minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
        [CLSCompliantAttribute(false)]
        public static IButton SetMinWidth(this IButton button, ulong minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
		public static IButton SetMinWidth(this IButton button, short minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}

        [CLSCompliantAttribute(false)]
		public static IButton SetMinWidth(this IButton button, ushort minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
		public static IButton SetMinWidth(this IButton button, decimal minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
		public static IButton SetMinWidth(this IButton button, float minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
		public static IButton SetMinWidth(this IButton button, double minWidth)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["minWidth "] = minWidth;
            
			return button;
		}
        
		public static Variant GetMinWidth(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("minWidth "))
				return new Variant(button.UserConfig ["minWidth "]);
			else
				return null;
		}
        
        #endregion MinWidth

        #region OverflowText
        
		public static IButton SetOverflowText(this IButton button, string overflowText)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(overflowText) && button.UserConfig.ContainsKey("overflowText"))
				button.UserConfig.Remove("overflowText");
			else
				button.UserConfig ["overflowText"] = overflowText;
            
			return button;
		}
        
		public static string GetOverflowText(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("overflowText"))
				return (string)button.UserConfig ["overflowText"];
			else
				return null;
		}

        #endregion OverflowText       
                
        #region Pressed
        
		public static IButton SetPressed(this IButton button, bool pressed)
		{
			if (button == null)
				throw new ArgumentNullException("button");
			button.UserConfig ["pressed"] = pressed;
            
			return button;
		}
        
		public static bool GetPressed(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("pressed"))
				return (bool)button.UserConfig ["pressed"];
			else
				return false;
		}
        
        #endregion Pressed

        #region Repeat
        
		public static IButton SetRepeat(this IButton button, bool repeat)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["repeat"] = repeat;
            
			return button;
		}
        
		public static bool GetRepeat(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("repeat"))
				return (bool)button.UserConfig ["repeat"];
			else
				return false;
		}
        
        #endregion Repeat

        #region Scale
        
		public static IButton SetScale(this IButton button, string scale)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(scale) && button.UserConfig.ContainsKey("scale"))
				button.UserConfig.Remove("scale");
			else
				button.UserConfig ["scale"] = scale;
            
			return button;
		}
        
		public static IButton SetScale(this IButton button, Scale  scale)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			string value = Enum.GetName(typeof(Scale), scale).ToLowerInvariant();
			button.UserConfig ["scale"] = value;
            
			return button;
		}
        
		public static Scale  GetScale(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("scale"))
				return (Scale)Enum.Parse(typeof(Scale), (string)button.UserConfig ["scale"], true);
			else
				return Scale.Small;
		}
        
        #endregion Scale

        #region TabIndex
        
		public static IButton SetTabIndex(this IButton button, int tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}

        [CLSCompliantAttribute(false)]
		public static IButton SetTabIndex(this IButton button, uint tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}
        
		public static IButton SetTabIndex(this IButton button, long tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}

        [CLSCompliantAttribute(false)]
		public static IButton SetTabIndex(this IButton button, ulong tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}
        
		public static IButton SetTabIndex(this IButton button, short tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}

        [CLSCompliantAttribute(false)]
		public static IButton SetTabIndex(this IButton button, ushort tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}
        
		public static IButton SetTabIndex(this IButton button, decimal tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}
        
		public static IButton SetTabIndex(this IButton button, float tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}
        
		public static IButton SetTabIndex(this IButton button, double tabIndex)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			button.UserConfig ["tabIndex "] = tabIndex;
            
			return button;
		}
        
		public static Variant GetTabIndex(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("tabIndex "))
				return new Variant(button.UserConfig ["tabIndex "]);
			else
				return null;
		}
        
        #endregion TabIndex

        #region Text
                
		public static IButton SetText(this IButton button, string text)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(text) && button.UserConfig.ContainsKey("text"))
				button.UserConfig.Remove("text");
			else
				button.UserConfig ["text"] = text;
            
			return button;
		}
        
		public static string GetText(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("text"))
				return (string)button.UserConfig ["text"];
			else
				return null;
		}
        
        #endregion Text

        #region ToggleGroup
        
		public static IButton SetToggleGroup(this IButton button, string toggleGroup)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(toggleGroup) && button.UserConfig.ContainsKey("toggleGroup"))
				button.UserConfig.Remove("toggleGroup");
			else
				button.UserConfig ["toggleGroup"] = toggleGroup;
            
			return button;
		}
        
		public static string GetToggleGroup(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("toggleGroup"))
				return (string)button.UserConfig ["toggleGroup"];
			else
				return null;
		}
        
        #endregion ToggleGroup      

        #region Tooltip
        
		public static IButton SetTooltip(this IButton button, string tooltip)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(tooltip) && button.UserConfig.ContainsKey("tooltip"))
				button.UserConfig.Remove("tooltip");
			else
				button.UserConfig ["tooltip"] = tooltip;
            
			return button;
		}
        
		public static string GetTooltip(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("tooltip"))
				return (string)button.UserConfig ["tooltip"];
			else
				return null;
		}

		public static IButton SetTooltip(this IButton button, object tooltip)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (tooltip == null && button.UserConfig.ContainsKey("tooltip"))
				button.UserConfig.Remove("tooltip");
			else
				button.UserConfig ["tooltip"] = tooltip;
            
			return button;
		}
        
        #endregion Tooltip

        #region TooltipType
        
		public static IButton SetTooltipType(this IButton button, string tooltipType)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(tooltipType) && button.UserConfig.ContainsKey("tooltipType"))
				button.UserConfig.Remove("tooltipType");
			else
				button.UserConfig ["tooltipType"] = tooltipType;
            
			return button;
		}
        
		public static string GetTooltipType(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("tooltipType"))
				return (string)button.UserConfig ["tooltipType"];
			else
				return null;
		}
        
        #endregion TooltipType       

        #region Type
        
		public static IButton SetType(this IButton button, string type)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (String.IsNullOrEmpty(type) && button.UserConfig.ContainsKey("type"))
				button.UserConfig.Remove("type");
			else
				button.UserConfig ["type"] = type;
            
			return button;
		}
        
		public static string GetType(this IButton button)
		{
			if (button == null)
				throw new ArgumentNullException("button");
            
			if (button.UserConfig.ContainsKey("type"))
				return (string)button.UserConfig ["type"];
			else
				return null;
		}
                
        #endregion Type        
              
	}
}
