//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
	using System;
	using System.Collections.Generic;
	using DesignerAPI.Builder;
    using DesignerAPI.Components;
    using DesignerAPI.Components.Form;
    using DesignerAPI.Model;

	public static class TabPanelHelper
	{
        public static IPanel AddPanel(this ITabPanel tabPanel)
        {
            if (tabPanel == null)
                throw new ArgumentNullException("tabPanel");
            
            IPanel panel = new Panel();
            tabPanel.Components.Add(tabPanel);
            ProjectBuilderHelper.LatestComponent.Push(tabPanel);
            
            return panel;
        }

        #region ActiveTab
        
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, string activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (String.IsNullOrEmpty(activeTab) && tabPanel.UserConfig.ContainsKey("activeTab"))
				tabPanel.UserConfig.Remove("activeTab");
			else
				tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}

		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, int activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, uint activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}
        
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, long activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, ulong activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}
        
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, short activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, ushort activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}
        
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, decimal activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}
        
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, float activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}
        
		public static ITabPanel SetActiveTab(this ITabPanel tabPanel, double activeTab)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["activeTab"] = activeTab;
            
			return tabPanel;
		}
                
		public static Variant GetActiveTab(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("activeTab"))
				return new Variant(tabPanel.UserConfig ["activeTab"]);
			else
				return null;
		}
        
        #endregion ActiveTab

        #region AnimScroll
        
		public static ITabPanel SetAnimScroll(this ITabPanel tabPanel, bool animScroll)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["animScroll"] = animScroll;
            
			return tabPanel;
		}
        
		public static bool GetAnimScroll(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("animScroll"))
				return (bool)tabPanel.UserConfig ["animScroll"];
			else
				return false;
		}
        
        #endregion AnimScroll

        #region BaseCls
        
		public static ITabPanel SetBaseCls(this ITabPanel tabPanel, string baseCls)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (String.IsNullOrEmpty(baseCls) && tabPanel.UserConfig.ContainsKey("baseCls"))
				tabPanel.UserConfig.Remove("baseCls");
			else
				tabPanel.UserConfig ["baseCls"] = baseCls;
            
			return tabPanel;
		}
        
		public static string GetBaseCls(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("baseCls"))
				return (string)tabPanel.UserConfig ["baseCls"];
			else
				return null;
		}
        
        #endregion BaseCls

        #region DeferredRender
        
		public static ITabPanel SetDeferredRender(this ITabPanel tabPanel, bool deferredRender)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["deferredRender"] = deferredRender;
            
			return tabPanel;
		}
        
		public static bool GetDeferredRender(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("deferredRender"))
				return (bool)tabPanel.UserConfig ["deferredRender"];
			else
				return false;
		}
        
        #endregion DeferredRender

        #region EnableTabScroll
        
		public static ITabPanel SetEnableTabScroll(this ITabPanel tabPanel, bool enableTabScroll)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["enableTabScroll"] = enableTabScroll;
            
			return tabPanel;
		}
        
		public static bool GetEnableTabScroll(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("enableTabScroll"))
				return (bool)tabPanel.UserConfig ["enableTabScroll"];
			else
				return false;
		}
        
        #endregion EnableTabScroll

        #region LayoutOnTabChange
        
		public static ITabPanel SetLayoutOnTabChange(this ITabPanel tabPanel, bool layoutOnTabChange)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["layoutOnTabChange"] = layoutOnTabChange;
            
			return tabPanel;
		}
        
		public static bool GetLayoutOnTabChange(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("layoutOnTabChange"))
				return (bool)tabPanel.UserConfig ["layoutOnTabChange"];
			else
				return false;
		}
        
        #endregion LayoutOnTabChange

        #region MinTabWidth
        
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, int minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, uint minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, long minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, ulong minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, short minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, ushort minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, decimal minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, float minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetMinTabWidth(this ITabPanel tabPanel, double minTabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["minTabWidth"] = minTabWidth;
            
			return tabPanel;
		}

		public static Variant GetMinTabWidth(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("minTabWidth"))
				return new Variant(tabPanel.UserConfig ["minTabWidth"]);
			else
				return null;
		}
        
        #endregion MinTabWidth

        #region Plain
        
		public static ITabPanel SetPlain(this ITabPanel tabPanel, bool plain)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["plain"] = plain;
            
			return tabPanel;
		}
        
		public static bool GetPlain(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("plain"))
				return (bool)tabPanel.UserConfig ["plain"];
			else
				return false;
		}
        
        #endregion Plain

        #region ResizeTabs
        
		public static ITabPanel SetResizeTabs(this ITabPanel tabPanel, bool resizeTabs)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["resizeTabs"] = resizeTabs;
            
			return tabPanel;
		}
        
		public static bool GetResizeTabs(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("resizeTabs"))
				return (bool)tabPanel.UserConfig ["resizeTabs"];
			else
				return false;
		}
        
        #endregion ResizeTabs

        #region ScrollDuration
        
		public static ITabPanel SetScrollDuration(this ITabPanel tabPanel, decimal scrollDuration)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollDuration"] = scrollDuration;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollDuration(this ITabPanel tabPanel, float scrollDuration)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollDuration"] = scrollDuration;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollDuration(this ITabPanel tabPanel, double scrollDuration)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollDuration"] = scrollDuration;
            
			return tabPanel;
		}

		public static Variant GetScrollDuration(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("scrollDuration"))
				return new Variant(tabPanel.UserConfig ["scrollDuration"]);
			else
				return null;
		}
        
        #endregion ScrollDuration

        #region ScrollIncrement

		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, int scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, uint scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, long scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, ulong scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, short scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, ushort scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, decimal scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, float scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollIncrement(this ITabPanel tabPanel, double scrollIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollIncrement"] = scrollIncrement;
            
			return tabPanel;
		}

		public static Variant GetScrollIncrement(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("scrollIncrement"))
				return new Variant(tabPanel.UserConfig ["scrollIncrement"]);
			else
				return null;
		}        
        
        #endregion ScrollIncrement

        #region ScrollRepeatInterval
        
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, int scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, uint scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, long scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, ulong scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, short scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, ushort scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, decimal scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, float scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}
        
		public static ITabPanel SetScrollRepeatInterval(this ITabPanel tabPanel, double scrollRepeatInterval)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["scrollRepeatInterval"] = scrollRepeatInterval;
            
			return tabPanel;
		}

		public static Variant GetScrollRepeatInterval(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("scrollRepeatInterval"))
				return new Variant(tabPanel.UserConfig ["scrollRepeatInterval"]);
			else
				return null;
		}

        #endregion ScrollRepeatInterval

        #region TabCls
        
		public static ITabPanel SetTabCls(this ITabPanel tabPanel, string tabCls)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (String.IsNullOrEmpty(tabCls) && tabPanel.UserConfig.ContainsKey("tabCls"))
				tabPanel.UserConfig.Remove("tabCls");
			else
				tabPanel.UserConfig ["tabCls"] = tabCls;
            
			return tabPanel;
		}
        
		public static string GetTabCls(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("tabCls"))
				return (string)tabPanel.UserConfig ["tabCls"];
			else
				return null;
		}
        
        #endregion TabCls

        #region TabMargin
        
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, int tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, uint tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, long tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, ulong tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, short tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, ushort tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, decimal tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, float tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabMargin(this ITabPanel tabPanel, double tabMargin)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabMargin"] = tabMargin;
            
			return tabPanel;
		}

		public static Variant GetTabMargin(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("tabMargin"))
				return new Variant(tabPanel.UserConfig ["tabMargin"]);
			else
				return null;
		}

        
        #endregion TabMargin

        #region TabPosition
        
		public static ITabPanel SetTabPosition(this ITabPanel tabPanel, TabPosition tabPosition)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			string value = Enum.GetName(typeof(TabPosition), tabPosition).ToUpperInvariant();
			tabPanel.UserConfig ["tabPosition"] = value;
            
			return tabPanel;
		}
        
		public static TabPosition GetTabPosition(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("tabPosition"))
				return (TabPosition)Enum.Parse(typeof(TabPosition), (string)tabPanel.UserConfig ["tabPosition"], true);
			else
				return TabPosition.Top;
		}
        
        #endregion TabPosition

        #region TabWidth
        
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, int tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, uint tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, long tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, ulong tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, short tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, ushort tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, decimal tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, float tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}
        
		public static ITabPanel SetTabWidth(this ITabPanel tabPanel, double tabWidth)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["tabWidth"] = tabWidth;
            
			return tabPanel;
		}

		public static Variant GetTabWidth(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("tabWidth"))
				return new Variant(tabPanel.UserConfig ["tabWidth"]);
			else
				return null;
		}
        
        #endregion TabWidth

        #region WheelIncrement

		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, int wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, uint wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, long wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, ulong wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, short wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}

        [CLSCompliantAttribute(false)]
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, ushort wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, decimal wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, float wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}
        
		public static ITabPanel SetWheelIncrement(this ITabPanel tabPanel, double wheelIncrement)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			tabPanel.UserConfig ["wheelIncrement"] = wheelIncrement;
            
			return tabPanel;
		}

		public static Variant GetWheelIncrement(this ITabPanel tabPanel)
		{
			if (tabPanel == null)
				throw new ArgumentNullException("tabPanel");
            
			if (tabPanel.UserConfig.ContainsKey("wheelIncrement"))
				return new Variant(tabPanel.UserConfig ["wheelIncrement"]);
			else
				return null;
		}
        
        #endregion WheelIncrement

	}
}
