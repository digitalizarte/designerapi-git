//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
    using System;
    using DesignerAPI.Model;
    using DesignerAPI.Components;
    using System.Collections.Generic;

    ///<summary>
    /// Container helper.
    ///</summary>
    public static class ContainerHelper
    {
        public static IContainer SetActiveItem (this IContainer container, string activeItem)
        {
            if (String.IsNullOrEmpty (activeItem) && container.UserConfig.ContainsKey ("activeItem"))
                container.UserConfig.Remove ("activeItem");
            else
                container.UserConfig ["activeItem"] = activeItem;
            return container;
        }

        [CLSCompliantAttribute(false)]
        public static IContainer SetActiveItem (this IContainer container, uint activeItem)
        {
            container.UserConfig ["activeItem"] = activeItem;
            return container;
        }
        
        public static IContainer SetAutoDestroy (this IContainer container, bool autoDestroy)
        {
            container.UserConfig ["autoDestroy"] = autoDestroy;
            return container;
        }

        [CLSCompliantAttribute(false)]
        public static IContainer SetBufferResize (this IContainer container, uint bufferResize)
        {
            container.UserConfig ["bufferResize"] = bufferResize;
            return container;
        }
        
        public static IContainer SetDefaults (this IContainer container, object defaults)
        {
            container.UserConfig ["defaults"] = defaults;
            return container;
        }
        
        public static IContainer SetDefaultType (this IContainer container, string defaultType)
        {
            if (String.IsNullOrEmpty (defaultType) && container.UserConfig.ContainsKey ("defaultType"))
                container.UserConfig.Remove ("defaultType");
            else
                container.UserConfig ["defaultType"] = defaultType;
            return container;
        }
        
        public static IContainer SetForeceLayout (this IContainer container, bool foreceLayout)
        {
            container.UserConfig ["foreceLayout"] = foreceLayout;
            return container;
        }
        
        public static IContainer SetHideBorders (this IContainer container, bool hideBorders)
        {
            container.UserConfig ["hideBorders"] = hideBorders;
            return container;
        }

        #region Layout

        public static IContainer SetLayout (this IContainer container, string layout)
        {
            if (container == null)
                throw new ArgumentNullException ("container");

            if (String.IsNullOrEmpty (layout) && container.UserConfig.ContainsKey ("layaout"))
                container.UserConfig.Remove ("layaout");
            else
                container.UserConfig ["layaout"] = layout.ToLowerInvariant();
            return container;
        }

        public static TEntity SetLayout<TEntity> (this IContainer container, string layout)
        {
            if (container == null)
                throw new ArgumentNullException ("container");
            
            return (TEntity)container.SetLayout (layout);
        }
        
        public static IContainer SetLayout (this IContainer container, Layout  layout)
        {
            if (container == null)
                throw new ArgumentNullException ("container");
            
            string value = Enum.GetName (typeof(Layout), layout).ToLowerInvariant ();
            container.UserConfig ["layout"] = value;
            
            return container;
        }
        
        public static TEntity SetLayout<TEntity> (this IContainer container, Layout layout)
        {
            if (container == null)
                throw new ArgumentNullException ("container");
            
            return (TEntity)container.SetLayout (layout);
        }
        
        public static Layout  GetLayout (this IContainer container)
        {
            if (container == null)
                throw new ArgumentNullException ("container");
            
            if (container.UserConfig.ContainsKey ("layout"))
                return (Layout)Enum.Parse (typeof(Layout), (string)container.UserConfig ["layout"], true);
            else
                return Layout.Auto;
        }
        
        #endregion Layout
    }
}