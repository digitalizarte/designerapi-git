//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Data
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components.Data;
	using System.Collections.Generic;

	public static class StoreHelper
	{   
        #region Getters/Setters

        #region Api
        
		public static IStore SetApi(this IStore store, object api)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (api == null && store.UserConfig.ContainsKey("api"))
				store.UserConfig.Remove("api");
			else
				store.UserConfig ["api"] = api;
            
			return store;
		}
        
		public static object GetApi(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("api"))
				return store.UserConfig ["api"];
			else
				return null;
		}
        
        #endregion Api

        #region AutoDestroy
                
		public static IStore SetAutoDestroy(this IStore store, bool autoDestroy)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["autoDestroy"] = autoDestroy;
            
			return store;
		}
        
		public static bool GetAutoDestroy(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("autoDestroy"))
				return (bool)store.UserConfig ["autoDestroy"];
			else
				return false;
		}
        
        #endregion AutoDestroy

        #region AutoLoad
        
		public static IStore SetAutoLoad(this IStore store, bool autoLoad)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["autoLoad"] = autoLoad;
            
			return store;
		}
        
		public static bool GetAutoLoad(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("autoLoad"))
				return (bool)store.UserConfig ["autoLoad"];
			else
				return false;
		}
        
        #endregion AutoLoad

        #region AutoSave
        
		public static IStore SetAutoSave(this IStore store, bool autoSave)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["autoSave"] = autoSave;
            
			return store;
		}
        
		public static bool GetAutoSave(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("autoSave"))
				return (bool)store.UserConfig ["autoSave"];
			else
				return true;
		}
        
        #endregion AutoSave

        #region BaseParams
        
		public static IStore SetBaseParams(this IStore store, object baseParams)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (baseParams == null && store.UserConfig.ContainsKey("baseParams"))
				store.UserConfig.Remove("baseParams");
			else
				store.UserConfig ["baseParams"] = baseParams;
            
			return store;
		}
        
		public static object GetBaseParams(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("baseParams"))
				return store.UserConfig ["baseParams"];
			else
				return null;
		}
        
        #endregion BaseParams

        #region Batch
        
		public static IStore SetBatch(this IStore store, bool batch)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["batch"] = batch;
            
			return store;
		}
        
		public static bool GetBatch(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("batch"))
				return (bool)store.UserConfig ["batch"];
			else
				return false;
		}
        
        #endregion Batch

        #region Data
        
		public static IStore SetData(this IStore store, object[] data)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (data == null)
				throw new ArgumentNullException("data");
            
			store.UserConfig ["data"] = data;
            
			return store;
		}
                
		public static IStore SetData(this IStore store, IList<object> data)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (data == null)
				throw new ArgumentNullException("data");
            
			store.UserConfig ["data"] = data;
            
			return store;
		}
                
		public static IList<object> GetData(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("data"))
				return  new List<object>((IList<object>)store.UserConfig ["data"]);
			else
				return null;
		}

        #endregion Data

        #region ParamNames
        
		public static IStore SetParamNames(this IStore store, object paramNames)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (paramNames == null && store.UserConfig.ContainsKey("paramNames"))
				store.UserConfig.Remove("paramNames");
			else
				store.UserConfig ["paramNames"] = paramNames;
            
			return store;
		}
        
		public static object GetParamNames(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("paramNames"))
				return store.UserConfig ["paramNames"];
			else
				return null;
		}
        
        #endregion ParamNames

        #region PruneModifiedRecords
        
		public static IStore SetPruneModifiedRecords(this IStore store, bool pruneModifiedRecords)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["pruneModifiedRecords"] = pruneModifiedRecords;
            
			return store;
		}
        
		public static bool GetPruneModifiedRecords(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("pruneModifiedRecords"))
				return (bool)store.UserConfig ["pruneModifiedRecords"];
			else
				return false;
		}
        
        #endregion PruneModifiedRecords

        #region RemoteSort
        
		public static IStore SetRemoteSort(this IStore store, bool remoteSort)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["remoteSort"] = remoteSort;
            
			return store;
		}
        
		public static bool GetRemoteSort(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("remoteSort"))
				return (bool)store.UserConfig ["remoteSort"];
			else
				return false;
		}
        
        #endregion RemoteSort

        #region Restful
        
		public static IStore SetRestful(this IStore store, bool restful)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			store.UserConfig ["restful"] = restful;
            
			return store;
		}
        
		public static bool GetRestful(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("restful"))
				return (bool)store.UserConfig ["restful"];
			else
				return false;
		}
        
        #endregion Restful

        #region SortDir
        
		public static IStore SetSortDir(this IStore store, SortDir sortDir)
		{
			if (store == null)
				throw new ArgumentNullException("store");

			string value = Enum.GetName(typeof(SortDir), sortDir).ToUpperInvariant();
			store.UserConfig ["sortDir"] = value;
            
			return store;
		}
        
		public static SortDir GetSortDir(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("sortDir"))
				return (SortDir)Enum.Parse(typeof(SortDir), (string)store.UserConfig ["sortDir"], true);
			else
				return SortDir.Asc;
		}

        #endregion SortDir

        #region SortField
        
		public static IStore SetSortField(this IStore store, string sortField)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (String.IsNullOrEmpty(sortField) && store.UserConfig.ContainsKey("sortField"))
				store.UserConfig.Remove("sortField");
			else
				store.UserConfig ["sortField"] = sortField;
            
			return store;
		}
        
		public static string GetSortField(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("sortField"))
				return (string)store.UserConfig ["sortField"];
			else
				return null;
		}
        
        #endregion SortField

        #region StoreId
        
		public static IStore SetStoreId(this IStore store, string storeId)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (String.IsNullOrEmpty(storeId) && store.UserConfig.ContainsKey("storeId"))
				store.UserConfig.Remove("storeId");
			else
				store.UserConfig ["storeId"] = storeId;
            
			return store;
		}
        
		public static string GetStoreId(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("storeId"))
				return (string)store.UserConfig ["storeId"];
			else
				return null;
		}
        
        #endregion StoreId  

        #region Url
        
		public static IStore SetUrl(this IStore store, string url)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (String.IsNullOrEmpty(url) && store.UserConfig.ContainsKey("url"))
				store.UserConfig.Remove("url");
			else
				store.UserConfig ["url"] = url;
            
			return store;
		}
        
		public static string GetUrl(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");
            
			if (store.UserConfig.ContainsKey("url"))
				return (string)store.UserConfig ["url"];
			else
				return null;
		}
        
        #endregion Url

        #endregion Getters/Setters

        #region Methods

		public static IDataField AddField(this IStore store)
		{
			if (store == null)
				throw new ArgumentNullException("store");

			return store.AddField(DataType.Auto);
		}

		public static IDataField AddField(this IStore store, DataType dataType)
		{
			IDataField field = null;
			if (store == null)
				throw new ArgumentNullException("store");

			switch (dataType)
			{
				case DataType.Auto:
					field = new DataField();
					break;

				case DataType.String:
					field = new StringField();
					break;

				case DataType.Int:
					field = new IntField();
					break;

				case DataType.Float:
					field = new FloatField();
					break;

				case DataType.Boolean:
					field = new BooleanField();
					break;

				case DataType.Date:
					field = new DateField();
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}

			if (store.Fields == null){
				store.Fields = new EnumerableId<IXDSItem>();
            }

            store.Fields.Add(field);

			return field;
		}

        #endregion Methods
	}           
}