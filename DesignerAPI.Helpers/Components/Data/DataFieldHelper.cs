
namespace DesignerAPI.Components.Data
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components.Data;
	using System.Collections.Generic;

	public static class DataFieldHelper
	{
        #region AllowBlank
        
		public static IDataField SetAllowBlank(this IDataField dataField, bool allowBlank)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["allowBlank"] = allowBlank;
            
			return dataField;
		}
        
		public static bool GetAllowBlank(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("allowBlank"))
				return (bool)dataField.UserConfig ["allowBlank"];
			else
				return false;
		}

        
        #endregion AllowBlank

        #region DateFormat
        
		public static IDataField SetDateFormat(this IDataField dataField, string dateFormat)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (String.IsNullOrEmpty(dateFormat) && dataField.UserConfig.ContainsKey("dateFormat"))
				dataField.UserConfig.Remove("dateFormat");
			else
				dataField.UserConfig ["dateFormat"] = dateFormat;
            
			return dataField;
		}
        
		public static string GetDateFormat(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("dateFormat"))
				return (string)dataField.UserConfig ["dateFormat"];
			else
				return null;
		}
        
        #endregion DateFormat       

        #region DefaultValue
        
		public static IDataField SetDefaultValue(this IDataField dataField, object defaultValue)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (defaultValue == null && dataField.UserConfig.ContainsKey("defaultValue"))
				dataField.UserConfig.Remove("defaultValue");
			else
				dataField.UserConfig ["defaultValue"] = defaultValue;
            
			return dataField;
		}
        
		public static object GetDefaultValue(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("defaultValue"))
				return dataField.UserConfig ["defaultValue"];
			else
				return null;
		}
        
        #endregion DefaultValue

        #region Mapping

		public static IDataField SetMapping(this IDataField dataField, string mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (String.IsNullOrEmpty(mapping) && dataField.UserConfig.ContainsKey("mapping"))
				dataField.UserConfig.Remove("mapping");
			else
				dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}

		public static IDataField SetMapping(this IDataField dataField, int mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}

        [CLSCompliantAttribute(false)]
		public static IDataField SetMapping(this IDataField dataField, uint mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}
        
		public static IDataField SetMapping(this IDataField dataField, long mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}

        [CLSCompliantAttribute(false)]
		public static IDataField SetMapping(this IDataField dataField, ulong mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}
        
		public static IDataField SetMapping(this IDataField dataField, short mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}

        [CLSCompliantAttribute(false)]
		public static IDataField SetMapping(this IDataField dataField, ushort mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}
        
		public static IDataField SetMapping(this IDataField dataField, decimal mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}
        
		public static IDataField SetMapping(this IDataField dataField, float mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}
        
		public static IDataField SetMapping(this IDataField dataField, double mapping)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			dataField.UserConfig ["mapping"] = mapping;
            
			return dataField;
		}

		public static Variant GetMapping(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("mapping"))
				return new Variant(dataField.UserConfig ["mapping"]);
			else
				return null;
		}
        
        #endregion Mapping

        #region Name
        
		public static IDataField SetName(this IDataField dataField, string name)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (String.IsNullOrEmpty(name) && dataField.UserConfig.ContainsKey("name"))
				dataField.UserConfig.Remove("name");
			else
				dataField.UserConfig ["name"] = name;
            
			return dataField;
		}
        
		public static string GetName(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("name"))
				return (string)dataField.UserConfig ["name"];
			else
				return null;
		}
        
        #endregion Name

        #region SortDir
        
		public static IDataField SetSortDir(this IDataField dataField, SortDir sortDir)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			string value = Enum.GetName(typeof(SortDir), sortDir).ToUpperInvariant();
			dataField.UserConfig ["sortDir"] = value;
            
			return dataField;
		}
        
		public static SortDir GetSortDir(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("sortDir"))
				return (SortDir)Enum.Parse(typeof(SortDir), (string)dataField.UserConfig ["sortDir"], true);
			else
				return SortDir.Asc;
		}
        
        #endregion SortDir

        #region DataType
        
		public static IDataField SetDataType(this IDataField dataField, DataType dataType)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			string value = Enum.GetName(typeof(DataType), dataType).ToLowerInvariant();
			dataField.UserConfig ["dataType"] = value;
            
			return dataField;
		}
        
		public static DataType GetDataType(this IDataField dataField)
		{
			if (dataField == null)
				throw new ArgumentNullException("dataField");
            
			if (dataField.UserConfig.ContainsKey("dataType"))
				return (DataType)Enum.Parse(typeof(DataType), (string)dataField.UserConfig ["dataType"], true);
			else
				return DataType.Auto;
		}
        
        #endregion DataType
	}               
}