//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
    using System;
    using DesignerAPI.Model;
    using System.Collections.Generic;

    ///<summary>
    /// Abstract component helper.
    ///</summary>
    public static class AbstractComponentHelper
    {
        ///<summary>
        /// Sets the auto el.
        ///</summary>
        /// <returns>
        /// The auto el.
        /// </returns>
        /// <param name='abstractComponent'>
        /// Abstract component.
        /// </param>
        /// <param name='autoEl'>
        /// Auto el.
        /// </param>
        public static IAbstractComponent SetAutoEl (this IAbstractComponent abstractComponent, string autoEl)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");

            if (String.IsNullOrEmpty (autoEl) && abstractComponent.UserConfig.ContainsKey ("autoEl"))
                abstractComponent.UserConfig.Remove ("autoEl");
            else
                abstractComponent.UserConfig ["autoEl"] = autoEl;

            return abstractComponent;
        }

        ///<summary>
        /// Sets the auto el.
        ///</summary>
        /// <returns>
        /// The auto el.
        /// </returns>
        /// <param name='abstractComponent'>
        /// Abstract component.
        /// </param>
        /// <param name='autoEl'>
        /// Auto el.
        /// </param>
        public static IAbstractComponent SetAutoEl (this IAbstractComponent abstractComponent, object autoEl)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (autoEl == null && abstractComponent.UserConfig.ContainsKey ("autoEl"))
                abstractComponent.UserConfig.Remove ("autoEl");
            else
                abstractComponent.UserConfig ["autoEl"] = autoEl;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetAutoRender (this IAbstractComponent abstractComponent, string autoRender)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (autoRender) && abstractComponent.UserConfig.ContainsKey ("autoRender"))
                abstractComponent.UserConfig.Remove ("autoRender");
            else
                abstractComponent.UserConfig ["autoRender"] = autoRender;
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetAutoRender (this IAbstractComponent abstractComponent, bool autoRender)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["autoRender"] = autoRender;
            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetAutoRender (this IAbstractComponent abstractComponent, object autoRender)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (autoRender == null && abstractComponent.UserConfig.ContainsKey ("autoRender"))
                abstractComponent.UserConfig.Remove ("autoRender");
            else
                abstractComponent.UserConfig ["autoRender"] = autoRender;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetAutoShow (this IAbstractComponent abstractComponent, bool autoShow)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["autoShow"] = autoShow;
            
            
            return abstractComponent;
        }

        public static IAbstractComponent SetBorder (this IAbstractComponent abstractComponent, string border)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (border) && abstractComponent.UserConfig.ContainsKey ("border"))
                abstractComponent.UserConfig.Remove ("border");
            else
                abstractComponent.UserConfig ["border"] = border;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetBorder (this IAbstractComponent abstractComponent, int border)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["border"] = border;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetBorder (this IAbstractComponent abstractComponent, uint border)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["border"] = border;
            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetBorder (this IAbstractComponent abstractComponent, long border)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["border"] = border;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetBorder (this IAbstractComponent abstractComponent, ulong border)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["border"] = border;
            
            
            return abstractComponent;
        }

        public static IAbstractComponent SetCls (this IAbstractComponent abstractComponent, string cls)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (cls) && abstractComponent.UserConfig.ContainsKey ("cls"))
                abstractComponent.UserConfig.Remove ("cls");
            else
                abstractComponent.UserConfig ["cls"] = cls;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetComponentLayout (this IAbstractComponent abstractComponent, object componentLayout)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (componentLayout == null && abstractComponent.UserConfig.ContainsKey ("componentLayout"))
                abstractComponent.UserConfig.Remove ("componentLayout");
            else
                abstractComponent.UserConfig ["componentLayout"] = componentLayout;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetContentEl (this IAbstractComponent abstractComponent, string contentEl)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (contentEl) && abstractComponent.UserConfig.ContainsKey ("contentEl"))
                abstractComponent.UserConfig.Remove ("contentEl");
            else
                abstractComponent.UserConfig ["contentEl"] = contentEl;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetData (this IAbstractComponent abstractComponent, object data)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (data == null && abstractComponent.UserConfig.ContainsKey ("data"))
                abstractComponent.UserConfig.Remove ("data");
            else
                abstractComponent.UserConfig ["data"] = data;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetDisabled (this IAbstractComponent abstractComponent, bool disabled)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["disabled"] = disabled;           
            
            return abstractComponent;
        }

        public static IAbstractComponent SetFloating (this IAbstractComponent abstractComponent, bool floating)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["floating"] = floating;           
            
            return abstractComponent;
        }

        #region Height

        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, int? height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");            

            if (!height.HasValue && abstractComponent.UserConfig.ContainsKey ("height")) {
                abstractComponent.UserConfig.Remove ("height");
            } else {
                abstractComponent.UserConfig ["height"] = height.Value;
            }
            
            return abstractComponent;
        }

        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, int? height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, int height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, int height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, uint height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, uint height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, long height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, long height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, ulong height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, ulong height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, short height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, short height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, ushort height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, ushort height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, decimal height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, decimal height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, float height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, float height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static IAbstractComponent SetHeight (this IAbstractComponent abstractComponent, double height)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["height"] = height;
            
            return abstractComponent;
        }
        
        public static TEntity SetHeight <TEntity> (this IAbstractComponent abstractComponent, double height)
        {
            return (TEntity)abstractComponent.SetHeight (height);
        }
        
        public static Variant GetHeight (this IAbstractComponent abstractComponent)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (abstractComponent.UserConfig.ContainsKey ("height"))
                return new Variant (abstractComponent.UserConfig ["height"]);
            else
                return null;
        }
        
        #endregion Height    

        public static IAbstractComponent SetHidden (this IAbstractComponent abstractComponent, bool hidden)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["hidden"] = hidden;
            
            
            return abstractComponent;
        }

        public static IAbstractComponent SetHideMode (this IAbstractComponent abstractComponent, string hideMode)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (hideMode) && abstractComponent.UserConfig.ContainsKey ("hideMode"))
                abstractComponent.UserConfig.Remove ("hideMode");
            else
                abstractComponent.UserConfig ["hideMode"] = hideMode;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetHtml (this IAbstractComponent abstractComponent, object html)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (html == null && abstractComponent.UserConfig.ContainsKey ("html"))
                abstractComponent.UserConfig.Remove ("html");
            else
                abstractComponent.UserConfig ["html"] = html;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetId (this IAbstractComponent abstractComponent, string id)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (id) && abstractComponent.UserConfig.ContainsKey ("id"))
                abstractComponent.UserConfig.Remove ("id");
            else
                abstractComponent.UserConfig ["id"] = id;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetItemId (this IAbstractComponent abstractComponent, string itemId)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (itemId) && abstractComponent.UserConfig.ContainsKey ("itemId"))
                abstractComponent.UserConfig.Remove ("itemId");
            else
                abstractComponent.UserConfig ["itemId"] = itemId;
            
            return abstractComponent;
        }

        public static TEntity SetItemId<TEntity> (this IAbstractComponent abstractComponent, string itemId)
        {                        
            return (TEntity)abstractComponent.SetItemId (itemId);
        }
//
        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, string margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (margin) && abstractComponent.UserConfig.ContainsKey ("margin"))
                abstractComponent.UserConfig.Remove ("margin");
            else
                abstractComponent.UserConfig ["margin"] = margin;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, int margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["margin"] = margin;            
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, uint margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["margin"] = margin;            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, long margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["margin"] = margin;            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, ulong margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["margin"] = margin;            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, short margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["margin"] = margin;
                        
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMargin (this IAbstractComponent abstractComponent, ushort margin)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["margin"] = margin;
                        
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMaxHeight (this IAbstractComponent abstractComponent, int maxHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxHeight"] = maxHeight;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxHeight (this IAbstractComponent abstractComponent, uint maxHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxHeight"] = maxHeight;
                        
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMaxHeight (this IAbstractComponent abstractComponent, long maxHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxHeight"] = maxHeight;
                        
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxHeight (this IAbstractComponent abstractComponent, ulong maxHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxHeight"] = maxHeight;
                        
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMaxHeight (this IAbstractComponent abstractComponent, short maxHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxHeight"] = maxHeight;
                        
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxHeight (this IAbstractComponent abstractComponent, ushort maxHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxHeight"] = maxHeight;
                        
            return abstractComponent;
        }
        /*
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, int maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxWidth"] = maxWidth;            
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, uint maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxWidth"] = maxWidth;
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, long maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxWidth"] = maxWidth;            
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, ulong maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxWidth"] = maxWidth;            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, short maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxWidth"] = maxWidth;            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, ushort maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["maxWidth"] = maxWidth;            
            
            return abstractComponent;
        }
*/
        #region MaxWidth 
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, int maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, int maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, uint maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, uint maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, long maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, long maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, ulong maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, ulong maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, short maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, short maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, ushort maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, ushort maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, decimal maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, decimal maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, float maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, float maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, double maxWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            abstractComponent.UserConfig [" maxwidth"] =  maxWidth;
            
            return abstractComponent;
        }
        
        public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, double maxWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
        }
        
        public static Variant GetX (this IAbstractComponent abstractComponent)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException("abstractComponent");
            
            if (abstractComponent.UserConfig.ContainsKey("maxWidth"))
                return new Variant(abstractComponent.UserConfig ["maxWidth"]);
            else
                return null;
        }
        
        #endregion MaxWidth 

        public static IAbstractComponent SetMinHeight (this IAbstractComponent abstractComponent, int minHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minHeight"] = minHeight;
                        
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMinHeight (this IAbstractComponent abstractComponent, uint minHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minHeight"] = minHeight;            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMinHeight (this IAbstractComponent abstractComponent, long minHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minHeight"] = minHeight;           
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMinHeight (this IAbstractComponent abstractComponent, ulong minHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minHeight"] = minHeight;
                        
            return abstractComponent;
        }
        
        public static IAbstractComponent SetMinHeight (this IAbstractComponent abstractComponent, short minHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minHeight"] = minHeight;
                        
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMinHeight (this IAbstractComponent abstractComponent, ushort minHeight)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minHeight"] = minHeight;
                        
            return abstractComponent;
        }

        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, bool minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;
                        
            return abstractComponent;
        }

        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, bool minWidth)
            where TEntity : IAbstractComponent
        {
            
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }

        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, int minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;           
            
            return abstractComponent;
        }

        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, int minWidth)
            where TEntity : IAbstractComponent
        {
          
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");

            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, uint minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, uint minWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
         
            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }
        
        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, long minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;            
            
            return abstractComponent;
        }

        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, long minWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, ulong minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, ulong minWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }
        
        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, short minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;            
            
            return abstractComponent;
        }

        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, short minWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetMinWidth (this IAbstractComponent abstractComponent, ushort minWidth)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["minWidth"] = minWidth;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetMinWidth<TEntity> (this IAbstractComponent abstractComponent, ushort minWidth)
            where TEntity : IAbstractComponent
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            return (TEntity)abstractComponent.SetMinWidth (minWidth);
        }

        public static IAbstractComponent SetOverCls (this IAbstractComponent abstractComponent, string overCls)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (overCls) && abstractComponent.UserConfig.ContainsKey ("overCls"))
                abstractComponent.UserConfig.Remove ("overCls");
            else
                abstractComponent.UserConfig ["overCls"] = overCls;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, string padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (padding) && abstractComponent.UserConfig.ContainsKey ("padding"))
                abstractComponent.UserConfig.Remove ("padding");
            else
                abstractComponent.UserConfig ["padding"] = padding;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, int padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["padding"] = padding;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, uint padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["padding"] = padding;
            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, long padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["padding"] = padding;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, ulong padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["padding"] = padding;
            
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, short padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["padding"] = padding;
            
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetPadding (this IAbstractComponent abstractComponent, ushort padding)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["padding"] = padding;
            
            
            return abstractComponent;
        }

        public static IAbstractComponent SetStyle (this IAbstractComponent abstractComponent, string style)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (style) && abstractComponent.UserConfig.ContainsKey ("style"))
                abstractComponent.UserConfig.Remove ("style");
            else
                abstractComponent.UserConfig ["style"] = style;
            
            return abstractComponent;
        }

        public static TEntity SetStyle<TEntity> (this IAbstractComponent abstractComponent, string style)
        {
            return (TEntity)abstractComponent.SetStyle (style);
        }
                
        public static IAbstractComponent SetStyle (this IAbstractComponent abstractComponent, object style)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (style == null && abstractComponent.UserConfig.ContainsKey ("style"))
                abstractComponent.UserConfig.Remove ("style");
            else
                abstractComponent.UserConfig ["style"] = style;
            
            return abstractComponent;
        }

        public static TEntity SetStyle<TEntity> (this IAbstractComponent abstractComponent, object style)
        {
            return (TEntity)abstractComponent.SetStyle (style);
        }

        public static IAbstractComponent SetStyleHtmlContent (this IAbstractComponent abstractComponent, bool styleHtmlContent)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["styleHtmlContent"] = styleHtmlContent;
            
            
            return abstractComponent;
        }

        public static IAbstractComponent SetTpl (this IAbstractComponent abstractComponent, string tpl)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (tpl) && abstractComponent.UserConfig.ContainsKey ("tpl"))
                abstractComponent.UserConfig.Remove ("tpl");
            else
                abstractComponent.UserConfig ["tpl"] = tpl;
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetTpl (this IAbstractComponent abstractComponent, string[] tpl)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (tpl == null)
                throw new ArgumentNullException ("tpl");
            
            abstractComponent.UserConfig ["tpl"] = tpl;
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetTpl (this IAbstractComponent abstractComponent, IList<string> tpl)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (tpl == null)
                throw new ArgumentNullException ("tpl");
            
            abstractComponent.UserConfig ["tpl"] = tpl;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetTplWriteMode (this IAbstractComponent abstractComponent, string tplWriteMode)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (tplWriteMode) && abstractComponent.UserConfig.ContainsKey ("tplWriteMode"))
                abstractComponent.UserConfig.Remove ("tplWriteMode");
            else
                abstractComponent.UserConfig ["tplWriteMode"] = tplWriteMode;
            
            return abstractComponent;
        }

        public static IAbstractComponent SetUi (this IAbstractComponent abstractComponent, string ui)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (String.IsNullOrEmpty (ui) && abstractComponent.UserConfig.ContainsKey ("ui"))
                abstractComponent.UserConfig.Remove ("ui");
            else
                abstractComponent.UserConfig ["ui"] = ui;
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetUi (this IAbstractComponent abstractComponent, string[] ui)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (ui == null)
                throw new ArgumentNullException ("ui");
            
            abstractComponent.UserConfig ["ui"] = ui;
            
            return abstractComponent;
        }
        
        public static IAbstractComponent SetUi (this IAbstractComponent abstractComponent, IList<string> ui)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (ui == null)
                throw new ArgumentNullException ("ui");
            
            abstractComponent.UserConfig ["ui"] = ui;
            
            return abstractComponent;
        }

        #region Width

        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, int? width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");            
            
            if (!width.HasValue && abstractComponent.UserConfig.ContainsKey ("width")) {
                abstractComponent.UserConfig.Remove ("width");
            } else {
                abstractComponent.UserConfig ["width"] = width.Value;
            }
            
            return abstractComponent;
        }

        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, int? width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }

        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, int width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, int width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, uint width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, uint width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }
        
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, long width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, long width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, ulong width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, ulong width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }
        
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, short width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, short width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }

        [CLSCompliantAttribute(false)]
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, ushort width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, ushort width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }
        
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, decimal width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, decimal width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }
        
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, float width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, float width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }
        
        public static IAbstractComponent SetWidth (this IAbstractComponent abstractComponent, double width)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            abstractComponent.UserConfig ["width"] = width;
            
            return abstractComponent;
        }
        
        public static TEntity SetWidth <TEntity> (this IAbstractComponent abstractComponent, double width)
        {
            return (TEntity)abstractComponent.SetWidth (width);
        }
        
        public static Variant GetWidth (this IAbstractComponent abstractComponent)
        {
            if (abstractComponent == null)
                throw new ArgumentNullException ("abstractComponent");
            
            if (abstractComponent.UserConfig.ContainsKey ("width"))
                return new Variant (abstractComponent.UserConfig ["width"]);
            else
                return null;
        }
        
        #endregion Width    
    }       
}