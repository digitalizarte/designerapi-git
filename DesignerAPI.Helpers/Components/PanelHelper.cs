//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
    using System;
    using System.Collections.Generic;
    using DesignerAPI.Builder;
    using DesignerAPI.Components;
    using DesignerAPI.Components.Form;
    using DesignerAPI.Components.Tree;
    using DesignerAPI.Components.Grid;
    using DesignerAPI.Model;

    ///<summary>
    /// Panel helper.
    ///</summary>
    public static class PanelHelper
    {
        public static ITreePanel AddTreePanel(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ITreePanel tree = new TreePanel();
            panel.Components.Add(tree);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return tree;
        }

        public static IGridPanel AddGridPanel(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IGridPanel grid = new GridPanel();
            panel.Components.Add(grid);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return grid;
        }

        public static IEditorGridPanel AddEditorGridPanel(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IEditorGridPanel grid = new EditorGridPanel();
            panel.Components.Add(grid);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return grid;
        }

        public static IContainer AddContainer(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IContainer container = new Container();
            panel.Components.Add(container);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return container;
        }

        public static ITabPanel AddTabPanel(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ITabPanel tabPanel = new TabPanel();
            panel.Components.Add(tabPanel);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return tabPanel;
        }

        public static IFieldSet AddFieldSet(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IFieldSet fieldSet = new FieldSet();
            panel.Components.Add(fieldSet);
            ProjectBuilderHelper.LatestComponent.Push(panel);
            return fieldSet;
        }

        public static ICheckbox AddCheckbox(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ICheckbox checkbox = new Checkbox();
            panel.Components.Add(checkbox);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return checkbox;
        }

        public static ICheckboxGroup AddCheckboxGroup(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ICheckboxGroup checkboxGroup = new CheckboxGroup();
            panel.Components.Add(checkboxGroup);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return checkboxGroup;
        }

        public static IComboBox AddComboBox(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IComboBox comboBox = new ComboBox();
            panel.Components.Add(comboBox);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return comboBox;
        }

        public static ICompositeField AddCompositeField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ICompositeField compositeField = new CompositeField();
            panel.Components.Add(compositeField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return compositeField;
        }

        public static DesignerAPI.Components.Form.IDateField AddDateField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            DesignerAPI.Components.Form.IDateField dateField = new DesignerAPI.Components.Form.DateField();
            panel.Components.Add(dateField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return dateField;
        }

        public static IDisplayField AddDisplayField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IDisplayField displayField = new DisplayField();
            panel.Components.Add(displayField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return displayField;
        }

        public static IHidden AddHidden(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IHidden hidden = new Hidden();
            panel.Components.Add(hidden);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return hidden;
        }

        public static IHtmlEditor AddHtmlEditor(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IHtmlEditor htmlEditor = new HtmlEditor();
            panel.Components.Add(htmlEditor);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return htmlEditor;
        }

        public static INumberField AddNumberField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            INumberField numberField = new NumberField();
            panel.Components.Add(numberField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return numberField;
        }

        public static IRadio AddRadio(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IRadio radio = new Radio();
            panel.Components.Add(radio);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return radio;
        }

        public static IRadioGroup AddRadioGroup(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            IRadioGroup radioGroup = new RadioGroup();
            panel.Components.Add(radioGroup);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return radioGroup;
        }

        public static ISliderField AddSliderField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ISliderField sliderField = new SliderField();
            panel.Components.Add(sliderField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return sliderField;
        }

        public static ITextArea AddTextArea(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ITextArea textArea = new TextArea();
            panel.Components.Add(textArea);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return textArea;
        }

        public static ITextField AddTextField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ITextField textField = new TextField();
            panel.Components.Add(textField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return textField;
        }

        public static ITimeField AddTimeField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ITimeField timeField = new TimeField();
            panel.Components.Add(timeField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return timeField;
        }

        public static ITriggerField AddTriggerField(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            ITriggerField triggerField = new TriggerField();
            panel.Components.Add(triggerField);
            ProjectBuilderHelper.LatestComponent.Push(panel);

            return triggerField;
        }

        public static IPanel SetAnimCollapse(this IPanel panel, bool animCollapse)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            panel.UserConfig["animCollapse"] = animCollapse;

            return panel;
        }

        public static bool GetAnimCollapse(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            if (panel.UserConfig.ContainsKey("animCollapse"))
                return (bool)panel.UserConfig["animCollapse"];
            else
                return false;
        }

        public static IPanel SetAutoHeight(this IPanel panel, bool autoHeight)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            panel.UserConfig["autoHeight"] = autoHeight;
            
            return panel;
        }

        public static bool GetAutoHeight(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            bool ret = false;
            if (panel.UserConfig.ContainsKey("autoHeight"))
            {
                ret = (bool)panel.UserConfig["autoHeight"];
            }

            return ret;

        }

        public static IPanel SetAutoLoad(this IPanel panel, string autoLoad)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            if (String.IsNullOrEmpty(autoLoad) && panel.UserConfig.ContainsKey("autoLoad"))
            {
                panel.UserConfig.Remove("autoLoad");
            }
            else
            {
                panel.UserConfig["autoLoad"] = autoLoad;
            }

            return panel;
        }

        public static string GetAutoLoad(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            object value;
            if (panel.UserConfig.TryGetValue("autoLoad", out value))
                return (string)value;
            else
                return null;
        }

        public static IPanel SetAutoLoad(this IPanel panel, object autoLoad)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (autoLoad == null && panel.UserConfig.ContainsKey("autoLoad"))
                panel.UserConfig.Remove("autoLoad");
            else
                panel.UserConfig["autoLoad"] = autoLoad;

            return panel;
        }

        public static IPanel SetBaseCls(this IPanel panel, string baseCls)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(baseCls) && panel.UserConfig.ContainsKey("baseCls"))
                panel.UserConfig.Remove("baseCls");
            else
                panel.UserConfig["baseCls"] = baseCls;

            return panel;
        }

        public static string GetBaseCls(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            object value;
            if (panel.UserConfig.TryGetValue("baseCls", out value))
                return (string)value;
            else
                return null;
        }

        public static IPanel SetBodyCssClass(this IPanel panel, string bodyCssClass)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(bodyCssClass) && panel.UserConfig.ContainsKey("bodyCssClass"))
                panel.UserConfig.Remove("bodyCssClass");
            else
                panel.UserConfig["bodyCssClass"] = bodyCssClass;

            return panel;
        }

        public static IPanel SetBodyCssClass(this IPanel panel, object bodyCssClass)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (bodyCssClass == null && panel.UserConfig.ContainsKey("bodyCssClass"))
                panel.UserConfig.Remove("bodyCssClass");
            else
                panel.UserConfig["bodyCssClass"] = bodyCssClass;

            return panel;
        }

        public static Variant GetBodyCssClass(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            object value;
            if (panel.UserConfig.TryGetValue("bodyCssClass", out value))
                return new Variant(value);
            else
                return null;
        }

        public static IPanel SetBodyStyle(this IPanel panel, string bodyStyle)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(bodyStyle) && panel.UserConfig.ContainsKey("bodyStyle"))
                panel.UserConfig.Remove("bodyStyle");
            else
                panel.UserConfig["bodyStyle"] = bodyStyle;

            return panel;
        }

        public static IPanel SetBodyStyle(this IPanel panel, object bodyStyle)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (bodyStyle == null && panel.UserConfig.ContainsKey("bodyStyle"))
                panel.UserConfig.Remove("bodyStyle");
            else
                panel.UserConfig["bodyStyle"] = bodyStyle;

            return panel;
        }

        public static Variant GetBodyStyle(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            object value;
            if (panel.UserConfig.TryGetValue("bodyStyle", out value))
                return new Variant(value);
            else
                return null;
        }

        public static IPanel SetBorder(this IPanel panel, bool border)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["border"] = border;


            return panel;
        }

        public static bool GetBorder(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            object value;
            if (panel.UserConfig.TryGetValue("border", out value))
                return (bool)value;
            else
                return false;
        }

        public static IPanel SetButtonAlign(this IPanel panel, string buttonAlign)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(buttonAlign) && panel.UserConfig.ContainsKey("buttonAlign"))
                panel.UserConfig.Remove("buttonAlign");
            else
            {
                if (!String.IsNullOrEmpty(buttonAlign))
                {
                    string value = buttonAlign.ToLower();
                    if (value == "right" || value == "left" || value == "center")
                    {
                        panel.UserConfig["buttonAlign"] = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("buttonAlign");
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException("buttonAlign");
                }
            }

            return panel;
        }

        public static IPanel SetButtonAlign(this IPanel panel, ButtonAlign buttonAlign)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            panel.UserConfig["buttonAlign"] = buttonAlign.ToString().ToLower();

            return panel;
        }

        public static string GetButtonAlign(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            object value;
            if (panel.UserConfig.TryGetValue("buttonAlign", out value))
                return (string)value;
            else
                return null;
        }

        public static IPanel SetClosable(this IPanel panel, bool closable)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            panel.UserConfig["closable"] = closable;


            return panel;
        }

        public static bool GetClosable(this IPanel panel)
        {
            if (panel == null)
            {
                throw new ArgumentNullException("panel");
            }

            object value;
            if (panel.UserConfig.TryGetValue("closable", out value))
                return (bool)value;
            else
                return false;
        }

        public static IPanel SetCollapsible(this IPanel panel, bool collapsible)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["collapsible"] = collapsible;


            return panel;
        }

        public static bool GetCollapsible(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            object value;
            if (panel.UserConfig.TryGetValue("collapsible", out value))
                return (bool)value;
            else
                return false;
        }

        public static IPanel SetDisabled(this IPanel panel, bool disabled)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["disabled"] = disabled;


            return panel;
        }

        public static bool GetDisabled(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("disabled"))
                return (bool)panel.UserConfig["disabled"];
            else
                return false;
        }

        public static IPanel SetDraggable(this IPanel panel, bool draggable)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["draggable"] = draggable;


            return panel;
        }

        public static bool GetDraggable(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("draggable"))
                return (bool)panel.UserConfig["draggable"];
            else
                return false;
        }

        public static IPanel SetElements(this IPanel panel, string elements)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(elements) && panel.UserConfig.ContainsKey("elements"))
                panel.UserConfig.Remove("elements");
            else
                panel.UserConfig["elements"] = elements;

            return panel;
        }

        public static string GetElements(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("elements"))
                return (string)panel.UserConfig["elements"];
            else
                return null;
        }

        public static IPanel SetFloating(this IPanel panel, bool floating)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["floating"] = floating;


            return panel;
        }

        public static bool GetFloating(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("floating"))
                return (bool)panel.UserConfig["floating"];
            else
                return false;
        }

        public static IPanel SetFooter(this IPanel panel, bool footer)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["footer"] = footer;


            return panel;
        }

        public static bool GetFooter(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("footer"))
                return (bool)panel.UserConfig["footer"];
            else
                return false;
        }

        #region Frame

        public static IPanel SetFrame(this IPanel panel, bool frame)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["frame"] = frame;


            return panel;
        }

        public static TEntity SetFrame<TEntity>(this IPanel panel, bool frame)
        {
            return (TEntity)panel.SetFrame(frame);
        }

        public static bool GetFrame(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("frame"))
                return (bool)panel.UserConfig["frame"];
            else
                return false;
        }

        #endregion Frame

        public static IPanel SetHeader(this IPanel panel, bool header)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["header"] = header;


            return panel;
        }

        public static TEntity SetHeader<TEntity>(this IPanel panel, bool header)
            where TEntity : IPanel
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            return (TEntity)panel.SetHeader(header);
        }

        public static bool GetHeader(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("header"))
                return (bool)panel.UserConfig["header"];
            else
                return false;
        }

        public static IPanel SetHeaderAsText(this IPanel panel, bool headerAsText)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["headerAsText"] = headerAsText;


            return panel;
        }

        public static bool GetHeaderAsText(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("headerAsText"))
                return (bool)panel.UserConfig["headerAsText"];
            else
                return false;
        }

        public static IPanel SetHideCollapseTool(this IPanel panel, bool hideCollapseTool)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["hideCollapseTool"] = hideCollapseTool;


            return panel;
        }

        public static bool GetHideCollapseTool(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("hideCollapseTool"))
                return (bool)panel.UserConfig["hideCollapseTool"];
            else
                return false;
        }

        public static IPanel SetIconCls(this IPanel panel, string iconCls)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(iconCls) && panel.UserConfig.ContainsKey("iconCls"))
                panel.UserConfig.Remove("iconCls");
            else
                panel.UserConfig["iconCls"] = iconCls;

            return panel;
        }

        public static string GetIconCls(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("iconCls"))
                return (string)panel.UserConfig["iconCls"];
            else
                return null;
        }

        public static IPanel SetMaskDisabled(this IPanel panel, bool maskDisabled)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["maskDisabled"] = maskDisabled;


            return panel;
        }

        public static bool GetMaskDisabled(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("maskDisabled"))
                return (bool)panel.UserConfig["maskDisabled"];
            else
                return false;
        }

        public static IPanel SetPadding(this IPanel panel, string padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(padding) && panel.UserConfig.ContainsKey("padding"))
                panel.UserConfig.Remove("padding");
            else
                panel.UserConfig["padding"] = padding;

            return panel;
        }

        public static TEntity SetPadding<TEntity>(this IPanel panel, string padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            return (TEntity)panel.SetPadding(padding);
        }

        public static IPanel SetPadding(this IPanel panel, int padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["padding"] = padding;


            return panel;
        }
        public static TEntity SetPadding<TEntity>(this IPanel panel, int padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            return (TEntity)panel.SetPadding(padding);
        }

        [CLSCompliantAttribute(false)]
        public static IPanel SetPadding(this IPanel panel, uint padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["padding"] = padding;


            return panel;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetPadding<TEntity>(this IPanel panel, uint padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");
            return (TEntity)panel.SetPadding(padding);
        }

        public static IPanel SetPadding(this IPanel panel, long padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["padding"] = padding;


            return panel;
        }

        public static TEntity SetPadding<TEntity>(this IPanel panel, long padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");
            return (TEntity)panel.SetPadding(padding);
        }

        [CLSCompliantAttribute(false)]
        public static IPanel SetPadding(this IPanel panel, ulong padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["padding"] = padding;

            return panel;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetPadding<TEntity>(this IPanel panel, ulong padding)
        {
            return (TEntity)panel.SetPadding(padding);
        }

        public static IPanel SetPadding(this IPanel panel, short padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["padding"] = padding;

            return panel;
        }

        public static TEntity SetPadding<TEntity>(this IPanel panel, short padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");
            return (TEntity)panel.SetPadding(padding);
        }

        [CLSCompliantAttribute(false)]
        public static IPanel SetPadding(this IPanel panel, ushort padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["padding"] = padding;

            return panel;
        }

        [CLSCompliantAttribute(false)]
        public static TEntity SetPadding<TEntity>(this IPanel panel, ushort padding)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");
            return (TEntity)panel.SetPadding(padding);
        }

        public static Variant GetPadding(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("padding"))
                return new Variant(panel.UserConfig["padding"]);
            else
                return null;
        }

        public static IPanel SetPreventBodyReset(this IPanel panel, bool preventBodyReset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["preventBodyReset"] = preventBodyReset;


            return panel;
        }

        public static bool GetPreventBodyReset(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("preventBodyReset"))
                return (bool)panel.UserConfig["preventBodyReset"];
            else
                return false;
        }

        public static IPanel SetResizeEvent(this IPanel panel, string resizeEvent)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(resizeEvent) && panel.UserConfig.ContainsKey("resizeEvent"))
                panel.UserConfig.Remove("resizeEvent");
            else
                panel.UserConfig["resizeEvent"] = resizeEvent;

            return panel;
        }

        public static string GetResizeEvent(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("resizeEvent"))
                return (string)panel.UserConfig["resizeEvent"];
            else
                return null;
        }

        public static IPanel SetShadow(this IPanel panel, string shadow)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(shadow) && panel.UserConfig.ContainsKey("shadow"))
                panel.UserConfig.Remove("shadow");
            else
            {

                if (!String.IsNullOrEmpty(shadow))
                {
                    var value = shadow.ToLower();
                    if (value == "sides" || value == "frame" || value == "drop")
                    {
                        panel.UserConfig["shadow"] = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("shadow");
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException("shadow");
                }
            }

            return panel;
        }

        public static IPanel SetShadow(this IPanel panel, Shadow shadow)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadow"] = shadow.ToString().ToLower();

            return panel;
        }

        public static IPanel SetShadow(this IPanel panel, bool shadow)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadow"] = shadow;


            return panel;
        }

        public static Variant GetShadow(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("shadow"))
                return new Variant(panel.UserConfig["shadow"]);
            else
                return null;
        }

        public static IPanel SetShadowOffset(this IPanel panel, int shadowOffset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadowOffset"] = shadowOffset;


            return panel;
        }

        [CLSCompliantAttribute(false)]
        public static IPanel SetShadowOffset(this IPanel panel, uint shadowOffset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadowOffset"] = shadowOffset;


            return panel;
        }

        public static IPanel SetShadowOffset(this IPanel panel, long shadowOffset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadowOffset"] = shadowOffset;


            return panel;
        }

        [CLSCompliantAttribute(false)]
        public static IPanel SetShadowOffset(this IPanel panel, ulong shadowOffset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadowOffset"] = shadowOffset;


            return panel;
        }

        public static IPanel SetShadowOffset(this IPanel panel, short shadowOffset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadowOffset"] = shadowOffset;


            return panel;
        }

        [CLSCompliantAttribute(false)]
        public static IPanel SetShadowOffset(this IPanel panel, ushort shadowOffset)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shadowOffset"] = shadowOffset;


            return panel;
        }

        public static Variant GetShadowOffset(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("shadowOffset"))
                return new Variant(panel.UserConfig["shadowOffset"]);
            else
                return null;
        }

        public static IPanel SetShim(this IPanel panel, bool shim)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["shim"] = shim;


            return panel;
        }

        public static bool GetShim(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("shim"))
                return (bool)panel.UserConfig["shim"];
            else
                return false;
        }

        public static IPanel SetTitle(this IPanel panel, string title)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(title) && panel.UserConfig.ContainsKey("title"))
                panel.UserConfig.Remove("title");
            else
                panel.UserConfig["title"] = title;

            return panel;
        }

        public static TEntity SetTitle<TEntity>(this IPanel panel, string title)
        {
            return (TEntity)panel.SetTitle(title);
        }

        public static string GetTitle(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("title"))
                return (string)panel.UserConfig["title"];
            else
                return null;
        }

        public static IPanel SetTitleCollapse(this IPanel panel, bool titleCollapse)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["titleCollapse"] = titleCollapse;


            return panel;
        }

        public static bool GetTitleCollapse(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("titleCollapse"))
                return (bool)panel.UserConfig["titleCollapse"];
            else
                return false;
        }

        public static IPanel SetUnstyled(this IPanel panel, bool unstyled)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["unstyled"] = unstyled;


            return panel;
        }

        public static bool GetUnstyled(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("unstyled"))
                return (bool)panel.UserConfig["unstyled"];
            else
                return false;
        }


        #region CollapseMode

        public static IPanel SetCollapseMode(this IPanel panel, string collapseMode)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (String.IsNullOrEmpty(collapseMode) && panel.UserConfig.ContainsKey("collapseMode"))
                panel.UserConfig.Remove("collapseMode");
            else
                panel.UserConfig["collapseMode"] = collapseMode;

            return panel;
        }

        public static TEntity SetCollapseMode<TEntity>(this IPanel panel, string collapseMode)
            where TEntity : IPanel
        {
            if (panel == null)
                throw new ArgumentNullException("panel");
            return (TEntity)panel.SetCollapseMode(collapseMode);
        }

        public static string GetCollapseMode(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("collapseMode"))
                return (string)panel.UserConfig["collapseMode"];
            else
                return null;
        }

        #endregion CollapseMode

        #region Split

        public static IPanel SetSplit(this IPanel panel, bool split)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            panel.UserConfig["split"] = split;

            return panel;
        }

        public static TEntity SetSplit<TEntity>(this IPanel panel, bool split)
            where TEntity : IPanel
        {
            if (panel == null)
                throw new ArgumentNullException("panel");
            return (TEntity)panel.SetSplit(split);
        }

        public static bool GetSplit(this IPanel panel)
        {
            if (panel == null)
                throw new ArgumentNullException("panel");

            if (panel.UserConfig.ContainsKey("split"))
                return (bool)panel.UserConfig["split"];
            else
                return false;
        }

        #endregion Split

    }
}
