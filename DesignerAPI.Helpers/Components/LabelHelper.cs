//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace  DesignerAPI.Components
{
    using System;
	using DesignerAPI.Components;

	public static class LabelHelper
	{

        #region ForId
        
		public static ILabel SetForId(this ILabel label, string forId)
		{
			if (label == null)
				throw new ArgumentNullException("label");
            
			if (String.IsNullOrEmpty(forId) && label.UserConfig.ContainsKey("forId"))
				label.UserConfig.Remove("forId");
			else
				label.UserConfig ["forId"] = forId;
            
			return label;
		}
        
		public static string GetForId(this ILabel label)
		{
			if (label == null)
				throw new ArgumentNullException("label");
            
			if (label.UserConfig.ContainsKey("forId"))
				return (string)label.UserConfig ["forId"];
			else
				return null;
		}
        
        #endregion ForId

        #region Html
        
		public static ILabel SetHtml(this ILabel label, string html)
		{
			if (label == null)
				throw new ArgumentNullException("label");
            
			if (String.IsNullOrEmpty(html) && label.UserConfig.ContainsKey("html"))
				label.UserConfig.Remove("html");
			else
				label.UserConfig ["html"] = html;
            
			return label;
		}
        
		public static string GetHtml(this ILabel label)
		{
			if (label == null)
				throw new ArgumentNullException("label");
            
			if (label.UserConfig.ContainsKey("html"))
				return (string)label.UserConfig ["html"];
			else
				return null;
		}
        
        #endregion Html       

        #region Text
        
		public static ILabel SetText(this ILabel label, string text)
		{
			if (label == null)
				throw new ArgumentNullException("label");
            
			if (String.IsNullOrEmpty(text) && label.UserConfig.ContainsKey("text"))
				label.UserConfig.Remove("text");
			else
				label.UserConfig ["text"] = text;
            
			return label;
		}
        
		public static string GetText(this ILabel label)
		{
			if (label == null)
				throw new ArgumentNullException("label");
            
			if (label.UserConfig.ContainsKey("text"))
				return (string)label.UserConfig ["text"];
			else
				return null;
		}

        #endregion Text
	}
}
