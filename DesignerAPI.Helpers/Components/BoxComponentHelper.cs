//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


namespace DesignerAPI.Components
{
    using System;
    using DesignerAPI.Components;
    using DesignerAPI.Builder;

    ///<summary>
    /// Box component.
    ///</summary>
    public static class BoxComponentHelper
    {
        public static IButton AddButton (this IBoxComponent box)
        {

            if (box == null)
                throw new ArgumentNullException ("box");                      
            
            IButton button = new Button ();
            box.Components.Add (button);
            ProjectBuilderHelper.LatestComponent.Push (box);
            
            return button;
        }

        ///<summary>
        /// Sets the height of the auto.
        ///</summary>
        /// <returns>
        /// The auto height.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='autoHeight'>
        /// If set to <c>true</c> auto height.
        /// </param>
        public static IBoxComponent SetAutoHeight (this IBoxComponent box, bool autoHeight)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["autoHeight"] = autoHeight;
            return box;
        }

        ///<summary>
        /// Sets the auto scroll.
        ///</summary>
        /// <returns>
        /// The auto scroll.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='autoScroll'>
        /// If set to <c>true</c> auto scroll.
        /// </param>
        public static IBoxComponent SetAutoScroll (this IBoxComponent box, bool autoScroll)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["autoScroll"] = autoScroll;
            return box;
        }

        ///<summary>
        /// Sets the width of the auto.
        ///</summary>
        /// <returns>
        /// The auto width.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='autoWidth'>
        /// If set to <c>true</c> auto width.
        /// </param>
        public static IBoxComponent SetAutoWidth (this IBoxComponent box, bool autoWidth)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["autoWidth"] = autoWidth;
            return box;
        }

        public static TEntity SetAutoWidth<TEntity> (this IBoxComponent box, bool autoWidth)
        {
            if (box == null)
                throw new ArgumentNullException ("box");

            return (TEntity)box.SetAutoWidth (autoWidth);
        }

        ///<summary>
        /// Sets the height of the box max.
        ///</summary>
        /// <returns>
        /// The box max height.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='boxMaxHeight'>
        /// Box max height.
        /// </param>
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetBoxMaxHeight (this IBoxComponent box, uint boxMaxHeight)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["boxMaxHeight"] = boxMaxHeight;
            return box;
        }

        ///<summary>
        /// Sets the width of the box max.
        ///</summary>
        /// <returns>
        /// The box max width.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='boxMaxWidth'>
        /// Box max width.
        /// </param>
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetBoxMaxWidth (this IBoxComponent box, uint boxMaxWidth)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["boxMaxWidth"] = boxMaxWidth;
            return box;
        }

        ///<summary>
        /// Sets the height of the box minimum.
        ///</summary>
        /// <returns>
        /// The box minimum height.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='boxMinHeight'>
        /// Box minimum height.
        /// </param>
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetBoxMinHeight (this IBoxComponent box, uint boxMinHeight)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["boxMinHeight"] = boxMinHeight;
            return box;
        }

        ///<summary>
        /// Sets the width of the box minimum.
        ///</summary>
        /// <returns>
        /// The box minimum width.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='boxMinWidth'>
        /// Box minimum width.
        /// </param>
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetBoxMinWidth (this IBoxComponent box, uint boxMinWidth)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["boxMinWidth"] = boxMinWidth;
            return box;
        }

        ///<summary>
        /// Sets the height.
        ///</summary>
        /// <returns>
        /// The height.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='height'>
        /// Height.
        /// </param>
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetHeight (this IBoxComponent box, uint height)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["height"] = height;
            return box;
        }

        ///<summary>
        /// Sets the width.
        ///</summary>
        /// <returns>
        /// The width.
        /// </returns>
        /// <param name='box'>
        /// Box.
        /// </param>
        /// <param name='width'>
        /// Width.
        /// </param>
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetWidth (this IBoxComponent box, uint width)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            box.UserConfig ["width"] = width;
            return box;
        }

        #region Region

        public static IBoxComponent SetRegion (this IBoxComponent box, string region)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            if (String.IsNullOrEmpty (region) && box.UserConfig.ContainsKey ("region"))
                box.UserConfig.Remove ("region");
            else
                box.UserConfig ["region"] = region.ToLowerInvariant ();
            
            return box;
        }
        
        public static TEntity SetRegion<TEntity> (this IBoxComponent box, string region)
        {
            return (TEntity)box.SetRegion (region);
        }
        
        public static IBoxComponent SetRegion (this IBoxComponent box, BorderLayout region)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            string regionAux = Enum.GetName (typeof(BorderLayout), region).ToLowerInvariant ();
            box.UserConfig ["region"] = regionAux;
            
            return box;
        }
        
        public static TEntity SetRegion<TEntity> (this IBoxComponent box, BorderLayout region)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            return (TEntity)box.SetRegion (region);
        }
        
        public static BorderLayout GetRegion (this IBoxComponent box)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            if (box.UserConfig.ContainsKey ("region"))
                return (BorderLayout)Enum.Parse (typeof(BorderLayout), (string)box.UserConfig ["region"], true);
            else
                return BorderLayout.Center;
        }
        
        #endregion Region       

        #region X
        
        public static IBoxComponent SetX (this IBoxComponent box, int x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        public static TEntity SetX <TEntity> (this IBoxComponent box, int x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            return (TEntity)box.SetX (x);
        }
        
        public static IBoxComponent SetX (this IBoxComponent box, uint x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetX <TEntity> (this IBoxComponent box, uint x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        public static IBoxComponent SetX (this IBoxComponent box, long x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        public static TEntity SetX <TEntity> (this IBoxComponent box, long x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetX (this IBoxComponent box, ulong x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetX <TEntity> (this IBoxComponent box, ulong x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        public static IBoxComponent SetX (this IBoxComponent box, short x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        public static TEntity SetX <TEntity> (this IBoxComponent box, short x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetX (this IBoxComponent box, ushort x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetX <TEntity> (this IBoxComponent box, ushort x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        public static IBoxComponent SetX (this IBoxComponent box, decimal x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        public static TEntity SetX <TEntity> (this IBoxComponent box, decimal x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        public static IBoxComponent SetX (this IBoxComponent box, float x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        public static TEntity SetX <TEntity> (this IBoxComponent box, float x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        public static IBoxComponent SetX (this IBoxComponent box, double x)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["x"] = x;
            
            return box;
        }
        
        public static TEntity SetX <TEntity> (this IBoxComponent box, double x)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetX (x);
        }
        
        public static Variant GetX (this IBoxComponent box)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            if (box.UserConfig.ContainsKey ("x"))
                return new Variant (box.UserConfig ["x"]);
            else
                return null;
        }
        
        #endregion X

        #region Y
        
        public static IBoxComponent SetY (this IBoxComponent box, int y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        public static TEntity SetY <TEntity> (this IBoxComponent box, int y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            return (TEntity)box.SetY (y);
        }
        
        public static IBoxComponent SetY (this IBoxComponent box, uint y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetY <TEntity> (this IBoxComponent box, uint y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        public static IBoxComponent SetY (this IBoxComponent box, long y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        public static TEntity SetY <TEntity> (this IBoxComponent box, long y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetY (this IBoxComponent box, ulong y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetY <TEntity> (this IBoxComponent box, ulong y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        public static IBoxComponent SetY (this IBoxComponent box, short y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        public static TEntity SetY <TEntity> (this IBoxComponent box, short y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        [CLSCompliantAttribute(false)]
        public static IBoxComponent SetY (this IBoxComponent box, ushort y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        [CLSCompliantAttribute(false)]
        public static TEntity SetY <TEntity> (this IBoxComponent box, ushort y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        public static IBoxComponent SetY (this IBoxComponent box, decimal y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        public static TEntity SetY <TEntity> (this IBoxComponent box, decimal y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        public static IBoxComponent SetY (this IBoxComponent box, float y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        public static TEntity SetY <TEntity> (this IBoxComponent box, float y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        public static IBoxComponent SetY (this IBoxComponent box, double y)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            box.UserConfig ["y"] = y;
            
            return box;
        }
        
        public static TEntity SetY <TEntity> (this IBoxComponent box, double y)
            where TEntity : IBoxComponent
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            return (TEntity)box.SetY (y);
        }
        
        public static Variant GetY (this IBoxComponent box)
        {
            if (box == null)
                throw new ArgumentNullException ("box");
            
            if (box.UserConfig.ContainsKey ("y"))
                return new Variant (box.UserConfig ["y"]);
            else
                return null;
        }
        
        #endregion Y
    }
}