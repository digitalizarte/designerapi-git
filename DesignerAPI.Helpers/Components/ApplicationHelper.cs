//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{

	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using System.Collections.Generic;


	///<summary>
	/// Application.
	///</summary>

	public static class ApplicationHelper
	{
		// TODO: AddFixedFunction.
		// TODO: AddLoader.
		// TODO: SetName.
		// TODO: GetName.

		public static IApplication SetAutoCreateViewport(this IApplication application, bool autoCreateViewport)
		{
			if (application == null)
				throw new ArgumentNullException("application");
			application.UserConfig ["autoCreateViewport"] = autoCreateViewport;
			return application;
		}

		public static bool GetAutoCreateViewport(this IApplication application)
		{
			if (application == null)
				throw new ArgumentNullException("application");
			if (application.UserConfig.ContainsKey("autoCreateViewport"))
				return (bool)application.UserConfig ["autoCreateViewport"];
			else
				return true;
		}

		public static IApplication AddModel(this IApplication application, string model)
		{
			if (application == null)
				throw new ArgumentNullException("application");

			if (model == null)
				throw new ArgumentNullException("model");

			if (!application.UserConfig.ContainsKey("models"))
			{
				application.UserConfig ["models"] = new List<string>();
			}

			(application.UserConfig ["models"] as List<string>).Add(model);

			return application;
		}

		public static IApplication AddView(this IApplication application, string view)
		{
			if (application == null)
				throw new ArgumentNullException("application");
			
			if (view == null)
				throw new ArgumentNullException("view");
			
			if (!application.UserConfig.ContainsKey("views"))
			{
				application.UserConfig ["views"] = new List<string>();
			}
			
			(application.UserConfig ["views"] as List<string>).Add(view);
			
			return application;
		}

		public static IApplication AddController(this IApplication application, string controller)
		{
			if (application == null)
				throw new ArgumentNullException("application");
			
			if (controller == null)
				throw new ArgumentNullException("controller");
			
			if (!application.UserConfig.ContainsKey("controllers"))
			{
				application.UserConfig ["controllers"] = new List<string>();
			}
			
			(application.UserConfig ["controllers"] as List<string>).Add(controller);
			
			return application;
		}
		
		public static IApplication AddStore(this IApplication application, string store)
		{
			if (application == null)
				throw new ArgumentNullException("application");
			
			if (store == null)
				throw new ArgumentNullException("model");
			
			if (!application.UserConfig.ContainsKey("stores"))
			{
				application.UserConfig ["stores"] = new List<string>();
			}
			
			(application.UserConfig ["stores"] as List<string>).Add(store);
			
			return application;
		}
	}	
}
