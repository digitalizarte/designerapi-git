//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components.Slider
{
    using System;
	using DesignerAPI.Model;
	using System.Collections.Generic;

	public static class SliderHelper
	{
        #region Animate
        
		public static ISlider SetAnimate(this ISlider slider, bool animate)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["animate"] = animate;
            
			return slider;
		}
        
		public static bool GetAnimate(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("animate"))
				return (bool)slider.UserConfig ["animate"];
			else
				return false;
		}
        
        #endregion Animate

        #region ClickToChange
        
		public static ISlider SetClickToChange(this ISlider slider, bool clickToChange)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["clickToChange"] = clickToChange;
            
			return slider;
		}
        
		public static bool GetClickToChange(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("clickToChange"))
				return (bool)slider.UserConfig ["clickToChange"];
			else
				return false;
		}
        
        #endregion ClickToChange

        #region ConstrainThumbs
        
		public static ISlider SetConstrainThumbs(this ISlider slider, bool constrainThumbs)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["constrainThumbs"] = constrainThumbs;
            
			return slider;
		}
        
		public static bool GetConstrainThumbs(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("constrainThumbs"))
				return (bool)slider.UserConfig ["constrainThumbs"];
			else
				return false;
		}
        
        #endregion ConstrainThumbs

        #region DecimalPrecision
        
		public static ISlider SetDecimalPrecision(this ISlider slider, int decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetDecimalPrecision(this ISlider slider, uint decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}
        
		public static ISlider SetDecimalPrecision(this ISlider slider, long decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetDecimalPrecision(this ISlider slider, ulong decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}
        
		public static ISlider SetDecimalPrecision(this ISlider slider, short decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetDecimalPrecision(this ISlider slider, ushort decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}
        
		public static ISlider SetDecimalPrecision(this ISlider slider, decimal decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}
        
		public static ISlider SetDecimalPrecision(this ISlider slider, float decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}
        
		public static ISlider SetDecimalPrecision(this ISlider slider, double decimalPrecision)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["decimalPrecision "] = decimalPrecision;
            
			return slider;
		}
        
		public static Variant GetDecimalPrecision(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("decimalPrecision "))
				return new Variant(slider.UserConfig ["decimalPrecision "]);
			else
				return null;
		}
        
        #endregion DecimalPrecision

        #region Increment
        
		public static ISlider SetIncrement(this ISlider slider, int increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetIncrement(this ISlider slider, uint increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}
        
		public static ISlider SetIncrement(this ISlider slider, long increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetIncrement(this ISlider slider, ulong increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}
        
		public static ISlider SetIncrement(this ISlider slider, short increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetIncrement(this ISlider slider, ushort increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}
        
		public static ISlider SetIncrement(this ISlider slider, decimal increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}
        
		public static ISlider SetIncrement(this ISlider slider, float increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}
        
		public static ISlider SetIncrement(this ISlider slider, double increment)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["increment "] = increment;
            
			return slider;
		}
        
		public static Variant GetIncrement(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("increment "))
				return new Variant(slider.UserConfig ["increment "]);
			else
				return null;
		}
        
        #endregion Increment

        #region KeyIncrement
        
		public static ISlider SetKeyIncrement(this ISlider slider, int keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetKeyIncrement(this ISlider slider, uint keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}
        
		public static ISlider SetKeyIncrement(this ISlider slider, long keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetKeyIncrement(this ISlider slider, ulong keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}
        
		public static ISlider SetKeyIncrement(this ISlider slider, short keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetKeyIncrement(this ISlider slider, ushort keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}
        
		public static ISlider SetKeyIncrement(this ISlider slider, decimal keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}
        
		public static ISlider SetKeyIncrement(this ISlider slider, float keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}
        
		public static ISlider SetKeyIncrement(this ISlider slider, double keyKeyIncrement)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["keyKeyIncrement "] = keyKeyIncrement;
            
			return slider;
		}
        
		public static Variant GetKeyIncrement(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("keyKeyIncrement "))
				return new Variant(slider.UserConfig ["keyKeyIncrement "]);
			else
				return null;
		}
        
        #endregion KeyIncrement

        #region MaxValue
        
		public static ISlider SetMaxValue(this ISlider slider, int maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetMaxValue(this ISlider slider, uint maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}
        
		public static ISlider SetMaxValue(this ISlider slider, long maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetMaxValue(this ISlider slider, ulong maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}
        
		public static ISlider SetMaxValue(this ISlider slider, short maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetMaxValue(this ISlider slider, ushort maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}
        
		public static ISlider SetMaxValue(this ISlider slider, decimal maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}
        
		public static ISlider SetMaxValue(this ISlider slider, float maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}
        
		public static ISlider SetMaxValue(this ISlider slider, double maxValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["maxValue "] = maxValue;
            
			return slider;
		}
        
		public static Variant GetMaxValue(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("maxValue "))
				return new Variant(slider.UserConfig ["maxValue "]);
			else
				return null;
		}
        
        #endregion MaxValue

        #region MinValue
        
		public static ISlider SetMinValue(this ISlider slider, int minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetMinValue(this ISlider slider, uint minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}
        
		public static ISlider SetMinValue(this ISlider slider, long minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetMinValue(this ISlider slider, ulong minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}
        
		public static ISlider SetMinValue(this ISlider slider, short minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetMinValue(this ISlider slider, ushort minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}
        
		public static ISlider SetMinValue(this ISlider slider, decimal minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}
        
		public static ISlider SetMinValue(this ISlider slider, float minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}
        
		public static ISlider SetMinValue(this ISlider slider, double minValue)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["minValue "] = minValue;
            
			return slider;
		}
        
		public static Variant GetMinValue(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("minValue "))
				return new Variant(slider.UserConfig ["minValue "]);
			else
				return null;
		}
        
        #endregion MinValue

        #region Value
        
		public static ISlider SetValue(this ISlider slider, int value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetValue(this ISlider slider, uint value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}
        
		public static ISlider SetValue(this ISlider slider, long value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetValue(this ISlider slider, ulong value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}
        
		public static ISlider SetValue(this ISlider slider, short value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}

        [CLSCompliantAttribute(false)]
		public static ISlider SetValue(this ISlider slider, ushort value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}
        
		public static ISlider SetValue(this ISlider slider, decimal value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}
        
		public static ISlider SetValue(this ISlider slider, float value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}
        
		public static ISlider SetValue(this ISlider slider, double value)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["value "] = value;
            
			return slider;
		}
        
		public static Variant GetValue(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("value "))
				return new Variant(slider.UserConfig ["value "]);
			else
				return null;
		}
        
        #endregion Value

        #region Vertical
        
		public static ISlider SetVertical(this ISlider slider, bool vertical)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			slider.UserConfig ["vertical"] = vertical;
            
			return slider;
		}
        
		public static bool GetVertical(this ISlider slider)
		{
			if (slider == null)
				throw new ArgumentNullException("slider");
            
			if (slider.UserConfig.ContainsKey("vertical"))
				return (bool)slider.UserConfig ["vertical"];
			else
				return false;
		}
        
        #endregion Vertical

	}           
}