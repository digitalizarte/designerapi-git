//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;

	///<summary>
	/// IComponent.
	///</summary>
	public static class ComponentHelper
	{       
		public static IComponent SetAutoEl(this IComponent component, string autoEl)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(autoEl) && component.UserConfig.ContainsKey("autoEl"))
				component.UserConfig.Remove("autoEl");
			else
				component.UserConfig ["autoEl"] = autoEl;
			return component;
		}
        
		public static string GetAutoEl(this IComponent component)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (component.UserConfig.ContainsKey("autoEl"))
				return (string)component.UserConfig ["autoEl"];
			else
				return null;
		}
        
		public static IComponent SetAutoShow(this IComponent component, bool autoShow)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["autoShow"] = autoShow;
			return component;
		}
        
		public static bool GetAutoShow(this IComponent component)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (component.UserConfig.ContainsKey("autoShow"))
				return (bool)component.UserConfig ["autoShow"];
			else
				return true;
		}
        
		public static IComponent SetBubleEvents(this IComponent component, object[] bubleEvents)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["bubleEvents"] = bubleEvents;
			return component;
		}
        
		public static object[] GetBubleEvents(this IComponent component)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (component.UserConfig.ContainsKey("bubleEvents"))
				return (object[])component.UserConfig ["bubleEvents"];
			else
				return null;
		}
        
		public static IComponent SetCls(this IComponent component, string cls)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(cls) && component.UserConfig.ContainsKey("cls"))
				component.UserConfig.Remove("cls");
			else
				component.UserConfig ["cls"] = cls;
			return component;
		}
        
		public static IComponent SetCtCls(this IComponent component, string ctCls)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(ctCls) && component.UserConfig.ContainsKey("ctCls"))
				component.UserConfig.Remove("ctCls");
			else
				component.UserConfig ["ctCls"] = ctCls;
			return component;
		}
        
		public static IComponent SetData(this IComponent component, object data)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["data"] = data;
			return component;
		}
        
		public static IComponent SetDisabled(this IComponent component, bool disabled)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["disabled"] = disabled;
			return component;
		}
        
		public static IComponent SetDisabledClass(this IComponent component, string disabledClass)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(disabledClass) && component.UserConfig.ContainsKey("disabledClass"))
				component.UserConfig.Remove("disabledClass");
			else
				component.UserConfig ["disabledClass"] = disabledClass;
			return component;
		}
        
		public static IComponent SetHidden(this IComponent component, bool hidden)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["hidden"] = hidden;
			return component;
		}
        
		public static IComponent SetHideMode(this IComponent component, HideMode hideMode)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			string value = Enum.GetName(typeof(HideMode), hideMode).ToLowerInvariant();
			component.UserConfig ["hideMode"] = value;
			return component;
		}
        
		public static IComponent SetHideParent(this IComponent component, bool hideParent)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["hideParent"] = hideParent;
			return component;
		}
        
		public static IComponent SetHtml(this IComponent component, string html)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(html) && component.UserConfig.ContainsKey("html"))
				component.UserConfig.Remove("html");
			else
				component.UserConfig ["html"] = html;
			return component;
		}
        
		public static IComponent SetId(this IComponent component, string id)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(id) && component.UserConfig.ContainsKey("id"))
				component.UserConfig.Remove("id");
			else
				component.UserConfig ["id"] = id;
			return component;
		}
        
		public static IComponent SetItemId(this IComponent component, string itemId)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(itemId) && component.UserConfig.ContainsKey("itemId"))
				component.UserConfig.Remove("itemId");
			else
				component.UserConfig ["itemId"] = itemId;
			return component;
		}
        
        public static TEntity SetItemId<TEntity>(this IComponent component, string itemId)
        {                        
            return (TEntity)component.SetItemId(itemId);
        }

        
		public static IComponent SetOverCls(this IComponent component, string overCls)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(overCls) && component.UserConfig.ContainsKey("overCls"))
				component.UserConfig.Remove("overCls");
			else
				component.UserConfig ["overCls"] = overCls;
			return component;
		}
        
		public static IComponent SetStateEvents(this IComponent component, object[] stateEvents)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["stateEvents"] = stateEvents;
			return component;
		}
        
		///<summary>
		/// Sets the stateful.
		///</summary>
		/// <returns>
		/// The stateful.
		/// </returns>
		/// <param name='stateful'>
		/// If set to <c>true</c> stateful.
		/// </param>
		public static IComponent SetStateful(this IComponent component, bool stateful)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["stateful"] = stateful;
			return component;
		}
        
		///<summary>
		/// Sets the state identifier.
		///</summary>
		/// <returns>
		/// The state identifier.
		/// </returns>
		/// <param name='stateId'>
		/// State identifier.
		/// </param>
		public static IComponent SetStateId(this IComponent component, string stateId)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(stateId) && component.UserConfig.ContainsKey("stateId"))
				component.UserConfig.Remove("stateId");
			else
				component.UserConfig ["stateId"] = stateId;
			return component;
		}
        
		///<summary>
		/// Sets the style.
		///</summary>
		/// <returns>
		/// The style.
		/// </returns>
		/// <param name='style'>
		/// Style.
		/// </param>
		public static IComponent SetStyle(this IComponent component, object style)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			component.UserConfig ["style"] = style;
			return component;
		}      
        
		///<summary>
		/// Sets the tpl.
		///</summary>
		/// <returns>
		/// The tpl.
		/// </returns>
		/// <param name='tpl'>
		/// Tpl.
		/// </param>
		public static IComponent SetTpl(this IComponent component, string tpl)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(tpl) && component.UserConfig.ContainsKey("tpl"))
				component.UserConfig.Remove("tpl");
			else
				component.UserConfig ["tpl"] = tpl;
			return component;
		}
        
		///<summary>
		/// Sets the tpl write mode.
		///</summary>
		/// <returns>
		/// The tpl write mode.
		/// </returns>
		/// <param name='tplWriteMode'>
		/// Tpl write mode.
		/// </param>
		public static IComponent SetTplWriteMode(this IComponent component, string tplWriteMode)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(tplWriteMode) && component.UserConfig.ContainsKey("tplWriteMode"))
				component.UserConfig.Remove("tplWriteMode");
			else
				component.UserConfig ["tplWriteMode"] = tplWriteMode;
			return component;
		}

        #region UserXType
		public static IComponent SetUserXType(this IComponent component, string userXType)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(userXType) && component.UserConfig.ContainsKey("designer|userAlias"))
				component.UserConfig.Remove("designer|userAlias");
			else
				component.UserConfig ["designer|userAlias"] = userXType;

			return component;
		}

		public static string GetUserXType(this IComponent component)
		{
			if (component == null)
				throw new ArgumentNullException("component");

			if (component.UserConfig.ContainsKey("designer|userAlias"))
				return (string)component.UserConfig ["designer|userAlias"];
			else
				return null;
		}

        #endregion UserXType

        #region JsClass
		public static IComponent SetJsClass(this IComponent component, string jsClass)
		{
			if (component == null)
				throw new ArgumentNullException("component");
			if (String.IsNullOrEmpty(jsClass) && component.UserConfig.ContainsKey("designer|userClassName"))
				component.UserConfig.Remove("designer|userClassName");
			else
				component.UserConfig ["designer|userClassName"] = jsClass;
            
			return component;
		}

        public static TEntity SetJsClass<TEntity>(this IComponent component, string jsClass) {
            return (TEntity)component.SetJsClass(jsClass);
        }
        
		public static string GetJsClass(this IComponent component)
		{
			if (component == null)
				throw new ArgumentNullException("component");
            
			if (component.UserConfig.ContainsKey("designer|userClassName"))
				return (string)component.UserConfig ["designer|userClassName"];
			else
				return null;
		}
        
        #endregion JsClass

        ///<summary>
        /// Sets the field label.
        ///</summary>
        /// <returns>The field label.</returns>
        /// <param name="component">Component.</param>
        /// <param name="fieldLabel">Field label.</param>
        public static IComponent SetFieldLabel(this IComponent component, string fieldLabel)
        {
            if (component == null)
                throw new ArgumentNullException("component");
            if (String.IsNullOrEmpty(fieldLabel) && component.UserConfig.ContainsKey("fieldLabel"))
                component.UserConfig.Remove("fieldLabel");
            else
                component.UserConfig ["fieldLabel"] = fieldLabel;
            return component;
        }
        
        ///<summary>
        /// Sets the field label.
        ///</summary>
        /// <returns>The field label.</returns>
        /// <param name="component">Component.</param>
        /// <param name="fieldLabel">Field label.</param>
        /// <typeparam name="TEntity">The 1st type parameter.</typeparam>
        public static TEntity SetFieldLabel<TEntity>(this IComponent component, string fieldLabel){
            return (TEntity)component.SetFieldLabel(fieldLabel);
        }
	}
}