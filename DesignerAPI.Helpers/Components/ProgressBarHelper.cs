//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
    
	using System.Collections.Generic;

	public static class ProgressBarHelper
	{
        #region Animate
        
		public static IProgressBar SetAnimate(this IProgressBar progressBar, bool animate)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			progressBar.UserConfig ["animate"] = animate;
            
			return progressBar;
		}
        
		public static bool GetAnimate(this IProgressBar progressBar)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			if (progressBar.UserConfig.ContainsKey("animate"))
				return (bool)progressBar.UserConfig ["animate"];
			else
				return false;
		}
        
        #endregion Animate

        #region BaseCls
        
		public static IProgressBar SetBaseCls(this IProgressBar progressBar, string baseCls)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			if (String.IsNullOrEmpty(baseCls) && progressBar.UserConfig.ContainsKey("baseCls"))
				progressBar.UserConfig.Remove("baseCls");
			else
				progressBar.UserConfig ["baseCls"] = baseCls;
            
			return progressBar;
		}
        
		public static string GetBaseCls(this IProgressBar progressBar)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			if (progressBar.UserConfig.ContainsKey("baseCls"))
				return (string)progressBar.UserConfig ["baseCls"];
			else
				return null;
		}
        
        #endregion BaseCls       

        #region Text
        
		public static IProgressBar SetText(this IProgressBar progressBar, string text)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			if (String.IsNullOrEmpty(text) && progressBar.UserConfig.ContainsKey("text"))
				progressBar.UserConfig.Remove("text");
			else
				progressBar.UserConfig ["text"] = text;
            
			return progressBar;
		}
        
		public static string GetText(this IProgressBar progressBar)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			if (progressBar.UserConfig.ContainsKey("text"))
				return (string)progressBar.UserConfig ["text"];
			else
				return null;
		}
        
        #endregion Text      

        #region Value
        
		public static IProgressBar SetValue(this IProgressBar progressBar, int value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value < 0 || value > 1)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}

        [CLSCompliantAttribute(false)]
		public static IProgressBar SetValue(this IProgressBar progressBar, uint value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value > 1U)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}
        
		public static IProgressBar SetValue(this IProgressBar progressBar, long value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value < 0L || value > 1L)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}

        [CLSCompliantAttribute(false)]
		public static IProgressBar SetValue(this IProgressBar progressBar, ulong value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value > 1UL)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}
        
		public static IProgressBar SetValue(this IProgressBar progressBar, short value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value < 0 || value > 1)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}

        [CLSCompliantAttribute(false)]
		public static IProgressBar SetValue(this IProgressBar progressBar, ushort value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value > 1)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}
        
		public static IProgressBar SetValue(this IProgressBar progressBar, decimal value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value < 0M || value > 1M)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}
        
		public static IProgressBar SetValue(this IProgressBar progressBar, float value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value < 0F || value > 1F)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}
        
		public static IProgressBar SetValue(this IProgressBar progressBar, double value)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");

			if (value < 0D || value > 1D)
				throw new ArgumentOutOfRangeException("progressBar");
            
			progressBar.UserConfig ["value "] = value;
            
			return progressBar;
		}
        
		public static Variant GetValue(this IProgressBar progressBar)
		{
			if (progressBar == null)
				throw new ArgumentNullException("progressBar");
            
			if (progressBar.UserConfig.ContainsKey("value "))
				return new Variant(progressBar.UserConfig ["value "]);
			else
				return null;
		}
        
        #endregion Value
	}    
}
