//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DesignerAPI.Model;
    
    public static class ProjectHelper
    {
        private static readonly object syncLock = new object ();
        [ThreadStatic]
        private static Dictionary<string, long>
            ids;
        [ThreadStatic]
        private static Dictionary<string, IList<Component>>
            usercomponent;
        
        private static Dictionary<string, long> Ids {
            get {
                
                lock (syncLock) {
                    if (ids == null) {
                        ids = new Dictionary<string, long> ();
                    }
                    return ids;
                }
            }            
        }
        
        private static Dictionary<string, IList<Component>> Usercomponent {
            get {
                
                lock (syncLock) {
                    if (usercomponent == null) {
                        usercomponent = new Dictionary<string, IList<Component>> ();
                    }
                    return usercomponent;
                }
            }   
        }
        
        private static void Init ()
        {
            lock (syncLock) {
                ids = null;
                usercomponent = null;              
            }                       
        }
        
        public static void Process (this IProject project)
        {
            if (project == null)
                throw new ArgumentNullException ("project");        

            Init ();
            GetUserComponents (project.Components);
            CleanUpProject (project.Components);
            if (project.Components != null) {
                project.Components = new EnumerableId<Component> (project.Components.OrderBy<Component, string> (c => c.JsClass ?? "").ToList<Component> ());                
            }
        }

        private static string GetComponentNewId (string cid)
        {
            string key = (cid ?? "nocid") .ToLower ();
            if (!Ids.ContainsKey (key)) {
                Ids [key] = 0;
            }
            return String.Format ("{0}{1:0000}", key, ++Ids [key]);
        }
        
        ///<summary>
        /// 
        ///</summary>
        /// <param name="comp"></param>        
        private static void GetUserComponents (EnumerableId<Component> components)
        {
            if (components != null) {
                foreach (Component component in components) {
                    if (!String.IsNullOrEmpty (component.cId) && component.cId.Equals ("usercomponent") && !String.IsNullOrEmpty (component.UserComponent)) {
                        if (!Usercomponent.ContainsKey (component.UserComponent)) {
                            Usercomponent [component.UserComponent] = new List<Component> ();
                        }
                        Usercomponent [component.UserComponent].Add (component);
                    }
                    GetUserComponents (component.Components);
                }
            }
        }
        
        private static void CleanUpProject (EnumerableId<Component> components)
        {
            if (components != null) {
                foreach (IComponent component in components) {
                    string oldId = component.Id;
                    component.Id = GetComponentNewId (component.Id);
                    if (Usercomponent .ContainsKey (oldId)) {
                        foreach (IComponent c in Usercomponent  [oldId]) {
                            c.UserComponent = component.Id;
                        }                        
                    }
                    if (component.JsClass != null && component.JsClass.StartsWith ("My", StringComparison.InvariantCultureIgnoreCase)) {
                        component.JsClass = component.Id;
                    }
                    CleanUpUserConfig (component);
                    CleanUpProject (component.Components);                    
                }
            }
        }
        
        private static void CleanUpUserConfig (IComponent component)
        {
            if (component == null)
                throw new ArgumentNullException ("component");
            
            if (component != null && component.UserConfig != null) {
                RemoveExtUndefined (component.UserConfig);
            }
        }
        
        private static void RemoveExtUndefined (IDictionary<string, object> config)
        {
            if (config == null)
                throw new ArgumentNullException ("config");

            List<string> keys = new List<string> ();
            foreach (KeyValuePair<string, object> kp in config) {
                if (kp.Value.Equals ("-ext-undefined-")) {
                    keys.Add (kp.Key);
                }
            }

            // keys.ForEach (k => config.Remove (k));
            foreach (string key in keys) {
                config.Remove(key);
            }
        }
    }    
}