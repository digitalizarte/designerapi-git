//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace System
{
	using System;
	using System.Reflection;

	///<summary>
	/// A strongly typed object that readily casts an intrinsic
	/// object to the other intrinsic types when possible.
	///</summary>
	/// <remarks>
	/// <para>The Variant class is an intrinsic object container structure
	/// inspired by Visual Basic 6.0's Variant. The key features
	/// of a Variant class include the ability to perform typecasts and
	/// arithmetic between types that are not normally considered compatible.
	/// For example, if a Variant class contains a string describing a
	/// number, such as "1.1", then floating point arithmetic can be
	/// performed on it.</para>
	/// <para>Variants are normally considered dangerous because they
	/// strip away the effectiveness of type safety, which is the
	/// reason why the Visual Basic 6.0 Variant was left out of
	/// Visual Basic .NET. However, this implementation restores the
	/// Variant structure, both as a proof of concept and as a
	/// restoration of the utility and positive merits of the Variant
	/// where it can be used responsibly.</para>
	/// </remarks>
	public struct Variant : IConvertible
	{
		#region Fields
		private object _value;

		private static char decimalSep = ((float)1.1).ToString().ToCharArray() [1];

		#endregion 

		#region Ctx 
		///<summary>
		/// Creates a strongly typed object that readily casts a primitive
		/// object to the other primitive types when possible.
		///</summary>
		public Variant(object value)
		{
			if (value.GetType() == typeof(Variant))
				_value = ((Variant)value).Value;
			else
				_value = value;
		}
		
		public Variant(string value) : this( (object)value)
		{
			
		}
		
        [CLSCompliantAttribute(false)]
		public Variant(ushort value) : this( (object)value)
		{
			
		}
		
		public Variant(short value) : this( (object)value)
		{
			
		}
		
        [CLSCompliantAttribute(false)]
		public Variant(uint value) : this( (object)value)
		{
			
		}
		
		public Variant(int value) : this( (object)value)
		{
			
		}
		
        [CLSCompliantAttribute(false)]
		public Variant(ulong value) : this( (object)value)
		{
			
		}
		
		public Variant(long value) : this( (object)value)
		{
			
		}
		
		public Variant(float value) : this( (object)value)
		{
			
		}
		
		public Variant(double value) : this( (object)value)
		{
			
		}
		
		public Variant(decimal value) : this( (object)value)
		{
			
		}
		
		public Variant(DateTime value) : this( (object)value)
		{
			
		}

		public Variant(TimeSpan value) : this( (object)value)
		{
			
		}

		#endregion		

		#region Properties 
		///<summary>
		/// The actual value being stored in its original <see cref="System.Type"/>,
		/// returned as an <see cref="Object"/>.
		///</summary>
		public object Value
		{
			get
			{
				return _value;
			}
		}
		///<summary>
		/// The <see cref="System.Type"/> of the <see cref="Value"/> property.
		///</summary>
		public Type Type
		{
			get
			{
				return _value.GetType();
			}
		}

		///<summary>
		/// Returns the string equivalent of the <see cref="Value"/> property.
		///</summary>
		private string String
		{
			get
			{
				return _value.ToString();
			}
		}

		///<summary>
		/// Returns true if the <see cref="Value"/> property implements <see cref="IConvertible"/>
		///</summary>
		public bool ImplementsIConvertible
		{
			get
			{
				return Type.IsSubclassOf(typeof(IConvertible));
			}
		}
		///<summary>
		/// Returns true if the <see cref="Value"/> property
		/// is a numeric intrinsic value.
		///</summary>
		public bool IsNumeric
		{
			get
			{
				Type tt = Type;
				return (tt == typeof(byte) ||
					tt == typeof(sbyte) ||
					tt == typeof(short) ||
					tt == typeof(int) ||
					tt == typeof(long) ||
					tt == typeof(ushort) ||
					tt == typeof(uint) ||
					tt == typeof(ulong) ||
					tt == typeof(float) ||
					tt == typeof(double) ||
					tt == typeof(decimal));
			}
		}
		
		///<summary>
		/// Returns true if the <see cref="Value"/> property
		/// is a numeric intrinsic value or else can be parsed into
		/// a numeric intrinsic value.
		///</summary>
		public bool IsNumberable
		{
			get
			{
				if (IsNumeric)
					return true;
				Type tt = Type;
				if (tt == typeof(bool))
					return true;
				if (tt == typeof(string) ||
					tt == typeof(char))
				{
					try
					{
						foreach (char c in ToString().ToCharArray())
						{
							if (!char.IsDigit(c) && c != decimalSep)
								return false;
						}
						//double d = (ToDouble() + (double)0.1);
						return true;
					} catch
					{
						return false;
					}
				}
				return false;
			}
		}

		///<summary>
		/// Returns true if the value is a date or can be parsed into a date.
		///</summary>
		public bool IsDate
		{
			get
			{
				Type tt = Type;
				if (tt == typeof(DateTime))
					return true;
				if (tt == typeof(string))
				{
					try
					{
						if (ToString().Length < 6)
							return false;
						DateTime.Parse(ToString());
						return true;
					} catch
					{
					}
				}
				return false;
			}
		}

		///<summary>
		/// Returns true if the value is a TimeSpan.
		///</summary>
		public bool IsTimeSpan
		{
			get
			{
				return Type == typeof(TimeSpan);
			}
		}

		#endregion Properties 

		#region Methods 

		///<summary>
		/// Returns the <see cref="System.TypeCode"/> for this instance.
		///</summary>
		/// <returns>The enumerated constant that is the <see cref="System.TypeCode"/>
		/// of the class or value type that implements this interface.</returns>
		public TypeCode GetTypeCode()
		{
			return System.Type.GetTypeCode(Type);
		}

		///<summary>
		/// Attempts to convert or typecast to the specified type.
		///</summary>
		/// <param name="type">The type to convert or cast to.</param>
		/// <returns>The object after typecasting.</returns>
		public object ToType(Type type)
		{
			return ToType(type, false, null);
		}
		///<summary>
		/// Attempts to convert or typecast to the specified type.
		///</summary>
		/// <param name="type">The type to convert or cast to.</param>
		/// <param name="provider">An <see cref="IFormatProvider"/>
		/// interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>The object after typecasting.</returns>
		public object ToType(Type type, IFormatProvider provider)
		{
			return ToType(type, false, provider);
		}
		private object ToType(Type type, bool nomap)
		{
			return ToType(type, nomap, null);
		}

		private object ToType(Type type, bool nomap, IFormatProvider formatProvider)
		{
			if (type == Type)
				return _value;
			if (!nomap && formatProvider == null)
			{
				return TypeMap(type);
			}
			try
			{
				if (formatProvider != null)
				{
					return Convert.ChangeType(_value, type, formatProvider);
				} else
				{
					return Convert.ChangeType(_value, type);
				}
			} catch
			{
			}
			MethodInfo[] mis = Type.GetMethods();
			var searchMethod = "to" + type.Name.ToLower();
			foreach (MethodInfo mi in mis)
			{
				if (mi.Name.ToLower() == searchMethod)
				{
					try
					{
						if (formatProvider != null && mi.GetParameters().Length == 1)
						{
							if (mi.GetParameters() [0].ParameterType.IsSubclassOf(typeof(IFormatProvider)))
								return mi.Invoke(_value, new object[] {formatProvider});
						}
						return mi.Invoke(_value, new object[] {});
					} catch
					{
					}
				}
			}
			if (!nomap)
				return TypeMap(type);
			throw new InvalidCastException("Cannot determine conversion method.");
		}

		private object TypeMap(Type type)
		{
			Type tt = type;
			if (tt == typeof(bool))
				return ToBoolean();
			if (tt == typeof(string))
				return ToString();
			if (tt == typeof(char))
				return ToChar();
			if (tt == typeof(byte))
				return ToByte();
			if (tt == typeof(short))
				return ToInt16();
			if (tt == typeof(int))
				return ToInt32();
			if (tt == typeof(long))
				return ToInt64();
			if (tt == typeof(sbyte))
				return ToSByte();
			if (tt == typeof(ushort))
				return ToUInt16();
			if (tt == typeof(uint))
				return ToUInt32();
			if (tt == typeof(ulong))
				return ToUInt64();
			if (tt == typeof(float))
				return ToSingle();
			if (tt == typeof(double))
				return ToDouble();
			if (tt == typeof(decimal))
				return ToDecimal();
			if (tt == typeof(DateTime))
				return ToDateTime();
			if (tt == typeof(TimeSpan))
				return ToTimeSpan();
			return ToType(type, true);
		}

		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Boolean"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent <see cref="System.Boolean"/> value.
		///</summary>
		public bool ToBoolean()
		{
			if (Type == typeof(bool))
				return (bool)_value;
			return CBln(_value);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Boolean"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent <see cref="System.Boolean"/> value using the specified culture-specific
		/// formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public bool ToBoolean(IFormatProvider formatProvider)
		{
			return (bool)ToType(typeof(bool), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Byte"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 8-bit unsigned integer.
		///</summary>
		public byte ToByte()
		{
			Type tt = Type;
			if (tt == typeof(byte))
				return (byte)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return byte.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (Byte)1;
			else
				return (Byte)0;
			if (tt == typeof(DateTime))
				return (byte)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (byte)((TimeSpan)_value).Ticks;
			return (byte)ToType(typeof(byte), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Byte"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 8-bit unsigned integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public byte ToByte(IFormatProvider formatProvider)
		{
			return (byte)ToType(typeof(byte), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Int16"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 16-bit signed integer.
		///</summary>
		public short ToInt16()
		{
			Type tt = Type;
			if (tt == typeof(short))
				return (short)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return short.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (short)1;
			else
				return (short)0;
			if (tt == typeof(DateTime))
				return (short)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (short)((TimeSpan)_value).Ticks;
			return (short)ToType(typeof(short), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Boolean"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent 16-bit signed integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public short ToInt16(IFormatProvider formatProvider)
		{
			return (short)ToType(typeof(short), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Int32"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 32-bit signed integer.
		///</summary>
		public int ToInt32()
		{
			Type tt = Type;
			if (tt == typeof(int))
				return (int)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return int.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (int)1;
			else
				return (int)0;
			if (tt == typeof(DateTime))
				return (int)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (int)((TimeSpan)_value).Ticks;
			return (int)ToType(typeof(int), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Int32"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 32-bit signed integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public int ToInt32(IFormatProvider formatProvider)
		{
			return (int)ToType(typeof(int), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Int64"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 64-bit signed integer.
		///</summary>
		public long ToInt64()
		{
			Type tt = Type;
			if (tt == typeof(long))
				return (long)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return long.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (long)1;
			else
				return (long)0;
			if (tt == typeof(DateTime))
				return (long)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (long)((TimeSpan)_value).Ticks;
			return (long)ToType(typeof(long), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Int64"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 64-bit signed integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public long ToInt64(IFormatProvider formatProvider)
		{
			return (long)ToType(typeof(long), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Double"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent double-precision floating-point number.
		///</summary>
		public double ToDouble()
		{
			Type tt = Type;
			if (tt == typeof(double))
				return (double)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return double.Parse(
					_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (double)1;
			else
				return (double)0;
			if (tt == typeof(DateTime))
				return (double)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (double)((TimeSpan)_value).Ticks;
			return (double)ToType(typeof(double), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Double"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent double-precision floating-point number using the
		/// specified culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public double ToDouble(IFormatProvider formatProvider)
		{
			return (double)ToType(typeof(double), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Single"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent single-precision floating-point number.
		///</summary>
		public float ToSingle()
		{
			Type tt = Type;
			if (tt == typeof(float))
				return (float)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return float.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (float)1;
			else
				return (float)0;
			if (tt == typeof(DateTime))
				return (float)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (float)((TimeSpan)_value).Ticks;
			return (float)ToType(typeof(float), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Single"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent single-precision floating-point number using the
		/// specified culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public float ToSingle(IFormatProvider formatProvider)
		{
			return (float)ToType(typeof(float), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Decimal"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent Decimal number.
		///</summary>
		public decimal ToDecimal()
		{
			Type tt = Type;
			if (tt == typeof(decimal))
				return (decimal)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return decimal.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (decimal)1;
			else
				return (decimal)0;
			if (tt == typeof(DateTime))
				return (decimal)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (decimal)((TimeSpan)_value).Ticks;
			return (decimal)ToType(typeof(decimal), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Decimal"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent Decimal number using the specified culture-specific
		/// formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public decimal ToDecimal(IFormatProvider formatProvider)
		{
			return (decimal)ToType(typeof(decimal), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.SByte"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 8-bit signed integer.
		///</summary>
        [CLSCompliantAttribute(false)]
		public sbyte ToSByte()
		{
			Type tt = Type;
			if (tt == typeof(sbyte))
				return (sbyte)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return sbyte.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (SByte)1;
			else
				return (SByte)0;
			if (tt == typeof(DateTime))
				return (sbyte)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (sbyte)((TimeSpan)_value).Ticks;
			return (sbyte)ToType(typeof(SByte), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.SByte"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 8-bit signed integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
        [CLSCompliantAttribute(false)]
		public sbyte ToSByte(IFormatProvider formatProvider)
		{
			return (sbyte)ToType(typeof(sbyte), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.UInt16"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 16-bit unsigned integer.
		///</summary>
        [CLSCompliantAttribute(false)]
		public ushort ToUInt16()
		{
			Type tt = Type;
			if (tt == typeof(ushort))
				return (ushort)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return ushort.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (ushort)1;
			else
				return (ushort)0;
			if (tt == typeof(DateTime))
				return (ushort)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (ushort)((TimeSpan)_value).Ticks;
			return (ushort)ToType(typeof(ushort), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.UInt16"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 16-bit unsigned integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
        [CLSCompliantAttribute(false)]
		public ushort ToUInt16(IFormatProvider formatProvider)
		{
			return (ushort)ToType(typeof(ushort), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.UInt32"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 32-bit unsigned integer.
		///</summary>
        [CLSCompliantAttribute(false)]
		public uint ToUInt32()
		{
			Type tt = Type;
			if (tt == typeof(uint))
				return (uint)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return uint.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (uint)1;
			else
				return (uint)0;
			if (tt == typeof(DateTime))
				return (uint)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (uint)((TimeSpan)_value).Ticks;
			return (uint)ToType(typeof(uint), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.UInt32"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 32-bit unsigned integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
        [CLSCompliantAttribute(false)]
		public uint ToUInt32(IFormatProvider formatProvider)
		{
			return (uint)ToType(typeof(uint), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.UInt64"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 64-bit unsigned integer.
		///</summary>
        [CLSCompliantAttribute(false)]
		public ulong ToUInt64()
		{
			Type tt = Type;
			if (tt == typeof(ulong))
				return (ulong)_value;
			if (tt == typeof(string) || tt == typeof(char))
				return ulong.Parse(_value.ToString());
			if (tt == typeof(bool))
			if ((bool)_value)
				return (ulong)1;
			else
				return (ulong)0;
			if (tt == typeof(DateTime))
				return (ulong)((DateTime)_value).Ticks;
			if (tt == typeof(TimeSpan))
				return (ulong)((TimeSpan)_value).Ticks;
			return (ulong)ToType(typeof(ulong), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.UInt64"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent 64-bit unsigned integer using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
        [CLSCompliantAttribute(false)]
		public ulong ToUInt64(IFormatProvider formatProvider)
		{
			return (ulong)ToType(typeof(ulong), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.DateTime"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent DateTime.
		///</summary>
		public DateTime ToDateTime()
		{
			Type tt = Type;
			if (tt == typeof(DateTime))
				return (DateTime)_value;
			if (tt == typeof(TimeSpan))
				return new DateTime(((TimeSpan)_value).Ticks);
			if (tt == typeof(string) || tt == typeof(char))
			if (IsDate)
				return DateTime.Parse(_value.ToString());
			//			if (tt == typeof(bool))
			//				throw new InvalidCastException();
			if (IsNumberable)
				return new DateTime(ToInt64());
			return (DateTime)ToType(typeof(DateTime), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.DateTime"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent DateTime using the specified culture-specific
		/// formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public DateTime ToDateTime(IFormatProvider formatProvider)
		{
			return (DateTime)ToType(typeof(DateTime), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.TimeSpan"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent TimeSpan.
		///</summary>
		public TimeSpan ToTimeSpan()
		{
			Type tt = Type;
			if (tt == typeof(TimeSpan))
				return (TimeSpan)_value;
			if (tt == typeof(DateTime))
				return new TimeSpan(((DateTime)_value).Ticks);
			if (tt == typeof(string) || tt == typeof(char))
			if (IsDate)
				return new TimeSpan(DateTime.Parse(_value.ToString()).Ticks);
			//			if (tt == typeof(bool))
			//				throw new InvalidCastException();
			if (IsNumberable)
				return new TimeSpan(ToInt64());
			return (TimeSpan)ToType(typeof(TimeSpan), true);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.TimeSpan"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance to
		/// an equivalent TimeSpan using the specified culture-specific
		/// formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public TimeSpan ToTimeSpan(IFormatProvider formatProvider)
		{
			return (TimeSpan)ToType(typeof(TimeSpan), false, formatProvider);
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Char"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent Unicode character.
		///</summary>
		public char ToChar()
		{
			Type tt = Type;
			if (tt == typeof(char))
				return (char)_value;
			if (tt == typeof(float) ||
				tt == typeof(double) ||
				tt == typeof(decimal))
			{
				return (char)ToInt32();
			}
			if (tt == typeof(string))
				return char.Parse((string)_value);
			if (tt == typeof(bool))
			if ((bool)_value)
				return '1';
			else
				return '0';
			try
			{
				return (char)ToType(typeof(char), true);
			} catch
			{
				try
				{
					return char.Parse(_value.ToString());
				} catch
				{
					try
					{
						return _value.ToString().ToCharArray() [0];
					} catch
					{
					}
				}
			}
			throw new InvalidCastException();
		}
		///<summary>
		/// If <see cref="Value" /> is a <see cref="System.Char"/>, returns
		/// as-is. Otherwise, attempts to convert the value of this instance
		/// to an equivalent Unicode character using the specified
		/// culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider">The culture-specific formatting information.</param>
		public char ToChar(IFormatProvider formatProvider)
		{
			return (Char)ToType(typeof(Char), false, formatProvider);
		}
		private enum MathAction
		{
			Add,
			Subtract,
			Multiply,
			Divide,
			Modulus,
			BitAnd,
			BitOr
		}

		private static object VariantArithmetic(MathAction action, object target, object value)
		{
			object ret = null;
			Variant sv = new Variant(target);
			Variant av = new Variant(value);
			if (sv.IsNumberable && av.IsNumberable)
			{
				Double sd = sv.ToDouble();
				Double ad = av.ToDouble();
				long lsd = (long)sd;
				long lad = (long)ad;
				Type dt = sv.Type;
				switch (action)
				{
					case MathAction.Add:
						sd += ad;
						break;
					case MathAction.Subtract:
						sd -= ad;
						break;
					case MathAction.Multiply:
						sd *= ad;
						break;
					case MathAction.Divide:
						sd /= ad;
						break;
					case MathAction.Modulus:
						sd %= ad;
						break;
					case MathAction.BitAnd:
						lsd &= lad;
						sd = (double)lsd;
						break;
					case MathAction.BitOr:
						lsd |= lad;
						sd = (double)lsd;
						break;
				}
				if (sv.IsNumeric)
				{
					bool isfloat = (sd != Math.Round(sd, 0));
					bool expandminmax = false;
					bool expandfloat = false;
					bool signed = (
						dt == typeof(sbyte) ||
						dt == typeof(short) ||
						dt == typeof(int) ||
						dt == typeof(long) ||
						dt == typeof(float) ||
						dt == typeof(double) ||
						dt == typeof(decimal));
					if ((dt == typeof(byte) && (sd < byte.MinValue || sd > byte.MaxValue)) ||
						(dt == typeof(sbyte) && (sd < sbyte.MinValue || sd > sbyte.MaxValue)) ||
						(dt == typeof(short) && (sd < short.MinValue || sd > short.MaxValue)) ||
						(dt == typeof(ushort) && (sd < ushort.MinValue || sd > ushort.MaxValue)) ||
						(dt == typeof(int) && (sd < int.MinValue || sd > int.MaxValue)) ||
						(dt == typeof(uint) && (sd < uint.MinValue || sd > uint.MaxValue)) ||
						(dt == typeof(long) && (sd < long.MinValue || sd > long.MaxValue)) ||
						(dt == typeof(ulong) && (sd < ulong.MinValue || sd > ulong.MaxValue)) ||
						(dt == typeof(float) && (sd < float.MinValue || sd > float.MaxValue)) ||
						(dt == typeof(decimal) && (sd < (double)decimal.MinValue || sd > (double)decimal.MaxValue)))
						expandminmax = true;
					
					if (isfloat && (
						dt == typeof(byte) ||
						dt == typeof(sbyte) ||
						dt == typeof(short) ||
						dt == typeof(ushort) ||
						dt == typeof(int) ||
						dt == typeof(uint) ||
						dt == typeof(long) ||
						dt == typeof(ulong)))
						expandfloat = true;
					if (expandfloat)
					{
						if (sd < (double)decimal.MinValue || sd > (double)decimal.MaxValue)
						{
							ret = sd;
						} else if ((float)sd < (float)float.MinValue || sd > (float)float.MaxValue)
						{
							ret = (decimal)sd;
						} else
						{
							ret = (float)sd;
						}
					} else if (expandminmax)
					{
						if (dt == typeof(byte) ||
							dt == typeof(sbyte) ||
							dt == typeof(short) ||
							dt == typeof(ushort) ||
							dt == typeof(int) ||
							dt == typeof(uint) ||
							dt == typeof(long) ||
							dt == typeof(ulong))
						{
							if (sd < 0 || signed)
							{
								long lmin = long.MinValue;
								long lmax = long.MaxValue;
								if (dt == typeof(sbyte))
								{
									lmin = sbyte.MinValue;
									lmax = sbyte.MaxValue;
								}
								if (dt == typeof(short))
								{
									lmin = short.MinValue;
									lmax = short.MaxValue;
								}
								if (dt == typeof(int))
								{
									lmin = int.MinValue;
									lmax = int.MaxValue;
								}
								if (sd < long.MinValue || sd > long.MaxValue ||
									lmin < long.MinValue || lmax > long.MaxValue)
								{
									ret = sd;
								} else if (sd < int.MinValue || sd > int.MaxValue ||
									lmin < int.MinValue || lmax > int.MaxValue)
								{
									ret = (long)sd;
								} else if (sd < short.MinValue || sd > short.MaxValue || lmin < short.MinValue || lmax > short.MaxValue)
								{
									ret = (int)sd;
								} else if (sd < sbyte.MinValue || sd > sbyte.MaxValue || lmin < sbyte.MinValue || lmax > sbyte.MaxValue)
								{
									ret = (short)sd;
								}
							} else
							{
								ulong lmax = ulong.MaxValue;
								if (dt == typeof(byte))
									lmax = byte.MaxValue;
								if (dt == typeof(ushort))
									lmax = ushort.MaxValue;
								if (dt == typeof(uint))
									lmax = uint.MaxValue;
								if (sd < ulong.MinValue || sd > ulong.MaxValue || lmax > ulong.MaxValue)
								{
									ret = sd;
								} else if (sd < uint.MinValue || sd > uint.MaxValue || lmax > uint.MaxValue)
								{
									ret = (ulong)sd;
								} else if (sd < ushort.MinValue || sd > ushort.MaxValue || lmax > ushort.MaxValue)
								{
									ret = (uint)sd;
								} else if (sd < byte.MinValue || sd > byte.MaxValue || lmax > byte.MaxValue)
								{
									ret = (ushort)sd;
								}
							}
						} else
						{
							ret = sd;
						}
					} else
					{ // Not expandfloat and not expandminmax,
						// so revert to original type!
						ret = System.Convert.ChangeType(sd, sv.Type);
					}
				} else
				{ // not numeric
					Variant v = new Variant(sd);
					ret = v.ToType(sv.Type);
				}
			}
			return ret;
		}
		///<summary>
		/// Addition operator.
		///</summary>
		/// <remarks>
		/// If the value on the right is a <see cref="System.String"/>
		/// or a <see cref="System.Char"/>,
		/// the Variant is converted to a string and appended.
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/> property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator +(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if (((sv.Type == typeof(string) || sv.Type == typeof(char)) && !sv.IsNumberable) || (tt == typeof(string) || tt == typeof(char)))
				return new Variant(sv.ToString() + value.ToString());

			if ((sv.IsDate && dv.IsDate) ||
				(sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) || (sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks + dv.ToTimeSpan().Ticks));
			}

			if (sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan))
				return new Variant(sv.ToTimeSpan() + dv.ToTimeSpan());

			if (tt == typeof(bool))
			{ // change to boolean and toggle if true
				Variant ret = new Variant(sv.ToBoolean());
				if ((bool)value)
				{
					ret = new Variant(!ret.ToBoolean());
				}
				return ret;
			}

			object retobj = VariantArithmetic(MathAction.Add, sv, value);
			if (retobj != null)
				return new Variant(retobj);
			throw new InvalidOperationException("Cannot implicitly add value to unidentified type.");
		}
		///<summary>
		/// Subtraction operator.
		///</summary>
		/// <remarks>
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/> property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator -(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if ((sv.IsDate && dv.IsDate) ||
				(sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) ||
				(sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks - dv.ToTimeSpan().Ticks));
			}

			if (sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan))
				return new Variant(sv.ToTimeSpan() - dv.ToTimeSpan());

			if (tt == typeof(string))
				throw new InvalidOperationException("Objects of type System.ToString() cannot subtract.");

			if (tt == typeof(char))
				throw new InvalidOperationException("Objects of type System.ToChar() cannot subtract.");

			if (tt == typeof(bool))
			{ // change to boolean and toggle if false
				Variant ret = new Variant(sv.ToBoolean());
				if (!(bool)value)
				{
					ret = new Variant(!ret.ToBoolean());
				}
				return ret;
			}
			object retobj = VariantArithmetic(MathAction.Subtract, sv, value);

			if (retobj != null)
				return new Variant(retobj);
			throw new InvalidOperationException("Cannot implicitly add value to unidentified type.");
		}

		///<summary>
		/// Multiplication operator.
		///</summary>
		/// <remarks>
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/> property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator *(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if ((sv.IsDate && dv.IsDate) ||
				(sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) || (sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks * dv.ToTimeSpan().Ticks));
			}

			if (sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan))
				return new Variant(new TimeSpan(sv.ToTimeSpan().Ticks * dv.ToTimeSpan().Ticks));

			if (tt == typeof(string))
				throw new InvalidOperationException("Objects of type System.ToString() cannot multiply.");

			if (tt == typeof(char))
				throw new InvalidOperationException("Objects of type System.ToChar() cannot multiply.");

			if (tt == typeof(bool))
			{ // change to boolean and multiply by 1 (true) or 0 (false)
				Variant ret = new Variant(sv.ToBoolean());
				if (!(bool)value)
				{
					ret = new Variant(!ret.ToBoolean());
				}
				return ret;
			}
			object retobj = VariantArithmetic(MathAction.Multiply, sv, value);
			if (retobj != null)
				return new Variant(retobj);
			throw new InvalidOperationException(
				"Cannot implicitly add value to unidentified type.");
		}
		///<summary>
		/// Division operator.
		///</summary>
		/// <remarks>
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/> property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator /(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if ((sv.IsDate && dv.IsDate) ||
				(sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) ||
				(sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks / dv.ToTimeSpan().Ticks));
			}
			if (sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan))
				return new Variant(new TimeSpan(sv.ToTimeSpan().Ticks / dv.ToTimeSpan().Ticks));
			if (tt == typeof(string))
				throw new InvalidOperationException("Objects of type System.ToString() cannot divide.");
			if (tt == typeof(char))
				throw new InvalidOperationException("Objects of type System.ToChar() cannot divide.");
			if (tt == typeof(bool))
			{
				throw new InvalidOperationException("Objects of type System.ToChar() cannot apply division.");
			}
			
			object retobj = VariantArithmetic(MathAction.Divide, sv, value);
			if (retobj != null)
				return new Variant(retobj);
			throw new InvalidOperationException("Cannot implicitly add value to unidentified type.");
		}
		///<summary>
		/// Modulus operator.
		///</summary>
		/// <remarks>
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/> property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator %(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if ((sv.IsDate && dv.IsDate) ||
				(sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) || (sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks / dv.ToTimeSpan().Ticks));
			}
			if (sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan))
				return new Variant(new TimeSpan(sv.ToTimeSpan().Ticks % dv.ToTimeSpan().Ticks));
			if (tt == typeof(string))
				throw new InvalidOperationException("Objects of type System.ToString() cannot apply modulus.");
			if (tt == typeof(char))
				throw new InvalidOperationException("Objects of type System.ToChar() cannot apply modulus.");
			if (tt == typeof(bool))
				throw new InvalidOperationException("Objects of type System.Boolean cannot apply modulus.");
			object retobj = VariantArithmetic(MathAction.Modulus, sv, value);
			if (retobj != null)
				return new Variant(retobj);
			throw new InvalidOperationException("Cannot implicitly add value to unidentified type.");
		}
		///<summary>
		/// Bitwise And operator.
		///</summary>
		/// <remarks>
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/>property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator &(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if (tt == typeof(float))
				throw new InvalidOperationException("Operator '&' cannot "
					+ "be applied to operands of type 'float' and 'float'.");
			if (tt == typeof(double))
				throw new InvalidOperationException("Operator '&' cannot "
					+ "be applied to operands of type 'double' and 'double'.");
			if (tt == typeof(decimal))
				throw new InvalidOperationException("Operator '&' cannot "
					+ "be applied to operands of type 'decimal' and 'decimal'.");
			if ((sv.IsDate && dv.IsDate) ||
				(sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) || (sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks & dv.ToTimeSpan().Ticks));
			}
			if (sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan))
				return new Variant(new TimeSpan(sv.ToTimeSpan().Ticks & dv.ToTimeSpan().Ticks));
			if (tt == typeof(string))
				throw new InvalidOperationException("Objects of type System.ToString() cannot apply '&' operator.");
			if (tt == typeof(char))
				throw new InvalidOperationException("Objects of type System.ToChar() cannot apply '&' operator.");
			if (tt == typeof(bool))
				return new Variant(sv.ToBoolean() & (bool)value);
			object retobj = VariantArithmetic(MathAction.BitAnd, sv, value);
			if (retobj != null)
				return new Variant(retobj);
			throw new InvalidOperationException("Cannot implicitly add value to unidentified type.");
		}
		///<summary>
		/// Bitwise Or operator.
		///</summary>
		/// <remarks>
		/// If the value on the right or the Variant
		/// is a <see cref="System.DateTime"/>, arithmetic
		/// is performed on the <see cref="DateTime.Ticks"/> property and the
		/// resulting value is set to the DateTime type.
		/// Otherwise, if the value on the right is a number, both
		/// the Variant and the value on the right are
		/// converted to a <see cref="System.Double"/>, the arithmetic
		/// is performed, and the resulting value is converted back to the
		
		/// original type that the Variant previously represented.
		/// If the type that the Variant previously represented
		/// cannot contain the resulting value--such as if the type is a
		/// <see cref="System.UInt32"/> and the value is <c>-12</c>--then the
		/// type will be converted to a type that can contain
		/// the value, such as <see cref="System.Int32"/>.
		/// </remarks>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>A new <see cref="Variant"/> containing the resulting value.</returns>
		public static Variant operator |(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Type tt = value.GetType();
			if (tt == typeof(bool) && sv.IsNumeric)
			{
				if ((bool)value)
					value = 1;
				else
					value = 0;
				tt = value.GetType();
			}
			if (tt == typeof(float))
				throw new InvalidOperationException("Operator '|' cannot be applied to operands of type 'float' and 'float'.");

			if (tt == typeof(double))
				throw new InvalidOperationException("Operator '|' cannot be applied to operands of type 'double' and 'double'.");

			if (tt == typeof(decimal))
				throw new InvalidOperationException("Operator '|' cannot be applied to operands of type 'decimal' and 'decimal'.");

			if ((sv.IsDate && dv.IsDate) || (sv.IsDate && dv.IsTimeSpan) || (sv.IsTimeSpan && dv.IsDate) || (sv.IsDate && dv.IsNumberable))
			{
				return new Variant(new DateTime(sv.ToTimeSpan().Ticks | dv.ToTimeSpan().Ticks));
			}
			if ((sv.Type == typeof(TimeSpan) && tt == typeof(TimeSpan)) || (sv.IsNumberable && tt == typeof(TimeSpan)) || (sv.Type == typeof(TimeSpan) && dv.IsNumberable))
				return new Variant(new TimeSpan(sv.ToTimeSpan().Ticks | dv.ToTimeSpan().Ticks));

			if (tt == typeof(string))
				throw new InvalidOperationException("Objects of type System.ToString() cannot apply '|' operator.");

			if (tt == typeof(char))
				throw new InvalidOperationException("Objects of type System.ToChar() cannot apply '|' operator.");

			if (tt == typeof(bool))
				return new Variant(sv.ToBoolean() | (bool)value);

			object retobj = VariantArithmetic(MathAction.BitOr, sv, value);
			if (retobj != null)
				return new Variant(retobj);

			throw new InvalidOperationException("Cannot implicitly add value to unidentified type.");
		}
		///<summary>
		/// Inequality operator.
		///</summary>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns>
		/// The opposite of <see cref="operator =="/>
		/// </returns>
		public static bool operator !=(Variant subjectVariant, object value)
		{
			return !(subjectVariant == value);
		}
		
		///<summary>
		/// <para>Equality operator.</para>
		/// <para>First attempts to compare the left value after
		/// temporarily converting it to the type of the right value.
		/// If the conversion cannot occur, such as if the value is not an
		/// intrinsic value type, the comparison occurs at the <see cref="System.Object"/>
		/// level using <see cref="Object.Equals"/>.</para>
		///</summary>
		/// <param name="subjectVariant"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool operator ==(Variant subjectVariant, object value)
		{
			if (value.GetType() == typeof(Variant))
				value = ((Variant)value).Value;
			Variant sv = subjectVariant; // smaller var name :)
			Variant dv = new Variant(value);
			Variant dvv = dv;
			if (sv.IsNumberable && dv.IsNumberable)
			{
				sv = new Variant(sv.ToDouble());
				dvv = new Variant(dv.ToDouble());
			}
			//if (sv.Type != dvv.Type) return false;
			if (sv.IsDate && dv.IsDate)
				return sv.ToDateTime() == dv.ToDateTime();
			if (sv.IsTimeSpan && dv.IsTimeSpan)
				return sv.ToTimeSpan() == dvv.ToTimeSpan();
			Type tt = dvv.Type;
			if (tt == typeof(string) || tt == typeof(char))
			if (sv.ToString() == dvv.ToString())
				return true;
			if (dv.IsNumeric)
				return sv.ToDouble() == dv.ToDouble();
			if (dv.Type == typeof(bool))
				return sv.ToBoolean() == (bool)value;
			return sv.Value == dv.Value;//Object.Equals(subjectVariant.Value, value);
		}
		///<summary>
		/// Returns <see cref="String"/> property unless the value on the right
		/// is null. If the value on the right is null, returns "".
		///</summary>
		/// <returns></returns>
		public override string ToString()
		{
			if (Value == null)
				return "";
			return String;
		}
		///<summary>
		/// Converts the value of this instance to an equivalent <see cref="String"/>
		/// using the specified culture-specific formatting information.
		///</summary>
		/// <param name="formatProvider"></param>
		/// <returns></returns>
		public string ToString(IFormatProvider formatProvider)
		{
			return (string)ToType(typeof(string), false, formatProvider);
		}
		///<summary>
		/// See <see cref="Object.GetHashCode()"/>.
		///</summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		///<summary>
		/// See <see cref="Object.Equals(object)"/>.
		///</summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
		///<summary>
		/// Converts an object to a boolean.
		/// For any type, if null, returns false.
		/// For Boolean: true/false.
		/// For String: "", "false", "0", etc. == false;
		///		"1", "true", etc. == true, else true.
		/// For numeric intrinsics: 0 == false, else true.
		/// For any other non-null object, returns true.
		///</summary>
		/// <param name="bln">The string to be converted</param>
		/// <returns>The boolean value of this string.</returns>
		public static bool CBln(object bln)
		{
			if (bln == null)
				return false;
			Type tt = bln.GetType();
			if (tt == typeof(bool))
			{
				return (bool)bln;
			} else if (tt == typeof(string))
			{
				string val = (string)bln;
				bool ret = true;
				val = val.ToLower().Trim();
				string sTrue = true.ToString().ToLower();
				string sFalse = false.ToString().ToLower();
				if (val == "" ||
					val == "false" ||
					val == "f" ||
					val == "0" ||
					val == "no" ||
					val == "n" ||
					val == "off" ||
					val == "negative" ||
					val == "neg" ||
					val == "disabled" ||
					val == "incorrect" ||
					val == "wrong" ||
					val == "left" ||
					val == sFalse)
				{
					ret = false;
					return ret;
				}
				if (val == "true" ||
					val == "t" ||
					val == "1" ||
					val == "-1" ||
					val == "yes" ||
					val == "y" ||
					val == "positive" ||
					val == "pos" ||
					val == "on" ||
					val == "enabled" ||
					val == "correct" ||
					val == "right" ||
					val == sTrue)
				{
					ret = true;
					return ret;
				}
				try
				{
					ret = bool.Parse(val);
				} catch
				{
				}
				return ret;
			} else if (tt == typeof(byte) ||
				tt == typeof(ushort) ||
				tt == typeof(decimal) ||
				tt == typeof(sbyte) ||
				tt == typeof(ulong) ||
				tt == typeof(int) ||
				tt == typeof(uint) ||
				tt == typeof(long) ||
				tt == typeof(short) ||
				tt == typeof(double) ||
				tt == typeof(float))
			{
				var blnVal = bln.ToString();
				if (blnVal != "0" && blnVal != "0.0")
				{
					return true;
				} else
				{
					return false;
				}
			}
			return true;
		}
		#endregion Methods 

		///<summary>
		/// Converts the value to a <see cref="System.String"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// <para>Calls <see cref="Variant.ToString()"/>.</para>
		/// <para>In most cases, <see cref="Object.ToString()"/> will be called
		/// on the <see cref="Value"/>.</para>
		/// </remarks>
		public static explicit operator string(Variant v)
		{
			return v.ToString();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Char"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToChar()"/>.
		/// </remarks>
		public static explicit operator char(Variant v)
		{
			return v.ToChar();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Byte"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToByte()"/>.
		/// </remarks>
		public static explicit operator byte(Variant v)
		{
			return v.ToByte();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.SByte"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToSByte()"/>.
		/// </remarks>
        [CLSCompliantAttribute(false)]
		public static explicit operator sbyte(Variant v)
		{
			return v.ToSByte();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Int16"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToInt16()"/>.
		/// </remarks>
		public static explicit operator short(Variant v)
		{
			return v.ToInt16();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.UInt16"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToUInt16()"/>.
		/// </remarks>
        [CLSCompliantAttribute(false)]
		public static explicit operator ushort(Variant v)
		{
			return v.ToUInt16();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Int32"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToInt32()"/>.
		/// </remarks>
		public static explicit operator int(Variant v)
		{
			return v.ToInt32();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.UInt32"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToUInt32()"/>.
		/// </remarks>
        [CLSCompliantAttribute(false)]
		public static explicit operator uint(Variant v)
		{
			return v.ToUInt32();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Int64"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToInt64()"/>.
		/// </remarks>
		public static explicit operator long(Variant v)
		{
			return v.ToInt64();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.UInt64"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToUInt64()"/>.
		/// </remarks>
        [CLSCompliantAttribute(false)]
		public static explicit operator ulong(Variant v)
		{
			return v.ToUInt64();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Single"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToSingle()"/>.
		/// </remarks>
		public static explicit operator float(Variant v)
		{
			return v.ToSingle();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Double"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToDouble()"/>.
		/// </remarks>
		public static explicit operator double(Variant v)
		{
			return v.ToDouble();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Decimal"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToDecimal()"/>.
		/// </remarks>
		public static explicit operator decimal(Variant v)
		{
			return v.ToDecimal();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.Boolean"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToBoolean()"/>.
		/// </remarks>
		public static explicit operator bool(Variant v)
		{
			return v.ToBoolean();
		}
		///<summary>
		/// Attempts to convert the value to a <see cref="System.DateTime"/>.
		///</summary>
		/// <param name="v"></param>
		/// <returns></returns>
		/// <remarks>
		/// Calls <see cref="Variant.ToDateTime()"/>.
		/// </remarks>
		public static explicit operator DateTime(Variant v)
		{
			return v.ToDateTime();
		}

		#region Operator Simple
		
		public static implicit operator Variant(string value)
		{
			return new Variant(value);
		}

        [CLSCompliantAttribute(false)]
		public static implicit operator Variant(ushort value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(short value)
		{
			return new Variant(value);
		}
		
        [CLSCompliantAttribute(false)]
        public static implicit operator Variant(uint value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(int value)
		{
			return new Variant(value);
		}
		
        [CLSCompliantAttribute(false)]
        public static implicit operator Variant(ulong value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(long value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(float value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(double value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(decimal value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(DateTime value)
		{
			return new Variant(value);
		}
		
		public static implicit operator Variant(TimeSpan value)
		{
			return new Variant(value);
		}
		
		#endregion Operator Simple
	}
}