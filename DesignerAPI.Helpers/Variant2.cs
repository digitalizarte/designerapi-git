//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Components
{
	using System;
	using System.Linq;
	using DesignerAPI.Model;
	using System.Collections.Generic;

	public class Variant2: IConvertible
	{
		#region Ctx

		#region Ctx Simple
		public Variant2(object value)
		{
			throw new NotImplementedException();
		}

		public Variant2(string value)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
		public Variant2(ushort value)
		{
			throw new NotImplementedException();
		}

		public Variant2(short value)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
		public Variant2(uint value)
		{
			throw new NotImplementedException();
		}

		public Variant2(int value)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
		public Variant2(ulong value)
		{
			throw new NotImplementedException();
		}

		public Variant2(long value)
		{
			throw new NotImplementedException();
		}

		public Variant2(float value)
		{
			throw new NotImplementedException();
		}

		public Variant2(double value)
		{
			throw new NotImplementedException();
		}

		public Variant2(DateTime value)
		{
			throw new NotImplementedException();
		}
		#endregion

		#region Ctx Array
		public Variant2(object[]  value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(string[]  value)
		{
			throw new NotImplementedException();
		}
		
        [CLSCompliantAttribute(false)]
		public Variant2(ushort[] value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(short[] value)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
		public Variant2(uint[]  value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(int[]  value)
		{
			throw new NotImplementedException();
		}
		
        [CLSCompliantAttribute(false)]
		public Variant2(ulong[]  value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(long[]  value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(float[]  value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(double[]  value)
		{
			throw new NotImplementedException();
		}

		public Variant2(DateTime[] value)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Ctx List
		public Variant2(IList<object> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<string> value)
		{
			throw new NotImplementedException();
		}

		public Variant2(IList<ushort> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<short> value)
		{
			throw new NotImplementedException();
		}

		public Variant2(IList<uint> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<int> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<ulong> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<long> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<float> value)
		{
			throw new NotImplementedException();
		}
		
		public Variant2(IList<double> value)
		{
			throw new NotImplementedException();
		}

		public Variant2(IList<DateTime> value)
		{
			throw new NotImplementedException();
		}
		#endregion

		#endregion 

		#region Operator

		#region Operator Simple

		public static implicit operator Variant2(string value)
		{
			return new Variant2(value);
		}

        [CLSCompliantAttribute(false)]
		public static implicit operator Variant2(ushort value)
		{
			return new Variant2(value);
		}

		public static implicit operator Variant2(short value)
		{
			return new Variant2(value);
		}

        [CLSCompliantAttribute(false)]
		public static implicit operator Variant2(uint value)
		{
			return new Variant2(value);
		}

		public static implicit operator Variant2(int value)
		{
			return new Variant2(value);
		}

        [CLSCompliantAttribute(false)]	
		public static implicit operator Variant2(ulong value)
		{
			return new Variant2(value);
		}
				
		public static implicit operator Variant2(long value)
		{
			return new Variant2(value);
		}

		public static implicit operator Variant2(float value)
		{
			return new Variant2(value);
		}
				
		public static implicit operator Variant2(double value)
		{
			return new Variant2(value);
		}

		public static implicit operator Variant2(DateTime value)
		{
			return new Variant2(value);
		}
		
		#endregion Operator Simple

		#region Operator Array

		public static implicit operator Variant2(string[]  value)
		{
			return new Variant2(value);
		}
		
        [CLSCompliantAttribute(false)]
		public static implicit operator Variant2(ushort[]  value)
		{
			return new Variant2(value);
		}
				
		public static implicit operator Variant2(short[]  value)
		{
			return new Variant2(value);
		}
		
        [CLSCompliantAttribute(false)]
		public static implicit operator Variant2(uint[]  value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(int[]  value)
		{
			return new Variant2(value);
		}
  
        [CLSCompliantAttribute(false)]
		public static implicit operator Variant2(ulong[]  value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(long[]  value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(float[]  value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(double[]  value)
		{
			return new Variant2(value);
		}

		public static implicit operator Variant2(DateTime[]  value)
		{
			return new Variant2(value);
		}
		#endregion Operator Array  

		#region Operator List
		
		public static implicit operator Variant2(List<string> value)
		{
			return new Variant2(value);
		}
		
        [CLSCompliantAttribute(false)]
        public static implicit operator Variant2(List<ushort> value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(List<short> value)
		{
			return new Variant2(value);
		}
		
        [CLSCompliantAttribute(false)]
        public static implicit operator Variant2(List<uint> value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(List<int> value)
		{
			return new Variant2(value);
		}
		
        [CLSCompliantAttribute(false)]
        public static implicit operator Variant2(List<ulong> value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(List<long> value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(List<float> value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(List<double> value)
		{
			return new Variant2(value);
		}
		
		public static implicit operator Variant2(List<DateTime> value)
		{
			return new Variant2(value);
		}

		#endregion Operator List  

		#endregion Operator

		#region Methods 

		public object AsObject()
		{
			throw new NotImplementedException();
		}

		public string AsString()
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
        public ushort AsUShort()
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
		public uint AsUInt()
		{
			throw new NotImplementedException();
		}

		public int AsInt()
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
        public ulong AsULong()
		{
			throw new NotImplementedException();
		}
		
		public long AsLong()
		{
			throw new NotImplementedException();
		}

		public float AsFloat()
		{
			throw new NotImplementedException();
		}

		public double AsDouble()
		{
			throw new NotImplementedException();
		}

		public double AsDateTime()
		{
			throw new NotImplementedException();
		}
		#endregion 

		#region IConvertible implementation

		public TypeCode GetTypeCode()
		{
			throw new NotImplementedException();
		}

		public bool ToBoolean(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public char ToChar(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
        public sbyte ToSByte(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public byte ToByte(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public short ToInt16(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
		public ushort ToUInt16(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public int ToInt32(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
        public uint ToUInt32(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public long ToInt64(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

        [CLSCompliantAttribute(false)]
        public ulong ToUInt64(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public float ToSingle(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public double ToDouble(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public decimal ToDecimal(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public DateTime ToDateTime(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public string ToString(IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		public object ToType(Type conversionType, IFormatProvider provider)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}