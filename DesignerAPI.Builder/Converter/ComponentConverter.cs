
namespace DesignerAPI.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using DesignerAPI.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    internal class ComponentConverter : JsonConverter
    {
        #region implemented abstract members of JsonConverter
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {	
            /*
            Console.Error.WriteLine();
            Console.Error.WriteLine("ComponentConverter");
            Console.Error.WriteLine();
            */
            Component val = value as Component;
                        
            writer.WriteStartObject();

            writer.WritePropertyName("id");
            writer.WriteValue(val.Id);

            writer.WritePropertyName("type");
            writer.WriteValue(val.Type);

            writer.WritePropertyName("reference");
            serializer.Serialize(writer, val.Reference);

            writer.WritePropertyName("codeClass");
            if (val.CodeClass == null)
            {
                writer.WriteNull();
            } else
            {
                writer.WriteValue(val.CodeClass);
            }

            if (val.UserConfig != null)
            {
                writer.WritePropertyName("userConfig");
                serializer.Serialize(writer, val.UserConfig);
            }

            if (val.CustomConfigs != null)
            {
                writer.WritePropertyName("customConfigs");
                serializer.Serialize(writer, val.CustomConfigs);
            }

            if (val.IconCls != null)
            {
                writer.WritePropertyName("iconCls");
                writer.WriteValue(val.IconCls);
            }

            if (val.DesignerId != null)
            {
                writer.WritePropertyName("designerId");
                writer.WriteValue(val.DesignerId);
            }

            if (val.ViewOrder.HasValue)
            {
                writer.WritePropertyName("$viewOrder");
                writer.WriteValue(val.ViewOrder);
            }

            if (val.Expanded.HasValue)
            {
                writer.WritePropertyName("expanded");
                writer.WriteValue(val.Expanded);
            }


            if (val.MasterInstanceId != null)
            {
                writer.WritePropertyName("masterInstanceId");
                writer.WriteValue(val.MasterInstanceId);
            }

            if (val.Components != null)
            {
                writer.WritePropertyName("cn");
                serializer.Serialize(writer, val.Components);			
            }

            writer.WriteEndObject();
        }
        
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotSupportedException("ComponentConverter should only be used while serializing.");
        }
        
        public override bool CanConvert(Type objectType)
        {
            // return typeof(Component).IsAssignableFrom (objectType);
            return typeof(Component) == objectType;
        }
#endregion
        
        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

    }
}
