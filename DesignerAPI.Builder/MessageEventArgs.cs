
namespace DesignerAPI.Builder
{
	using System;

	public class MessageEventArgs: EventArgs
	{
		public MessageEventArgs ()
		{
		}

		public MessageEventArgs (string message)
		{
			Message = message;
		}

		///<summary>
		/// Gets or sets the message.
		///</summary>
		/// <value>
		/// The message.
		/// </value>
		public string Message { 
			get;
			set;
		}
	}
	
}
