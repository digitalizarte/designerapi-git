namespace DesignerAPI.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters;
    using System.Text;

    using DesignerAPI.Model;

    using Newtonsoft.Json;

    /// <summary>The message.</summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The e.</param>
    public delegate void Message(object sender, MessageEventArgs e);

    /// <summary>The project builder.</summary>
    public class ProjectBuilder
    {
        #region Static Fields

        /// <summary>The sync lock.</summary>
        private static readonly object SyncLock = new object();

        #endregion

        #region Fields

        /// <summary>The settings.</summary>
        private JsonSerializerSettings settings;

        #endregion

        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="ProjectBuilder"/> class.</summary>
        public ProjectBuilder()
        {
            this.New();
        }

        #endregion

        #region Public Events

        /// <summary>The message.</summary>
        public virtual event Message Message;

        #endregion

        #region Public Properties

        /// <summary>Gets or sets the app directory.</summary>
        public virtual string AppDirectory { get; set; }

        /// <summary>
        /// Gets or sets the application.
        /// </summary>
        /// <value>
        /// The application.
        /// </value>
        public virtual Application Application { get; set; }

        /// <summary>
        /// Gets or sets the application file.
        /// </summary>
        /// <value>
        /// The application file.
        /// </value>
        public virtual string ApplicationFile { get; set; }

        /// <summary>
        /// Gets or sets the architect.
        /// </summary>
        /// <value>
        /// The architect.
        /// </value>
        public virtual Architect Architect { get; set; }

        /// <summary>
        /// Gets or sets the architect file.
        /// </summary>
        /// <value>
        /// The architect file.
        /// </value>
        public virtual string ArchitectFile { get; set; }

        /// <summary>
        /// Gets or sets the components.
        /// </summary>
        /// <value>
        /// The components.
        /// </value>
        public virtual EnumerableId<Component> Components { get; set; }

        /// <summary>Gets or sets the components files.</summary>
        public virtual IDictionary<string, IDictionary<string, string>> ComponentsFiles { get; set; }

        /// <summary>
        /// Gets or sets the metadata directory.
        /// </summary>
        /// <value>
        /// The metadata directory.
        /// </value>
        public virtual string MetadataDirectory { get; set; }

        /// <summary>
        /// Gets or sets the project.
        /// </summary>
        /// <value>
        /// The project.
        /// </value>
        public virtual Project Project { get; set; }

        /// <summary>
        /// Gets or sets the project directory.
        /// </summary>
        /// <value>
        /// The project directory.
        /// </value>
        public virtual string ProjectDirectory { get; set; }

        /// <summary>Gets or sets the project encoding.</summary>
        public virtual Encoding ProjectEncoding { get; set; }

        /// <summary>
        /// Gets or sets the project file.
        /// </summary>
        /// <value>
        /// The project file.
        /// </value>
        public virtual string ProjectFile { get; set; }

        /// <summary>Gets or sets the resource directory.</summary>
        public virtual string ResourceDirectory { get; set; }

        #endregion

        #region Properties

        /// <summary>Gets the serializer settings.</summary>
        protected virtual JsonSerializerSettings SerializerSettings
        {
            get
            {
                lock (SyncLock)
                {
                    if (this.settings == null)
                    {
                        this.settings = new JsonSerializerSettings
                        {
                            Formatting = Formatting.Indented,
                            NullValueHandling = NullValueHandling.Ignore,
                            ReferenceLoopHandling = ReferenceLoopHandling.Error,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            Culture = CultureInfo.InvariantCulture,
                            MissingMemberHandling = MissingMemberHandling.Ignore,
                            Converters = new List<JsonConverter> { 
                                new ComponentConverter(), 
                                new ApplicationConverter(), 
                                new ExpandedStateConverter()
                             },
                            TypeNameHandling = TypeNameHandling.None,
                            TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
                        };

                        // settings.Converters.Add (new KeyValuePairConverter ());
                    }
                }

                return this.settings;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>Clear this instance.</summary>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        public virtual ProjectBuilder Clear()
        {
            this.ProjectDirectory = null;
            this.ProjectFile = null;
            this.Project = null;
            this.MetadataDirectory = null;
            this.ApplicationFile = null;
            this.Application = null;
            this.Architect = null;
            this.ComponentsFiles = null;
            this.AppDirectory = null;
            this.ResourceDirectory = null;

            return this;
        }

        /// <summary>Load the specified project file.</summary>
        /// <param name="projectFile">Project file.</param>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        public virtual ProjectBuilder Load(string projectFile)
        {
            if (projectFile == null)
            {
                throw new ArgumentNullException("projectFile");
            }

            if (!File.Exists(projectFile))
            {
                throw new FileNotFoundException("projectFile", projectFile);
            }

            this.ProjectFile = projectFile;
            this.ProjectDirectory = Path.GetDirectoryName(projectFile);

            Encoding encoding;
            this.Project = this.DeserializeFile<Project>(projectFile, out encoding);
            this.ProjectEncoding = encoding;

            // Console.Error.WriteLine(JsonConvert.SerializeObject(Project));
            if (this.Project.XDSVersion != null
                && string.Compare(this.Project.XDSVersion, "1.2.3", StringComparison.InvariantCulture) > 0)
            {
                this.MetadataDirectory = Path.Combine(this.ProjectDirectory, "metadata");
                this.AppDirectory = Path.Combine(this.ProjectDirectory, "app");
                this.ResourceDirectory = Path.Combine(this.ProjectDirectory, "resource");
                this.ApplicationFile = Path.Combine(this.MetadataDirectory, "Application");

                this.Application = this.DeserializeFile<Application>(this.ApplicationFile);

                if (string.Compare(this.Project.XDSVersion, "2.0.0", StringComparison.InvariantCulture) > 0)
                {
                    this.ArchitectFile = Path.Combine(this.ProjectDirectory, ".architect");
                    this.Architect = this.DeserializeFile<Architect>(this.ArchitectFile);
                }

                this.Components = this.LoadComponents();
                this.ComponentsFiles = this.LoadComponentsFiles();
            }
            else
            {
                this.Components = this.Project.Components;
            }

            return this;
        }

        /// <summary>The new.</summary>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        public virtual ProjectBuilder New()
        {
            this.Clear();

            this.Project = new Project { Components = new EnumerableId<Component>() };
            this.Components = this.Project.Components;
            this.Application = new Application();
            this.Architect = new Architect();
            this.ComponentsFiles = new Dictionary<string, IDictionary<string, string>>();
            this.ProjectEncoding = Encoding.Default;

            return this;
        }

        /// <summary>Save this instance.</summary>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        public virtual ProjectBuilder Save()
        {
            return this.Save(this.ProjectFile);
        }

        /// <summary>Save the specified projectFile.</summary>
        /// <param name="projectFile">Project file.</param>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        public virtual ProjectBuilder Save(string projectFile)
        {
            ProjectBuilder ret;
            if (projectFile == null)
            {
                throw new ArgumentNullException("projectFile");
            }

            if (File.Exists(projectFile))
            {
                ret = this.Update(projectFile);
            }
            else
            {
                ret = this.Create(projectFile);
            }

            return ret;
        }

        #endregion

        #region Methods

        /// <summary>Create the specified projectFile.</summary>
        /// <param name="projectFile">Project file.</param>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        protected virtual ProjectBuilder Create(string projectFile)
        {
            string directoryPath = Path.GetDirectoryName(projectFile);
            this.CreateDirectoryIfNotExists(directoryPath);

            EnumerableId<Store> localStores = this.Project.Stores;
            EnumerableId<Component> localComponents = this.Project.Components;
            FileMap localTopInstanceFileMap = this.Project.TopInstanceFileMap;
            ExpandedState localExpandedState = this.Project.ExpandedState;
            ViewOrderMap localViewOrderMap = this.Project.ViewOrderMap;
            int? localXdsBuild = this.Project.XDSBuild;

            try
            {
                bool useNewXdsFormat = this.Project.XDSVersion != null && this.Project.XDSVersion.CompareTo("1.2.3") > 0;
                if (useNewXdsFormat)
                {
                    this.Project.Stores = null;
                    this.Project.Components = null;
                }
                else
                {
                    this.Project.TopInstanceFileMap = null;
                    this.Project.ExpandedState = null;
                    this.Project.ViewOrderMap = null;
                    this.Project.XDSBuild = null;
                }

                // Genera el archivo principal.
                this.SerializaFile(this.Project, Path.ChangeExtension(projectFile, "xds"));
                if (useNewXdsFormat)
                {
                    if (directoryPath != null)
                    {
                        string metadataDirectory = Path.Combine(directoryPath, "metadata");

                        // Directory.CreateDirectory (metadataDirectory);
                        this.CreateDirectoryIfNotExists(metadataDirectory);

                        string appDirectory = Path.Combine(directoryPath, "app");

                        // Directory.CreateDirectory (appDirectory);
                        this.CreateDirectoryIfNotExists(appDirectory);

                        string resourceDirectory = Path.Combine(directoryPath, "resource");

                        // Directory.CreateDirectory (resourceDirectory);
                        this.CreateDirectoryIfNotExists(resourceDirectory);

                        this.SerializaFile(this.Application, Path.Combine(metadataDirectory, "Application"));

                        if (this.Architect != null)
                        {
                            this.SerializaFile(this.Architect, Path.Combine(directoryPath, ".architect"));
                        }

                        foreach (KeyValuePair<string, Fileinfo> item in this.Project.TopInstanceFileMap)
                        {
                            int idx = 0, len = item.Value.Paths.Count;
                            while (idx < len)
                            {
                                string filePath = item.Value.Paths[idx];
                                this.OnMessage(filePath);
                                string ext = Path.GetExtension(filePath);
                                if (!filePath.Contains("/override/"))
                                {
                                    string fixFilePath = this.FixFilePath(
                                        filePath,
                                        metadataDirectory,
                                        appDirectory,
                                        resourceDirectory);

                                    // Directory.CreateDirectory (Path.GetDirectoryName (fixFilePath));
                                    this.CreateDirectoryIfNotExists(Path.GetDirectoryName(fixFilePath));
                                    if (!string.IsNullOrWhiteSpace(ext) || ext == "js")
                                    {
                                        using (StreamWriter sw = new StreamWriter(fixFilePath, false))
                                        {
                                            sw.Write(this.ComponentsFiles[item.Key][filePath]);
                                        }
                                    }
                                    else
                                    {
                                        this.OnMessage("K: {0} P: {1} F: {2}", item.Key, filePath, fixFilePath);

                                        // SerializaFile(Components.Where(c => c.DesignerId == item.Key).FirstOrDefault(), fixFilePath);
                                        this.SerializaFile(
                                            this.Components.FirstOrDefault(c => c.DesignerId == item.Key),
                                            fixFilePath);
                                    }
                                }

                                idx++;
                            }
                        }
                    }
                }
            }
            finally
            {
                this.Project.Stores = localStores;
                this.Project.Components = localComponents;
                this.Project.TopInstanceFileMap = localTopInstanceFileMap;
                this.Project.ExpandedState = localExpandedState;
                this.Project.ViewOrderMap = localViewOrderMap;
                this.Project.XDSBuild = localXdsBuild;
            }

            return this;
        }

        /// <summary>The create directory if not exists.</summary>
        /// <param name="path">The path.</param>
        protected virtual void CreateDirectoryIfNotExists(string path)
        {
            // http://gallery.technet.microsoft.com/scriptcenter/DelimonWin32IO-Library-V40-7ff6b16c
            // http://gallery.technet.microsoft.com/DelimonWin32IO-Library-V40-7ff6b16c
            // http://gallery.technet.microsoft.com/Delimon-Win32-Explorer-V40-bc957ab4
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>The deserialize file.</summary>
        /// <param name="filePath">The file path.</param>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns>The <see cref="TEntity"/>.</returns>
        protected virtual TEntity DeserializeFile<TEntity>(string filePath)
        {
            Encoding encoding;
            return this.DeserializeFile<TEntity>(filePath, out encoding);
        }

        /// <summary>Deserializes the file.</summary>
        /// <returns>The file.</returns>
        /// <param name="filePath">File path.</param>
        /// <param name="encoding">The encoding.</param>
        /// <typeparam name="TEntity">The 1st type parameter.</typeparam>
        protected virtual TEntity DeserializeFile<TEntity>(string filePath, out Encoding encoding)
        {
            if (filePath == null)
            {
                throw new ArgumentNullException("filePath");
            }

            TEntity ret;
            using (StreamReader sr = new StreamReader(filePath, true))
            {
                string content = sr.ReadToEnd();
                encoding = sr.CurrentEncoding;
                ret = JsonConvert.DeserializeObject<TEntity>(content);
            }

            return ret;
        }

        /// <summary>The fix file path.</summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="metadataDirectory">The metadata directory.</param>
        /// <param name="appDirectory">The app directory.</param>
        /// <param name="resourceDirectory">The resource directory.</param>
        /// <returns>The <see cref="string"/>.</returns>
        protected virtual string FixFilePath(
            string filePath,
            string metadataDirectory = null,
            string appDirectory = null,
            string resourceDirectory = null)
        {
            string md = string.Format("{0}/", metadataDirectory ?? this.MetadataDirectory),
                   ad = string.Format("{0}/", appDirectory ?? this.AppDirectory),
                   rd = string.Format("{0}/", resourceDirectory ?? this.ResourceDirectory);
            return filePath.Replace("metadata/", md)
                .Replace("app/", ad)
                .Replace("resources/", rd)
                .Replace('/', Path.DirectorySeparatorChar);
        }

        /// <summary>Gets the component file.</summary>
        /// <returns>The component file.</returns>
        /// <param name="paths">Paths.</param>
        /// <param name="fixPath">The fix Path.</param>
        protected virtual string GetComponentFile(IList<string> paths, bool fixPath = true)
        {
            string ret = null;

            if (paths == null)
            {
                throw new ArgumentNullException("paths");
            }

            int idx = 0, len = paths.Count;
            bool ok = false;
            while (!ok && idx < len)
            {
                string ext = Path.GetExtension(paths[idx]);
                if (!(ok = string.IsNullOrWhiteSpace(ext) || ext != "js"))
                {
                    idx++;
                }
            }

            if (ok)
            {
                if (fixPath)
                {
                    ret = this.FixFilePath(paths[idx]);
                }
                else
                {
                    ret = paths[idx];
                }
            }

            return ret;
        }

        /// <summary>
        /// Loads the components.
        /// </summary>
        /// <returns>
        /// The components.
        /// </returns>
        protected virtual EnumerableId<Component> LoadComponents()
        {
            EnumerableId<Component> components = new EnumerableId<Component>();
            foreach (KeyValuePair<string, Fileinfo> item in this.Project.TopInstanceFileMap)
            {
                string componentFile = this.GetComponentFile(item.Value.Paths);
                if (string.IsNullOrWhiteSpace(componentFile))
                {
                    throw new Exception(
                        string.Format(
                            "Can't get component path for the instance file map: {0} in the project file {1}",
                            item.Key,
                            this.ProjectFile));
                }

                this.OnMessage("K: {0} F: {1}", item.Key, componentFile);
                Component component = this.DeserializeFile<Component>(componentFile);
                components.Add(component);
            }

            return components;
        }

        /// <summary>The load components files.</summary>
        /// <returns>The <see cref="IDictionary"/>.</returns>
        protected virtual IDictionary<string, IDictionary<string, string>> LoadComponentsFiles()
        {
            IDictionary<string, IDictionary<string, string>> componentsFiles =
                new Dictionary<string, IDictionary<string, string>>(this.Project.TopInstanceFileMap.Count);
            foreach (KeyValuePair<string, Fileinfo> item in this.Project.TopInstanceFileMap)
            {
                int idx = 0, len = item.Value.Paths.Count;
                componentsFiles[item.Key] = new Dictionary<string, string>(len);
                while (idx < len)
                {
                    string filePath = item.Value.Paths[idx];
                    string ext = Path.GetExtension(filePath);
                    if (!filePath.Contains("/override/") && (!string.IsNullOrWhiteSpace(ext) || ext == "js"))
                    {
                        using (StreamReader sr = new StreamReader(this.FixFilePath(filePath), true))
                        {
                            string content = sr.ReadToEnd();
                            componentsFiles[item.Key][filePath] = content;
                        }
                    }

                    idx++;
                }
            }

            return componentsFiles;
        }

        /// <summary>Raises the message event.</summary>
        /// <param name="e">E.</param>
        protected virtual void OnMessage(MessageEventArgs e)
        {
            if (e == null)
            {
                throw new ArgumentNullException("e");
            }

            Message handler = this.Message;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>The on message.</summary>
        /// <param name="message">The message.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void OnMessage(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            this.OnMessage(new MessageEventArgs(message));
        }

        /// <summary>The on message.</summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void OnMessage(string format, object[] args)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            OnMessage(string.Format(format, args));
        }

        /// <summary>The on message.</summary>
        /// <param name="format">The format.</param>
        /// <param name="arg0">The arg 0.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void OnMessage(string format, object arg0)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            OnMessage(string.Format(format, arg0));
        }

        /// <summary>The on message.</summary>
        /// <param name="format">The format.</param>
        /// <param name="arg0">The arg 0.</param>
        /// <param name="arg1">The arg 1.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void OnMessage(string format, object arg0, object arg1)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            OnMessage(string.Format(format, arg0, arg1));
        }

        /// <summary>The on message.</summary>
        /// <param name="format">The format.</param>
        /// <param name="arg0">The arg 0.</param>
        /// <param name="arg1">The arg 1.</param>
        /// <param name="arg2">The arg 2.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void OnMessage(string format, object arg0, object arg1, object arg2)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            OnMessage(string.Format(format, arg0, arg1, arg2));
        }

        /// <summary>The on message.</summary>
        /// <param name="provider">The provider.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void OnMessage(IFormatProvider provider, string format, object[] args)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            OnMessage(string.Format(provider, format, args));
        }

        /// <summary>The serializa file.</summary>
        /// <param name="entity">The entity.</param>
        /// <param name="filePath">The file path.</param>
        protected virtual void SerializaFile(object entity, string filePath)
        {
            this.SerializaFile(entity, filePath, this.ProjectEncoding ?? Encoding.Default);
        }

        /// <summary>The serializa file.</summary>
        /// <param name="entity">The entity.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="encoding">The encoding.</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void SerializaFile(object entity, string filePath, Encoding encoding)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if (filePath == null)
            {
                throw new ArgumentNullException("filePath");
            }

            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }

            using (StreamWriter sw = new StreamWriter(filePath, false, encoding))
            {
                JsonSerializerSettings localSettings = this.SerializerSettings;

                // string content = JsonConvert.SerializeObject(entity, settings);
                string content = this.SerializeObject(entity, localSettings);
                byte[] buffer = encoding.GetBytes(content);
                byte[] nlBuf = encoding.GetBytes(Environment.NewLine);
                byte[] nlUnix = { 0x0A };
                byte[] newBuf = buffer.ReplaceAll(nlBuf, nlUnix);
                sw.BaseStream.Write(newBuf, 0, newBuf.Length);
            }
        }

        /// <summary>The serialize object.</summary>
        /// <param name="value">The value.</param>
        /// <param name="jsonSettings">The json settings.</param>
        /// <returns>The <see cref="string"/>.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual string SerializeObject(object value, JsonSerializerSettings jsonSettings)
        {
            if (jsonSettings == null)
            {
                throw new ArgumentNullException("settings");
            }

            JsonSerializer jsonSerializer = JsonSerializer.Create(jsonSettings);
            StringBuilder sb = new StringBuilder(256);
            StringWriter sw = new StringWriter(sb, CultureInfo.InvariantCulture);
            using (JsonTextWriter jsonWriter = new JsonTextWriter(sw))
            {
                jsonWriter.Formatting = Formatting.Indented;
                jsonWriter.Indentation = 4;
                jsonSerializer.Serialize(jsonWriter, value);
            }

            return sw.ToString();
        }

        /// <summary>Update the specified projectFile.</summary>
        /// <param name="projectFile">Project file.</param>
        /// <returns>The <see cref="ProjectBuilder"/>.</returns>
        protected virtual ProjectBuilder Update(string projectFile)
        {
            return this;
        }

        #endregion
    }
}