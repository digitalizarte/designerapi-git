
namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;

	///<summary>
	/// Box component.
	///</summary>
	public class BoxComponent : Component, IBoxComponent
	{
		public BoxComponent() : base()
		{
            Components = new EnumerableId<Component>();
            Reference = new Reference();
			Reference.Name = "items";
			Reference.Type = "array";
			UserConfig = new UserConfig();
			Id = IdManager.GetNextId();
			// TODO: Antes de grabar modificar todos los Id para que sean secuenciales.
			UserConfig ["designer|userClassName"] = String.Format("My{0}", GetType().Name);
		}
	}
}