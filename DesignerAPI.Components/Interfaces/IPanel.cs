
namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;

	public interface IPanel: IAbstractComponent, IContainer
	{
	}    
}
