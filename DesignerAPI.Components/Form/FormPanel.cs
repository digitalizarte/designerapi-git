
namespace DesignerAPI.Components.Form
{
	using System;
	using DesignerAPI.Model;

	public class FormPanel : Panel, IFormPanel, IBasicForm, IStateful
	{
		public FormPanel() : base()
		{
			Type = "form";
		}
	}
}
