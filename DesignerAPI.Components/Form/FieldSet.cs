
namespace DesignerAPI.Components.Form
{
    using System;
	using DesignerAPI.Model;

	public class FieldSet : Panel, IFieldSet, IStateful
	{
		public FieldSet():base()
		{
			Type = "fieldset";
			UserConfig ["title"] = "My Fields";
		}
	}
}
