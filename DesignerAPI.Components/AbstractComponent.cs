
namespace DesignerAPI.Components
{
	using System;
    using DesignerAPI.Model;

    public abstract class AbstractComponent: Component, IAbstractComponent
    {
        public AbstractComponent() :base()
        {
            Id = IdManager.GetNextId();
        }
    }

}
