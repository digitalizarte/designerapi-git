
namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;

	///<summary>
	/// Abstract panel.
	///</summary>
	public abstract class AbstractPanel : Container, IStateful
	{
	}
}
