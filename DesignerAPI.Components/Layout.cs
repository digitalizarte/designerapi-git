namespace DesignerAPI.Components
{
	///<summary>
	/// Layout.
	///</summary>
	public enum Layout
	{
		///<summary>
		/// The absolute.
		///</summary>
		Absolute,
		///<summary>
		/// The acordion.
		///</summary>
		Acordion,
		///<summary>
		/// The anchor.
		///</summary>
		Anchor,
		///<summary>
		/// The auto.
		///</summary>
		Auto,
		///<summary>
		/// The border.
		///</summary>
		Border,
		///<summary>
		/// The card.
		///</summary>
		Card,
		///<summary>
		/// The column.
		///</summary>
		Column,
		///<summary>
		/// The fit.
		///</summary>
		Fit,
		///<summary>
		/// The H box.
		///</summary>
		HBox,
		///<summary>
		/// The table.
		///</summary>
		Table,
		///<summary>
		/// The V box.
		///</summary>
		VBox,
        ///<summary>
        /// The form.
        ///</summary>
        Form
	}

}
