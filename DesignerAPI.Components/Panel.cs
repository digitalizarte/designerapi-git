
namespace DesignerAPI.Components
{
    using System;
	using DesignerAPI.Model;

	///<summary>
	/// Panel.
	///</summary>
	public class Panel : AbstractPanel, IPanel, IStateful
	{
		public Panel() : base()
		{
			Type = "panel";
			UserConfig ["height"] = 250;
			UserConfig ["width"] = 400;
			UserConfig ["title"] = "My Panel";
		}
	}
}
