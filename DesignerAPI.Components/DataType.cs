namespace DesignerAPI.Components
{
		using System;
	public enum DataType
	{		
		Auto,
		String,
		Int,
		Float,
		Boolean,
		Date
	}
}
