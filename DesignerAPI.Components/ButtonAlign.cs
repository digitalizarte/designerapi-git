
namespace DesignerAPI.Components
{

	using System;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	
	using System.Collections.Generic;

	public enum ButtonAlign
	{
		Right,
		Left,
		Center
	}
}
