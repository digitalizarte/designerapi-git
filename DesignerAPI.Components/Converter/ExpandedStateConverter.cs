
namespace DesignerAPI.Builder
{
    using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using System.Linq;

	using DesignerAPI.Model;
	using Newtonsoft.Json;
	using Newtonsoft.Json.Converters;

	internal class ExpandedStateConverter : JsonConverter
	{
        #region implemented abstract members of JsonConverter
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			/*
			Console.Error.WriteLine();
			Console.Error.WriteLine("ExpandedStateConverter");
			Console.Error.WriteLine();
            */  

			ExpandedState val = value as ExpandedState;

			writer.WriteStartObject();
			writer.WritePropertyName("data");
			writer.WriteValue(val.Data);
			writer.WritePropertyName("model");
			writer.WriteValue(val.Model);
			writer.WritePropertyName("component");
			writer.WriteValue(val.Component);
			writer.WritePropertyName("controller");
			writer.WriteValue(val.Controller);
			writer.WritePropertyName("resource");
			writer.WriteValue(val.Resource);
			foreach (var item in val)
			{
				writer.WritePropertyName(item.Key);
				serializer.Serialize(writer, item.Value);
			}
            
			writer.WriteEndObject();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotSupportedException("ExpandedStateConverter should only be used while serializing.");
		}

		public override bool CanConvert(Type objectType)
		{
			// return typeof(ExpandedState).IsAssignableFrom (objectType);
			return typeof(ExpandedState).Equals(objectType);
		}
        #endregion

		public override bool CanRead
		{
			get
			{
				return false;
			}
		}
	}
}
