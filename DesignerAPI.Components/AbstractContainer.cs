
namespace DesignerAPI.Components
{
	using System;
	using DesignerAPI.Model;

	///<summary>
	/// Abstract container.
	///</summary>
	public abstract class AbstractContainer : BoxComponent, IAbstractContainer, IStateful
	{
	}
}
