
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;
	
	public class BooleanField : DataField , IBooleanField
	{
		public BooleanField():base()
		{
			UserConfig ["type"] = "boolean";
		}
	}
}
