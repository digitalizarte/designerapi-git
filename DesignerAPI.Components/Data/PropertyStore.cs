
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class PropertyStore: StoreBase, IPropertyStore
	{
		public PropertyStore():base()
		{
			Type = "propertystore";
		}
	}
}
