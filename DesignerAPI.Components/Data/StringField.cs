
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class StringField : DataField , IStringField
	{
		public StringField():base()
		{
			UserConfig ["type"] = "string";
		}
	}
			
}
