
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class GroupingStore: StoreBase , IGroupingStore
	{
		public GroupingStore():base()
		{
			Type = "groupingstore";
		}
	}
}
