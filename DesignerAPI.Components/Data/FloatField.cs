
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class FloatField : DataField, IFloatField
	{
		public FloatField():base()
		{
			UserConfig ["type"] = "float";
		}
	}
}
