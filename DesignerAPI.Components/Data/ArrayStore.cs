
namespace DesignerAPI.Components.Data
{
    using System;
	using DesignerAPI.Model;
        
	public class ArrayStore: StoreBase , IArrayStore
	{
		public ArrayStore():base()
		{
			Type = "arraystore";
		}
	}
}
