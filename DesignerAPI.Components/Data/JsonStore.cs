
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class JsonStore: StoreBase , IJsonStore
	{
		public JsonStore():base()
		{
			Type = "jsonstore";
		}
	}
}
