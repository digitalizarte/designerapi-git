
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class StoreBase: Store
	{
		public StoreBase()
		{
			Reference = new Reference();
			Reference.Name = "items";
			Reference.Type = "array";
			UserConfig = new UserConfig();
			Id = IdManager.GetNextId();
		}
	}	
}
