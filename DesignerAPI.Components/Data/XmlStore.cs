
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;


	public class XmlStore: StoreBase , IXmlStore
	{
		public XmlStore():base()
		{
			Type = "xmlstore";
		}
	}
}
