
namespace DesignerAPI.Components.Data
{
	using System;
	using DesignerAPI.Model;

	public class DirectStore: StoreBase , IDirectStore
	{
		public DirectStore():base()
		{
			Type = "directstore";
		}
	}
}
