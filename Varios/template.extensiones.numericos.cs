#region MaxWidth

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, int maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, int maxWidth)
	        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, uint maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

[CLSCompliantAttribute(false)]
public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, uint maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, long maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, long maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

[CLSCompliantAttribute(false)]
public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, ulong maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

[CLSCompliantAttribute(false)]
public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, ulong maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, short maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, short maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

[CLSCompliantAttribute(false)]
public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, ushort maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

[CLSCompliantAttribute(false)]
public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, ushort maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, decimal maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, decimal maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, float maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, float maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static IAbstractComponent SetMaxWidth (this IAbstractComponent abstractComponent, double maxWidth)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	abstractComponent.UserConfig [" maxwidth"] =  maxWidth;

	return abstractComponent;
}

public static TEntity SetMaxWidth <TEntity>(this IAbstractComponent abstractComponent, double maxWidth)
        where TEntity : IAbstractComponent
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");
	return (TEntity)abstractComponent.SetMaxWidth(maxWidth);
}

public static Variant GetX (this IAbstractComponent abstractComponent)
{
	if (abstractComponent == null)
		throw new ArgumentNullException("abstractComponent");

	if (abstractComponent.UserConfig.ContainsKey("maxWidth");)
		return new Variant(abstractComponent.UserConfig ["maxWidth"]);
	else
		return null;
}

#endregion MaxWidth