#region CollapseMode



#endregion CollapseMode

public static IPanel SetCollapseMode (this IPanel panel, string collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (String.IsNullOrEmpty (collapseMode) && panel.UserConfig.ContainsKey ("collapseMode"))
		panel.UserConfig.Remove ("collapseMode");
	else
		panel.UserConfig ["collapseMode"] = collapseMode;

	return panel;
}

public static TEntity SetCollapseMode<TEntity> (this IPanel panel, string collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static string GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return (string)panel.UserConfig ["collapseMode"];
	else
		return null;
}

public static IPanel SetCollapseMode (this IPanel panel, string[] collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (collapseMode == null)
		throw new ArgumentNullException ("collapseMode");

	panel.UserConfig ["collapseMode"] = collapseMode;

	return panel;
}

public static TEntity SetCollapseMode<TEntity>(this IPanel panel, string[] collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static string[] GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return (string[])panel.UserConfig ["collapseMode"];
	else
		return null;
}

public static IPanel SetCollapseMode (this IPanel panel, IList<string> collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (collapseMode == null)
		throw new ArgumentNullException ("collapseMode");

	panel.UserConfig ["collapseMode"] = collapseMode;

	return panel;
}

public static TEntity SetCollapseMode<TEntity> (this IPanel panel, IList<string> collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static IList<string> GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return (IList<string>)panel.UserConfig ["collapseMode"];
	else
		return null;
}

#region CollapseMode

public static IPanel SetCollapseMode (this IPanel panel, bool collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	panel.UserConfig ["collapseMode"] = collapseMode;

	return panel;
}

public static TEntity SetCollapseMode<TEntity>(this IPanel panel, bool collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static bool GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return (bool)panel.UserConfig ["collapseMode"];
	else
		return false;
}

#endregion CollapseMode

public static IPanel SetCollapseMode (this IPanel panel, object collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (collapseMode == null && panel.UserConfig.ContainsKey ("collapseMode"))
		panel.UserConfig.Remove ("collapseMode");
	else
		panel.UserConfig ["collapseMode"] = collapseMode;

	return panel;
}

public static TEntity SetCollapseMode<TEntity>(this IPanel panel, object collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static object GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return panel.UserConfig ["collapseMode"];
	else
		return null;
}

public static Variant GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return new Variant(panel.UserConfig ["collapseMode"]);
	else
		return null;
}

public static IPanel SetCollapseMode (this IPanel panel, IList<string> collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (collapseMode == null) {
		if (panel.UserConfig.ContainsKey ("collapseMode")) {
			panel.UserConfig.Remove ("collapseMode");
		} else {
			throw new ArgumentNullException ("collapseMode");
		}
	} else {
		panel.UserConfig ["collapseMode"] = new List<string> (collapseMode);
	}

	return panel;
}


public static TEntity SetCollapseMode<TEntity>(this IPanel panel, IList<string> collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static IList<string> GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return (string)panel.UserConfig ["collapseMode"];
	else
		return null;
}

 #region CollapseMode

public static IPanel SetCollapseMode (this IPanel panel, BorderLayout collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	string collapseMode = Enum.GetName(typeof(CollapseMode ), collapseMode).ToUpperInvariant ();
	panel.UserConfig ["collapseMode"] = collapseMode;

	return panel;
}

public static TEntity SetCollapseMode<TEntity>(this IPanel panel, BorderLayout collapseMode)
{

	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static BorderLayout GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException ("panel");

	if (panel.UserConfig.ContainsKey ("collapseMode"))
		return (BorderLayout )Enum.Parse (typeof(BorderLayout), (string)panel.UserConfig ["collapseMode"], true);
	else
		return BorderLayout.Center;
}

#endregion CollapseMode


#region CollapseMode

public static IPanel SetCollapseMode (this IPanel panel, string collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException("panel");

	if (String.IsNullOrEmpty(collapseMode) && panel.UserConfig.ContainsKey("collapseMode"))
		panel.UserConfig.Remove("collapseMode");
	else
	{
		if (collapseMode.IsValidPattern())
			panel.UserConfig ["collapseMode"] = collapseMode;
		else
			throw new ArgumentNullException("collapseMode");
	}

	return panel;
}

public static TEntity SetCollapseMode<TEntity> (this IPanel panel, string collapseMode)
{

	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static IPanel SetCollapseMode (this IPanel panel, Regex collapseMode)
{
	if (panel == null)
		throw new ArgumentNullException("panel");

	if (collapseMode == null)
		throw new ArgumentNullException("collapseMode");

	panel.UserConfig ["collapseMode"] = collapseMode.ToString();

	return panel;
}

public static TEntity SetCollapseMode<TEntity>(this IPanel panel, Regex collapseMode)
{
	return (TEntity)panel.SetCollapseMode(collapseMode);
}

public static Regex GetCollapseMode (this IPanel panel)
{
	if (panel == null)
		throw new ArgumentNullException("panel");

	if (panel.UserConfig.ContainsKey("collapseMode"))
		return new Regex((string)panel.UserConfig ["collapseMode"], RegexOptions.Compiled);
	else
		return null;
}

#endregion CollapseMode