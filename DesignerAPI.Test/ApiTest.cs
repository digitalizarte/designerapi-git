//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
namespace DesignerAPI.Test
{
    using NUnit.Framework;
	using System;
	using System.Linq;
	using DesignerAPI.Builder;
	using DesignerAPI.Model;
	using DesignerAPI.Components;
	using System.IO;
	using System.Collections.Generic;
	using DesignerAPI.Components.Form;

	[Category("Api")]
	[TestFixture( )]
	public class ApiTest :FilesCompareBase
	{
		string basePath = @"C:\Users\Damian\Dropbox\TrabajosLocal\DotNetProjects\DesignerAPI\Examples\Ext Designer";
		// string basePath = @"C:\Users\Damian\Dropbox\TrabajosLocal\DotNetProjects\DesignerAPI\Examples\Ext Designer";
		// string basePath = @"C:\Users\damiane\Dropbox\Trabajos\DesignerAPI\Examples\Ext Designer";
		// string basePath = @"/home/damian/Trabajos/DotNetProjects/DesignerAPI/Examples/Ext Designer";

		[Test()]
		public void Generate_project_test()
		{
			ProjectBuilder pjt = new ProjectBuilder();
			pjt.Load(Path.Combine(basePath, "Window", "layout.xds"));
			ConfigComponents(pjt.Components);
			pjt.Save(Path.Combine(basePath, "Window", "layout2.xds"));
		}

		[Test()]
		public void Convert_old_projects_test()
		{
			// var basePath = @"C:\Users\Damian\Dropbox\TrabajosLocal\DotNetProjects\DesignerAPI\Examples\Ext Designer";
			var dirs = Directory.EnumerateDirectories(basePath);            
			foreach (var dir in dirs)
			{
				var basePathFile = Path.Combine(basePath, dir);
				var files = Directory.EnumerateFiles(basePathFile);

				foreach (var file in files)
				{
					var filePath = Path.Combine(basePathFile, file);                    
					if (!Path.GetFileNameWithoutExtension(file).EndsWith("2"))
					{
						ProjectBuilder pjt = new ProjectBuilder();
						Console.Error.WriteLine("Load {0}", filePath);
						pjt.Load(filePath);
						ConfigComponents(pjt.Components);
						var newFileName = Path.ChangeExtension(Path.GetFileNameWithoutExtension(file) + "2", "xds");
						pjt.Save(Path.Combine(basePathFile, newFileName));
					} else
					{
						Console.Error.WriteLine("Skip {0}", filePath);
					}
				}
			}
		}
        
		void ConfigComponents(EnumerableId<Component> components)
		{
			if (components != null)
			{
				foreach (var component in components)
				{
					if (String.IsNullOrWhiteSpace(component.Type))
					{
						component.Type = component.cId;
					}

					if (component.Reference == null)
					{
						component.Reference = new Reference();
						component.Reference.Name = "items";
						component.Reference.Type = "array";
					}
					ConfigComponents(component.Components);
				}
			}
		}

		[Test()]
		public void Create_project_from_api_test()
		{
			// Arrange
			var sut = new ProjectBuilder();
			sut.Message += (s, e) => Console.Error.WriteLine("ProjectBuilder say: {0}", e.Message);
			var projectName = "Form";
			var originalProjectPath = Path.Combine(basePath, projectName, "form2.xds");          
			string projectPath = Path.Combine(Path.GetTempPath(), projectName, "form2.xds");

			
			// Act
			Console.Error.WriteLine(@"Project Path: {0}\nNew Project Path: {1}", originalProjectPath, projectPath);            
			sut.AddFormPanel()
                    .SetTitle<IFormPanel>("Simple Form")
                    .SetWidth<IFormPanel>(350)
                    .SetLabelWidth<IFormPanel>(75)
                    .SetFrame<IFormPanel>(true)
                    .SetHeight<IFormPanel>(213)
                    .End<ProjectBuilder>();

			
			Console.WriteLine("New Project Path: {0}", projectPath);      
			Console.Error.WriteLine("New Project Path: {0}", projectPath);      
			sut.Save(projectPath);
            
			// Assert
			var diff = new List<string>();
			var same = new List<string>();
			Diff(diff, same, originalProjectPath, projectPath);
            
			Assert.AreEqual(0, diff.Count, "El proyecto original no es igual a grabado.");
			Assert.Fail();
		}       
	}   
}