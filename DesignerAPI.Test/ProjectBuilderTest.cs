namespace DesignerAPI.Test
{
	using NUnit.Framework;
	using System;
	using System.Linq;
	using DesignerAPI.Builder;
	using System.IO;
	using System.Collections.Generic;

	[Category("ProjectBuilder")]
	[TestFixture( )]
	public class ProjectBuilderTest : FilesCompareBase
	{
		// string basePath = @"C:\Users\Damian\Dropbox\TrabajosLocal\DotNetProjects\DesignerAPI\Examples\Sencha Architect\ext40";
		string basePath = @"C:\Users\Damian\Dropbox\TrabajosLocal\DotNetProjects\DesignerAPI\Examples\Sencha Architect\ext40";    
		// string basePath = @"C:\Users\Damian\Dropbox\TrabajosLocal\DotNetProjects\DesignerAPI\Examples\Sencha Architect\";
		// string basePath = @"C:\Users\damiane\Dropbox\Trabajos\DesignerAPI\Examples\Sencha Architect\ext40";
		// string basePath = @"/home/damian/Trabajos/DotNetProjects/DesignerAPI/Examples/Sencha Architect/ext40";
		
		// string projectName = "BookReviews";
		// string projectName = "area-chart";
		string projectName = "DesignerExample";

		string GetProjectPath (string projName = null)
		{
			string p1;
			var p = projName ?? projectName;
			if (p.EndsWith ("V2")) {
				p1 = p.Substring (0, p.Length - 2);
			} else {
				p1 = p;
			}

			return Path.Combine (basePath, p, String.Format (@"{0}.xds", p1));
		}

		[Test()]
		public void Save_project_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);
			var projectName = "BookReviewsV2";
			var originalProjectPath = GetProjectPath (projectName);
			sut.Load (originalProjectPath);
//			var key = sut.Project.TopInstanceFileMap.Keys.First ();
//			var ces = new DesignerAPI.Model.ComponentExpandedState ();
//			ces.Expanded = true;
//			ces.Id = key;
//			sut.Project.ExpandedState.Add (key, ces);
			//			Console.Error.WriteLine ("K: {0}, C: {1}", key, ces);
			Console.Error.WriteLine ("O: {0}, N: {1}", originalProjectPath, Path.GetTempPath ());
			// string projectPath = Path.Combine (Path.GetTempPath (), projectName, String.Format (@"{0}.xds", projectName));
			string projectName1;
			if (projectName.EndsWith ("V2", StringComparison.InvariantCultureIgnoreCase)) {
				projectName1 = projectName.Substring (0, projectName.Length - 2);
			} else {
				projectName1 = projectName;
			}
			string projectPath = Path.Combine (Path.GetTempPath (), projectName, String.Format (@"{0}.xds", projectName1));

			// Act
			Console.WriteLine ("New Project Path: {0}", projectPath);
			Console.Error.WriteLine ("New Project Path: {0}", projectPath);
			Console.Error.WriteLine (sut.ProjectEncoding);
			sut.Save (projectPath);
			
			// Assert
			var diff = new List<string> ();
			var same = new List<string> ();
			Diff (diff, same, originalProjectPath, projectPath);

			Assert.AreEqual (0, diff.Count, "El proyecto original no es igual a grabado.");
		}

		[Test()]
		public void Load_file_project_is_not_null_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Act
			sut.Load (GetProjectPath ());

			// Assert
			Assert.IsNotNull (sut.Project, "El objeto es null");
		}

		[Test()]
		public void Load_file_application_is_not_null_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Act
			sut.Load (GetProjectPath ("BookReviews"));

			// Assert
			Assert.IsNotNull (sut.Application, "El objeto es null");
		}

		[Test()]
		public void Load_file_components_is_not_null_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Act
			sut.Load (GetProjectPath ());

			// Assert
			Assert.IsNotNull (sut.Components, "El objeto es null");
		}

		[Test()]
		public void Load_file_metadata_directory_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);
			var projectName = "BookReviews";

			// Act
			sut.Load (GetProjectPath (projectName));

			// Assert
			Assert.AreEqual (Path.Combine (basePath, projectName, "metadata"), sut.MetadataDirectory);
		}

		[Test()]
		public void Load_file_components_count_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Act
			sut.Load (GetProjectPath ("BookReviews"));

			// Assert
			Assert.AreEqual (11, sut.Components.Count);
		}

		[Test()]
		public void Create_project_builder_project_is_null_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Assert
			Assert.IsNull (sut.Project, "El proyecto debe ser null.");
		}

		[Test()]
		public void Create_project_builder_application_is_null_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Assert
			Assert.IsNull (sut.Application, "La aplicacion debe ser null.");
		}

		[Test()]
		public void Create_project_builder_components_is_null_test ()
		{
			// Arrange
			var sut = new ProjectBuilder ();
			sut.Message += (s, e) => Console.Error.WriteLine ("ProjectBuilder say: {0}", e.Message);

			// Assert
			Assert.IsNull (sut.Components, "Los componentes debe ser null.");
		}
	}
}