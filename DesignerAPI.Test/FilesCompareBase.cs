//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Test
{
	using NUnit.Framework;
	using System;
	using System.Linq;
	using DesignerAPI.Builder;
	using System.IO;
	using System.Collections.Generic;

	public class FilesCompareBase
	{        
		public void Diff (List<string> diff, List<string> same, string first, string second)
		{
			if (File.Exists (first)) {
				if (AreFileEqual (first, second)) {
					same.Add (first);
				} else {
					diff.Add (first);
				}
			} else if (Directory.Exists (first)) {
				foreach (var file in Directory.GetFiles(first)) {
					Diff (diff, same, file, Path.Combine (second, Path.GetFileName (file)));
				}
				foreach (var sub in Directory.GetDirectories(first)) {
					Diff (diff, same, sub, Path.Combine (second, Path.GetFileName (sub)));
				}
			}
		}
        
		public bool AreFileEqual (string first, string second)
		{
			if (first == null)
				throw new ArgumentNullException ("first");
            
			if (second == null || !File.Exists (second)) {
				return false;
			}
            
			using (var firstStream = File.OpenRead(first)) {
				using (var secondStream = File.OpenRead(second)) {
					byte[] firstBuf = new byte[1024 * 1024];
					byte[] secondBuf = new byte[1024 * 1024];
					while (true) {
						var firstBytes = firstStream.Read (firstBuf, 0, firstBuf.Length);
						var secondBytes = secondStream.Read (secondBuf, 0, secondBuf.Length);
						if (firstBytes != secondBytes) {
							return false;
						}
						if (firstBytes == 0) {
							break;
						}
                        
						for (var i = 0; i < firstBytes; i++) {
							if (firstBuf [i] != secondBuf [i]) {
								return false;
							}
						}
					}
				}
			}           
			return true;
		}
        
		public bool CompareDirectories (string pathA, string pathB)
		{
			bool ret = false;
			var dirInfo1 = new DirectoryInfo (pathA);
			var dirInfo2 = new DirectoryInfo (pathB);
            
			var q = from dirA in dirInfo1.GetDirectories ()
                from dirB in dirInfo2.GetDirectories ()
                    where dirA.Name == dirB.Name 
                    let filesA = dirA.GetFiles ()
                    let filesB = dirB.GetFiles ()
                    where filesA.Length == filesB.Length
                    from fileA in filesA
                    from fileB in filesB
                    where fileA.Name == fileB.Name
                    from fileAB in File.ReadAllBytes (fileA.FullName)
                    from fileBB in File.ReadAllBytes (fileB.FullName)
                    where fileAB != fileBB
                    select 1;
			ret = q.Any ();
			return ret;
		}
	}
}