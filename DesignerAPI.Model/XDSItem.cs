//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
    using System.Collections.Generic;
	using System.Runtime.Serialization;
	using Newtonsoft.Json;

	///<summary>
	/// XDS item.
	///</summary>
	[JsonObject]
	public class XDSItem : IXDSItem
	{
		public XDSItem()
		{
			Id = IdManager.GetNextId();
		}

        #region Properties

        #region IItemId implementation

		public string Id
		{
			get;
			set;
		}

        #endregion
		public virtual string Type
		{
			get;
			set;
		}

		public virtual Reference Reference
		{
			get;
			set;
		}

		public virtual string CodeClass
		{
			get;
			set;
		}

		public virtual UserConfig UserConfig
		{
			get;
			set;
		}

		public virtual IList<object> CustomConfigs
		{
			get;
			set;
		}

		public virtual string IconCls
		{
			get;
			set;
		}

		public virtual bool? Expanded
		{
			get;
			set;
		}
        
		public virtual string DesignerId
		{
			get;
			set;
		}

		public virtual string MasterInstanceId
		{
			get;
			set;
		}

		public virtual int? ViewOrder
		{
			get;
			set;
		}

		public virtual string cId { get; set; }

		public virtual string JsClass { get; set; }

		public virtual long? SnapToGrid { get; set; }

		public virtual string UserXType { get; set; }

		public virtual string UserComponent { get; set; }

        #endregion
	}
}
