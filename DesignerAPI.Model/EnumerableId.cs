//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System;
	using System.Collections.Generic;

	///<summary>
	/// Enumerable identifier.
	///</summary>
	public class EnumerableId<TEntity> :  Enumerable  <TEntity> 
		where TEntity : IItemId
	{
		///<summary>
		/// Initializes a new instance of the <see cref="DesignerAPI.Model.EnumerableId`1"/> class.
		///</summary>
		public EnumerableId()
		{
		}

		///<summary>
		/// Initializes a new instance of the <see cref="DesignerAPI.Model.EnumerableId`1"/> class.
		///</summary>
		/// <param name='list'>
		/// List.
		/// </param>
		public EnumerableId(IList<TEntity> list)
            : this()
		{
			if (list == null) 
				throw new ArgumentNullException("list");

			InternalList.ForEach(value =>
			{
				InternalMap [value.Id] = value;
				list.Add(value);
			});
		}

		///<summary>
		/// Add the specified value.
		///</summary>
		/// <param name='value'>
		/// Value.
		/// </param>
		public override void Add(TEntity value)
		{
			if (value == null)
				throw new ArgumentNullException("value");
			
			InternalMap [value.Id] = value;
			InternalList.Add(value);
		}

		///<summary>
		/// Insert the specified index and item.
		///</summary>
		/// <param name='index'>
		/// Index.
		/// </param>
		/// <param name='item'>
		/// Item.
		/// </param>
		public override void Insert(int index, TEntity item)
		{
			if (item == null)
				throw new ArgumentNullException("item");
			InternalList.Insert(index, item);
			InternalMap [item.Id] = item;
		}

		///<summary>
		/// Removes at index.
		///</summary>
		/// <param name='index'>
		/// Index.
		/// </param>
		public override void RemoveAt(int index)
		{
			TEntity value = InternalList [index];
			InternalList.Remove(value);
			InternalMap.Remove(value.Id);
		}

		///<summary>
		/// Remove the specified item.
		///</summary>
		/// <param name='item'>
		/// If set to <c>true</c> item.
		/// </param>
		public override bool Remove(TEntity item)
		{
			if (item == null)
				throw new ArgumentNullException("item");
			return InternalList.Remove(item) && InternalMap.Remove(item.Id);
		}
	}	
}