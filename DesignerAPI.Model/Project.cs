//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System.Runtime.Serialization;
	using Newtonsoft.Json;

	///<summary>
	/// Represente the Project file.
	///</summary>
	[JsonObject]
	public class Project: IProject
	{
		#region Contructor
		///<summary>
		/// Initializes a new instance of the <see cref="DesignerAPI.Model.Project"/> class.
		///</summary>
		public Project()
		{
			Settings = new Settings();
			// XDSVersion = "2.0.0";
			XDSVersion = "1.2.3";
			// XDSBuild = 401;
			// Framework = "ext40";
			Framework = "ext33";
			TopInstanceFileMap = new FileMap();
			ExpandedState = new ExpandedState();
		}

		#endregion 

		#region Properties

		public virtual string Name
		{
			get;
			set;
		}

		public virtual Settings Settings
		{ 
			get;
			set;
		}

		public virtual string XDSVersion
		{
			get;
			set;
		}

		public virtual int? XDSBuild
		{ 
			get;
			set;
		}

		public virtual string Framework
		{ 
			get;
			set;
		}

		public virtual FileMap TopInstanceFileMap
		{
			get;
			set;
		}

		public virtual ExpandedState ExpandedState
		{
			get;
			set;
		}

		public virtual int? SchemaVersion
		{
			get;
			set;
		}

		public virtual int? UpgradeVersion
		{
			get;
			set;
		}

		public virtual ViewOrderMap ViewOrderMap
		{
			get; 
			set;
		}

		public virtual string ProjectId
		{ 
			get; 
			set; 
		}

		public virtual EnumerableId<Store> Stores
		{ 
			get; 
			set;
		}


		#region IProject implementation

		public EnumerableId<Component> Components
		{
			get;
			set;
		}

		#endregion

		#endregion
	}
}