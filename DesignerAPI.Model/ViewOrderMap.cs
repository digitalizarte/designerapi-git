//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System.Collections.Generic;
	using System.Runtime.Serialization;
	using Newtonsoft.Json;


	///<summary>
	/// Represente the Project file.
	///</summary>
	public class ViewOrderMap: IViewOrderMap
	{
		#region Contructor
		///<summary>
		/// Initializes a new instance of the <see cref="DesignerAPI.Model.ViewOrderMap"/> class.
		///</summary>
		public ViewOrderMap ()
		{
			View = new List<string> ();
			Store = new List<string> ();
			Controller = new List<string> ();
			Model = new List<string> ();
			Resource = new List<string> ();
			App = new List<string> ();
		}

		#endregion

		#region Properties

		public virtual IList<string> View {
			get;
			set;
		}

		public virtual IList<string> Store {
			get;
			set;
		}

		public virtual IList<string> Controller {
			get;
			set;
		}

		public virtual IList<string> Model {
			get;
			set;
		}

		public virtual IList<string> Resource {
			get;
			set;
		}

		public virtual IList<string> App {
			get;
			set;
		}

		#endregion
	}
}

