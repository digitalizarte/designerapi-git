//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{

	using System;
	using System.Collections.Generic;
	using System.Collections;

	///<summary>
	/// Enumerable.
	///</summary>
	public abstract class Enumerable<TEntity> : IEnumerable<TEntity>, IEnumerable, IList<TEntity>, IList, IDisposable
	{
		protected readonly Dictionary<string, TEntity> InternalMap = new Dictionary<string, TEntity> ();
		protected readonly List<TEntity> InternalList = new List<TEntity> ();

		///<summary>
		/// Gets or sets the element at the specified index.
		///</summary>
		/// <returns>
		/// The element at the specified index.
		/// </returns>
		/// <param name='index'>
		/// The zero-based index of the element to get or set.
		/// </param>
		/// <param name='memberName'>
		/// Member name.
		/// </param>
		public virtual TEntity this [string memberName] {
			get {
				TEntity value;
				if (InternalMap.TryGetValue (memberName, out value))
					return value;
				else
					return default(TEntity);
			}
		}

		///<summary>
		/// Gets or sets the element at the specified index.
		///</summary>
		/// <returns>
		/// The element at the specified index.
		/// </returns>
		/// <param name='index'>
		/// The zero-based index of the element to get or set.
		/// </param>
		public virtual TEntity this [int index] {
			get {
				return InternalList [index];
			}
			set {
				return;
			}
		}

		///<summary>
		/// Clear this instance.
		///</summary>
		public virtual void Clear ()
		{
			InternalList.Clear ();
			InternalMap.Clear ();
		}

		///<summary>
		/// Gets the count.
		///</summary>
		/// <value>
		/// The count.
		/// </value>
		public virtual int Count {
			get {
				return InternalList.Count;
			}
		}

        #region IEnumerable<Project> Members
		///<summary>
		/// Returns an enumerator that iterates through a collection.
		///</summary>
		/// <returns>
		/// The enumerator.
		/// </returns>
		public virtual IEnumerator<TEntity> GetEnumerator ()
		{
			return InternalList.GetEnumerator ();
		}
        #endregion

        #region IEnumerable Members
		///<summary>
		/// Gets the enumerator.
		///</summary>
		/// <returns>
		/// The enumerator.
		/// </returns>
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return InternalList.GetEnumerator ();
		}
        #endregion

        #region IList<TEntity> Members
		///<summary>
		/// Indexs the of.
		///</summary>
		/// <returns>
		/// The of.
		/// </returns>
		/// <param name='item'>
		/// Item.
		/// </param>
		public virtual int IndexOf (TEntity item)
		{
			if (item == null)
				throw new ArgumentNullException ("item");

			return InternalList.IndexOf (item);
		}

		///<summary>
		/// Insert the specified index and item.
		///</summary>
		/// <param name='index'>
		/// Index.
		/// </param>
		/// <param name='item'>
		/// Item.
		/// </param>
		public abstract void Insert (int index, TEntity item);

		///<summary>
		/// Removes at index.
		///</summary>
		/// <param name='index'>
		/// Index.
		/// </param>
		public abstract void RemoveAt (int index);

        #endregion

        #region ICollection<TEntity> Members

		///<summary>
		/// Add the specified item.
		///</summary>
		/// <param name='item'>
		/// Item.
		/// </param>
		public abstract void Add (TEntity item);

		///<summary>
		/// Contains the specified item.
		///</summary>
		/// <param name='item'>
		/// If set to <c>true</c> item.
		/// </param>
		public virtual bool Contains (TEntity item)
		{
			if (item == null)
				throw new ArgumentNullException ("item");
			return InternalList.Contains (item);
		}

		///<summary>
		/// Copies to.
		///</summary>
		/// <param name='array'>
		/// Array.
		/// </param>
		/// <param name='arrayIndex'>
		/// Array index.
		/// </param>
		public virtual void CopyTo (TEntity[] array, int arrayIndex)
		{
			if (array == null)
				throw new ArgumentNullException ("array");
			InternalList.CopyTo (array, arrayIndex);
		}

		///<summary>
		/// Gets a value indicating whether this instance is read only.
		///</summary>
		/// <value>
		/// <c>true</c> if this instance is read only; otherwise, <c>false</c>.
		/// </value>
		public virtual bool IsReadOnly {
			get {
				return false;
			}
		}

		///<summary>
		/// Remove the specified item.
		///</summary>
		/// <param name='item'>
		/// If set to <c>true</c> item.
		/// </param>
		public abstract bool Remove (TEntity item);
        #endregion

        #region ICollection Members

		///<summary>
		/// Copies to.
		///</summary>
		/// <param name='array'>
		/// Array.
		/// </param>
		/// <param name='index'>
		/// Index.
		/// </param>
		public virtual void CopyTo (Array array, int index)
		{
			if (array == null)
				throw new ArgumentNullException ("array");

			CopyTo ((TEntity[])array, index);
		}

		///<summary>
		/// Gets a value indicating whether this instance is synchronized.
		///</summary>
		/// <value>
		/// <c>true</c> if this instance is synchronized; otherwise, <c>false</c>.
		/// </value>
		public virtual bool IsSynchronized {
			get {
				return true;
			}
		}

		///<summary>
		/// Gets the sync root.
		///</summary>
		/// <value>
		/// The sync root.
		/// </value>
		public virtual object SyncRoot {
			get {
				return null;
			}
		}

        #endregion

        #region IList Members

		///<summary>
		/// Add the specified value.
		///</summary>
		/// <param name='value'>
		/// Value.
		/// </param>
		public virtual int Add (object value)
		{
			if (value == null)
				throw new ArgumentNullException ("value");

			Add ((TEntity)value);

			return InternalList.Count - 1;
		}

		///<summary>
		/// Contains the specified value.
		///</summary>
		/// <param name='value'>
		/// If set to <c>true</c> value.
		/// </param>
		public virtual bool Contains (object value)
		{
			if (value == null)
				throw new ArgumentNullException ("value");

			return Contains ((TEntity)value);
		}

		///<summary>
		/// Indexs the of.
		///</summary>
		/// <returns>
		/// The of.
		/// </returns>
		/// <param name='value'>
		/// Value.
		/// </param>
		public virtual int IndexOf (object value)
		{
			if (value == null)
				throw new ArgumentNullException ("value");

			return IndexOf ((TEntity)value);
		}

		///<summary>
		/// Insert the specified index and value.
		///</summary>
		/// <param name='index'>
		/// Index.
		/// </param>
		/// <param name='value'>
		/// Value.
		/// </param>
		public virtual void Insert (int index, object value)
		{
			if (value == null)
				throw new ArgumentNullException ("value");

			Insert (index, (TEntity)value);
		}

		///<summary>
		/// Gets a value indicating whether this instance is fixed size.
		///</summary>
		/// <value>
		/// <c>true</c> if this instance is fixed size; otherwise, <c>false</c>.
		/// </value>
		public virtual bool IsFixedSize {
			get {
				return false;
			}
		}

		///<summary>
		/// Remove the specified value.
		///</summary>
		/// <param name='value'>
		/// Value.
		/// </param>
		public virtual void Remove (object value)
		{
			if (value == null)
				throw new ArgumentNullException ("value");

			Remove ((TEntity)value);
		}

		///<summary>
		/// Gets or sets the <see cref="DesignerAPI.Model.Enumerable`1"/> at the specified index.
		///</summary>
		/// <param name='index'>
		/// Index.
		/// </param>
		object IList.this [int index] {
			get {
				return this [index];
			}
			set {
				return;
			}
		}
        #endregion

        #region IDisposable Members

		///<summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		///</summary>
		/// <filterpriority>
		/// 2
		/// </filterpriority>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="DesignerAPI.Model.Enumerable`1"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="DesignerAPI.Model.Enumerable`1"/> in an unusable state.
		/// After calling <see cref="Dispose"/>, you must release all references to the
		/// <see cref="DesignerAPI.Model.Enumerable`1"/> so the garbage collector can reclaim the memory that the
		/// <see cref="DesignerAPI.Model.Enumerable`1"/> was occupying.
		/// </remarks>
		public virtual void Dispose ()
		{
			InternalMap.Clear ();
			InternalList.Clear ();
		}
        #endregion
	}
	
}
