namespace SenchaArchitect2.Model
{
	using System;
	using System.Collections.Generic;
	using System.Runtime.Serialization;
	using Newtonsoft.Json;

	public interface IApplication: IXDSItem
	{

		#region Properties
		
		/// <summary>
		/// Gets or sets the components.
		/// </summary>
		/// <value>
		/// The components.
		/// </value>
		[DataMember(Name="cn")]
		[JsonProperty("cn")]
		EnumerableId<Component>  Components {
			get;
			set;
		}
				#endregion 
	}	
}
