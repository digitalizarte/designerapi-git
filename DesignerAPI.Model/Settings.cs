//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System.Runtime.Serialization;
	using Newtonsoft.Json;


	public class Settings: ISettings
	{
		#region Contructor
		public Settings ()
		{
			UrlPrefix = "http://localhost/";
			SpacesToIndent = 4;
			CodeGenFormat = "Class";
			ExtPath = "http://extjs.cachefly.net/ext-4.0.2a/";
			SenchaIoPath = "http://extjs.cachefly.net/designer/IO/";
			LineEnding = "LF";
			InstantiateStore = true;
			AppName = "MyApp";
			MapsAPI = "http://maps.google.com/maps/api/js?sensor=true";
		}
		#endregion

		#region Properties

		public virtual string UrlPrefix {
			get;
			set;
		}

		public virtual string DirectAPI {
			get;
			set;
		}

		public virtual int SpacesToIndent {
			get;
			set;
		}

		public virtual string CodeGenFormat {
			get;
			set;
		}

		public virtual string ExportPath {
			get;
			set;
		}

		public virtual string ExtPath {
			get;
			set;
		}

		public virtual string SenchaIoKey {
			get;
			set;
		}

		public virtual string SenchaIoPath {
			get;
			set;
		}

		public virtual string LineEnding {
			get;
			set;
		}

		public virtual bool InstantiateStore {
			get;
			set;
		}

		public virtual bool GenTimestamps {
			get;
			set;
		}

		public virtual bool SaveOnDeploy {
			get;
			set;
		}

		public virtual string AppName {
			get;
			set;
		}

		public virtual string MapsAPI {
			get;
			set;
		}

		public virtual string CssPath {
			get;
			set;
		}

		public virtual bool? CacheBust {
			get;
			set;
		}

		#endregion
	}
}