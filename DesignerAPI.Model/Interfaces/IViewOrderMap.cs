//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System.Collections.Generic;
	using System.Runtime.Serialization;
	using Newtonsoft.Json;

	public interface IViewOrderMap
	{
		#region Properties
		///<summary>
		/// Gets or sets the view.
		///</summary>
		/// <value>
		/// The view.
		/// </value>
		[DataMember(Name="view")]
		[JsonProperty("view")]
		IList<string> View {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the store.
		///</summary>
		/// <value>
		/// The store.
		/// </value>
		[DataMember(Name="store")]
		[JsonProperty("store")]
		IList<string> Store {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the controller.
		///</summary>
		/// <value>
		/// The controller.
		/// </value>
		[DataMember(Name="controller")]
		[JsonProperty("controller")]
		IList<string> Controller {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the model.
		///</summary>
		/// <value>
		/// The model.
		/// </value>
		[DataMember(Name="model")]
		[JsonProperty("model")]
		IList<string> Model {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the resource.
		///</summary>
		/// <value>
		/// The resource.
		/// </value>
		[DataMember(Name="resource")]
		[JsonProperty("resource")]
		IList<string> Resource {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the app.
		///</summary>
		/// <value>
		/// The app.
		/// </value>
		[DataMember(Name="app")]
		[JsonProperty("app")]
		IList<string> App {
			get;
			set;
		}

        #endregion

	}	
}
