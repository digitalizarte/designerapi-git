//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
    using System.Runtime.Serialization;
    using Newtonsoft.Json;

    ///<summary>
    /// Represente the Project file.
    ///</summary>
    public interface IProject
    {
        #region Properties
        
        ///<summary>
        /// Gets or sets the name.
        ///</summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember(Name="name")]
        [JsonProperty("name")]
        string Name {
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the settings.
        ///</summary>
        /// <value>
        /// The settings.
        /// </value>
        [DataMember(Name="settings")]
        [JsonProperty("settings")]
        Settings Settings { 
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the XDS version.
        ///</summary>
        /// <value>
        /// The XDS version.
        /// </value>
        [DataMember(Name="xdsVersion")]
        [JsonProperty("xdsVersion")]
        string XDSVersion {
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the XDS build.
        ///</summary>
        /// <value>
        /// The XDS build.
        /// </value>
        [DataMember(Name="xdsBuild")]
        [JsonProperty("xdsBuild")]
        int? XDSBuild { 
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the framework.
        ///</summary>
        /// <value>
        /// The framework.
        /// </value>
        [DataMember(Name="framework")]
        [JsonProperty("framework")]
        string Framework { 
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the top instance file map.
        ///</summary>
        /// <value>
        /// The top instance file map.
        /// </value>
        [DataMember(Name="topInstanceFileMap")]
        [JsonProperty("topInstanceFileMap")]
        FileMap TopInstanceFileMap {
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the state of the expanded.
        ///</summary>
        /// <value>
        /// The state of the expanded.
        /// </value>
        [DataMember(Name="expandedState")]
        [JsonProperty("expandedState")]
        ExpandedState ExpandedState {
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the schema version.
        /// Para la version xdsVersion: 2.1.0 xdsBuild: 613.
        ///</summary>
        /// <value>
        /// The schema version.
        /// </value>
        [DataMember(Name="schemaVersion")]
        [JsonProperty("schemaVersion")]
        int? SchemaVersion {
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the upgrade version.
        /// Para la version xdsVersion: 2.1.0 xdsBuild: 613.
        ///</summary>
        /// <value>
        /// The upgrade version.
        /// </value>
        [DataMember(Name="upgradeVersion")]
        [JsonProperty("upgradeVersion")]
        int? UpgradeVersion {
            get;
            set;
        }
        
        ///<summary>
        /// Gets or sets the view order map.
        /// Para la version xdsVersion: 2.1.0 xdsBuild: 613.
        ///</summary>
        /// <value>
        /// The view order map.
        /// </value>
        [DataMember(Name="viewOrderMap")]
        [JsonProperty("viewOrderMap")]
        ViewOrderMap ViewOrderMap {
            get; 
            set;
        }

        [DataMember(Name="projectId")]
        [JsonProperty("projectId")]
        string ProjectId { get; set; }

        [DataMember(Name="stores")]
        [JsonProperty("stores")]
        EnumerableId<Store> Stores { 
            get;
            set;
        }

        [DataMember(Name="components")]
        [JsonProperty("components")]
        EnumerableId<Component> Components { 
            get;
            set;
        }
        
#endregion

    }
        
}
