//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System.Runtime.Serialization;
	using Newtonsoft.Json;

	public interface ISettings
	{

		#region Properties
		///<summary>
		/// Gets or sets the URL prefix.
		///</summary>
		/// <value>
		/// The URL prefix.
		/// </value>
		[DataMember(Name="urlPrefix")]
		[JsonProperty("urlPrefix")]
		string UrlPrefix
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the direct AP.
		///</summary>
		/// <value>
		/// The direct AP.
		/// </value>
		[DataMember(Name="directAPI")]
		[JsonProperty("directAPI")]
		string DirectAPI
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the spaces to indent.
		///</summary>
		/// <value>
		/// The spaces to indent.
		/// </value>
		[DataMember(Name="spacesToIndent")]
		[JsonProperty("spacesToIndent")]
		int SpacesToIndent
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the code gen format.
		///</summary>
		/// <value>
		/// The code gen format.
		/// </value>
		[DataMember(Name="codeGenFormat")]
		[JsonProperty("codeGenFormat")]
		string CodeGenFormat
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the export path.
		///</summary>
		/// <value>
		/// The export path.
		/// </value>
		[DataMember(Name="exportPath")]
		[JsonProperty("exportPath")]
		string ExportPath
		{
			get;
			set;
		}
        		
		///<summary>
		/// Gets or sets the ext path.
		///</summary>
		/// <value>
		/// The ext path.
		/// </value>
		[DataMember(Name="extPath")]
		[JsonProperty("extPath")]
		string ExtPath
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the sencha io key.
		///</summary>
		/// <value>
		/// The sencha io key.
		/// </value>
		[DataMember(Name="senchaIoKey")]
		[JsonProperty("senchaIoKey")]
		string SenchaIoKey
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the sencha io path.
		///</summary>
		/// <value>
		/// The sencha io path.
		/// </value>
		[DataMember(Name="senchaIoPath")]
		[JsonProperty("senchaIoPath")]
		string SenchaIoPath
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the line ending.
		///</summary>
		/// <value>
		/// The line ending.
		/// </value>
		[DataMember(Name="lineEnding")]
		[JsonProperty("lineEnding")]
		string LineEnding
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets a value indicating whether this <see cref="DesignerAPI.Model.Settings"/> instantiate store.
		///</summary>
		/// <value>
		/// <c>true</c> if instantiate store; otherwise, <c>false</c>.
		/// </value>
		[DataMember(Name="instantiateStore")]
		[JsonProperty("instantiateStore")]
		bool InstantiateStore
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets a value indicating whether this <see cref="DesignerAPI.Model.Settings"/> gen timestamps.
		///</summary>
		/// <value>
		/// <c>true</c> if gen timestamps; otherwise, <c>false</c>.
		/// </value>
		[DataMember(Name="genTimestamps")]
		[JsonProperty("genTimestamps")]
		bool GenTimestamps
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets a value indicating whether this <see cref="DesignerAPI.Model.Settings"/> save on deploy.
		///</summary>
		/// <value>
		/// <c>true</c> if save on deploy; otherwise, <c>false</c>.
		/// </value>
		[DataMember(Name="saveOnDeploy")]
		[JsonProperty("saveOnDeploy")]
		bool SaveOnDeploy
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the name of the app.
		///</summary>
		/// <value>
		/// The name of the app.
		/// </value>
		[DataMember(Name="appName")]
		[JsonProperty("appName")]
		string AppName
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the maps AP.
		///</summary>
		/// <value>
		/// The maps AP.
		/// </value>
		[DataMember(Name="mapsAPI")]
		[JsonProperty("mapsAPI")]
		string MapsAPI
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the css path.
		///</summary>
		/// <value>
		/// The css path.
		/// </value>
		[DataMember(Name="cssPath")]
		[JsonProperty("cssPath")]
		string CssPath
		{
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the cache bust.
		/// Para la version xdsVersion: 2.1.0 xdsBuild: 613.
		///</summary>
		/// <value>
		/// The cache bust.
		/// </value>
		[DataMember(Name="cacheBust")]
		[JsonProperty("cacheBust")]
		bool? CacheBust
		{
			get;
			set;
		}       
		
        #endregion

	}	
}