//
//  Author:
//    Damian Eiff damian@digitalizarte.com.ar
//
//  Copyright (c) 2012, Damian Eiff
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DesignerAPI.Model
{
	using System.Collections.Generic;
	using System.Runtime.Serialization;
	using Newtonsoft.Json;

	///<summary>
	/// IXDS item.
	///</summary>
	public interface IXDSItem: IItemId
	{
		#region Properties
		///<summary>
		/// Gets or sets the type.
		///</summary>
		/// <value>
		/// The type.
		/// </value>
		[DataMember(Name="type")]
		[JsonProperty("type")]
		string Type {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the reference.
		///</summary>
		/// <value>
		/// The reference.
		/// </value>
		[DataMember(Name="reference")]
		[JsonProperty("reference")]
		Reference Reference {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the code class.
		///</summary>
		/// <value>
		/// The code class.
		/// </value>
		[DataMember(Name="codeClass")]
		[JsonProperty("codeClass")]
		string CodeClass {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the user config.
		///</summary>
		/// <value>
		/// The user config.
		/// </value>
		[DataMember(Name="userConfig")]
		[JsonProperty("userConfig")]
		UserConfig UserConfig {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the custom configs.
		///</summary>
		/// <value>
		/// The custom configs.
		/// </value>
		[DataMember(Name="customConfigs")]
		[JsonProperty("customConfigs")]
		IList<object> CustomConfigs {
			get;
			set;
		}

		///<summary>
		/// Gets or sets the icon cls.
		///</summary>
		/// <value>
		/// The icon cls.
		/// </value>
		[DataMember(Name="iconCls")]
		[JsonProperty("iconCls")]
		string IconCls {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets a value indicating whether this <see cref="DesignerAPI.Model.XDSItem"/> is expanded.
		///</summary>
		/// <value>
		/// <c>true</c> if expanded; otherwise, <c>false</c>.
		/// </value>
		[DataMember(Name="expanded")]
		[JsonProperty("expanded")]
		bool? Expanded {
			get;
			set;
		}

		[DataMember(Name="masterInstanceId")]
		[JsonProperty("masterInstanceId")]
		string MasterInstanceId {
			get;
			set;
		}

		///<summary>
		/// Gets or sets the designer identifier.
		///</summary>
		/// <value>
		/// The designer identifier.
		/// </value>
		[DataMember(Name="designerId")]
		[JsonProperty("designerId")]
		string DesignerId {
			get;
			set;
		}
		
		///<summary>
		/// Gets or sets the view order.
		///</summary>
		/// <value>
		/// The view order.
		/// </value>
		[DataMember(Name="$viewOrder")]
		[JsonProperty("$viewOrder")]
		int? ViewOrder {
			get;
			set;
		}

		///<summary>
		/// Gets or sets the c identifier.
		///</summary>
		/// <value>
		/// The c identifier.
		/// </value>
		[DataMember(Name="cid")]
		[JsonProperty("cid")]
		string cId { 
			get; 
			set;
		}

		///<summary>
		/// Gets or sets the js class.
		///</summary>
		/// <value>
		/// The js class.
		/// </value>
		[DataMember(Name="jsClass")]
		[JsonProperty("jsClass")]
		string JsClass { 
			get; 
			set;
		}

		///<summary>
		/// Gets or sets the snap to grid.
		///</summary>
		/// <value>
		/// The snap to grid.
		/// </value>
		[DataMember(Name="snapToGrid")]
		[JsonProperty("snapToGrid")]
		long? SnapToGrid {
			get; 
			set; 
		}

		///<summary>
		/// Gets or sets the type of the user X.
		///</summary>
		/// <value>
		/// The type of the user X.
		/// </value>
		[DataMember(Name="userXType")]
		[JsonProperty("userXType")]
		string UserXType { 
			get;
			set; 
		}

		///<summary>
		/// Gets or sets the user component.
		///</summary>
		/// <value>
		/// The user component.
		/// </value>
		[DataMember(Name="userComponent")]
		[JsonProperty("userComponent")]
		string UserComponent { 
			get; 
			set;
		}

#endregion

	}		
}
