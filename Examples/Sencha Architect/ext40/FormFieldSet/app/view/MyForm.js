/*
 * File: app/view/MyForm.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.MyForm', {
    extend: 'Ext.form.Panel',

    autoRender: false,
    height: 250,
    width: 400,
    autoScroll: false,
    layout: {
        type: 'fit'
    },
    bodyPadding: 10,
    title: 'My Form',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'fieldset',
                    title: 'My Fields'
                }
            ]
        });

        me.callParent(arguments);
    }

});