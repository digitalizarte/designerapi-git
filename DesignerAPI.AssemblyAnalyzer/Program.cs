namespace DesignerAPI.AssemblyAnalyzer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using ConsoleLib;

    using Mono.Cecil;

    using ToYuml;

    /// <summary>The program.</summary>
    internal class Program : ProgramBase
    {
        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="Program"/> class.</summary>
        /// <param name="args">The args.</param>
        public Program(string[] args)
            : base(args)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="Program"/> class.</summary>
        /// <param name="arguments">The arguments.</param>
        public Program(Arguments arguments)
            : base(arguments)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>The main.</summary>
        /// <param name="args">The args.</param>
        public static void Main(string[] args)
        {
            Program prg = new Program(args);
            prg.ConfigOptions.PauseOnEnd = true;
            prg.ConfigOptions.LaunchBrowserOnError = true;
            prg.ConfigOptions.LogExceptions = false;
            prg.ConfigOptions.IgnoreZeroArguments = true;
            prg.Init();
            prg = null;
        }

        #endregion

        #region Methods

        /// <summary>The process.</summary>
        protected override void Process()
        {
            const string PathDir = @"C:\Users\Damian\Dropbox\Trabajos\digitalizARTE\SistemaApolo";
                
                // System.IO.Path.Combine (AppDomain.CurrentDomain.BaseDirectory, "..", "..", "..");
            string[] files = Directory.GetFiles(PathDir, @"*Entidad2.dll", SearchOption.AllDirectories);

            Console.WriteLine(PathDir);
            foreach (string file in files)
            {
                if (file.ToLowerInvariant().Contains("com.digitalizarte.apolo"))
                {
                    File.Delete(file);
                }
            }

            IDictionary<string, string> idx = new Dictionary<string, string>();
            IDictionary<string, string> idxType = new Dictionary<string, string>();
            files =
                Directory.GetFiles(PathDir, @"*Entidad2.dll", SearchOption.AllDirectories)
                    .Where(f => f.Contains("Release"))
                    .ToArray();
            List<TypeDefinition> listType = new List<TypeDefinition>();
            foreach (string file in files)
            {
                string fname = Path.GetFileName(file);
                if (!idx.ContainsKey(fname))
                {
                    idx[fname] = fname;
                    Console.WriteLine(fname);
                    AssemblyDefinition asmDef = AssemblyDefinition.ReadAssembly(file);
                    IEnumerable<TypeDefinition> types = asmDef.MainModule.GetTypes();
                    foreach (TypeDefinition type in types)
                    {
                        if (!idxType.ContainsKey(type.FullName))
                        {
                            listType.Add(type);
                            idxType[type.FullName] = type.FullName;
                            Console.WriteLine("\t{0}", type.FullName);
                            foreach (PropertyDefinition pi in type.Properties)
                            {
                                Console.WriteLine("\t\t{0} {1}", pi.Name, pi.PropertyType);
                            }
                        }
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(new YumlGenerator(listType));
            Console.WriteLine();
            Console.WriteLine();
        }

        #endregion
    }
}